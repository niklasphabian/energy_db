### What is this repository for? ###

This repository contains scripts to acquire and store data relevant for the analysis of European electricity production and trade from various sources. 
Besides the acquisition, scripts for data evaluation and visualization are provided.

### How do I get set up? ###

The scripts download data from various sources and store them in a database. In the current implementation, a postgresql database is used. An sql script to create the according schema can be found in "database/create_schema.sql". In order for the scripts to use the database, the host, database name, username and password have to be specified in the configuration file "database/database_config.ini".

The scripts are verified to run under python3.6. Apart from the standard libraries, following libraries are required:

* pandas 0.22
* psycopg2
* requests
* sqlalchemy
* urllib3
* bs4
* python3-lxml 


## Database configuration ## 
A database configuration file has to be created/edited in energy_db/database/database_config.ini with:
~~~~
[server_name]
host = 
database = 
user = 
pwd = ~~~~


## Pandas
the scripts require pandas 0.22

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact