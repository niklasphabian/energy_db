﻿SELECT 
"EEXTrans_100MW_actual_DE"."TimeStamp",
  "EEXTrans_100MW_actual_DE"."Power" AS "Conv",
  ("EEXTrans_Solar_actual_DE"."FZHertz"+"EEXTrans_Solar_actual_DE"."Amprion"+"EEXTrans_Solar_actual_DE"."Tennet"+"EEXTrans_Solar_actual_DE"."TransnetBW") as "Solar",
  ("EEXTrans_Wind_actual_DE"."FZHertz"+"EEXTrans_Wind_actual_DE"."Amprion"+"EEXTrans_Wind_actual_DE"."Tennet"+"EEXTrans_Wind_actual_DE"."TransnetBW") as "Wind"
FROM 
  public."EEXTrans_100MW_actual_DE", 
  public."EEXTrans_Solar_actual_DE", 
  public."EEXTrans_Wind_actual_DE"
WHERE 
  "EEXTrans_100MW_actual_DE"."TimeStamp" = "EEXTrans_Solar_actual_DE"."TimeStamp" AND
  "EEXTrans_Solar_actual_DE"."TimeStamp" = "EEXTrans_Wind_actual_DE"."TimeStamp" AND
  "EEXTrans_100MW_actual_DE"."TimeStamp" >= '2015-02-20'  AND 
  "EEXTrans_100MW_actual_DE"."TimeStamp" < '2015-06-01'  
ORDER BY
  "EEXTrans_100MW_actual_DE"."TimeStamp" ASC;
