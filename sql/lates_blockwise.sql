﻿SELECT 
  "TimeStamps"."TimeStamp", 
  "EEXTrans_Blockwise"."Power",
  "EEXTrans_Blockwise"."PlantID"  
FROM 
  public."EEXTrans_Blockwise", 
  public."TimeStamps"
WHERE 
  "TimeStamps"."timeID" = "EEXTrans_Blockwise"."timeID"
ORDER BY
  "TimeStamps"."TimeStamp" DESC
LIMIT 100;
