﻿SELECT 
  "TimeStamps"."TimeStamp",  
  "Climate_WindByStations"."WindSpeed", 
  "Climate_Stations"."StationName"  
FROM 
  public."Climate_Stations", 
  public."Climate_WindByStations", 
  public."TimeStamps"
WHERE 
  "Climate_WindByStations"."StationID" = "Climate_Stations"."StationID" AND
  "TimeStamps"."timeID" = "Climate_WindByStations"."TimeID" AND
  "Climate_WindByStations"."StationID"  = 03668 AND
  "TimeStamps"."TimeStamp" >= '2015-01-01' AND
  "TimeStamps"."TimeStamp" < '2016-01-01'
ORDER BY
  "TimeStamps"."TimeStamp" ASC;
