﻿SELECT 
  sum("CapacityPrice" * "OfferedPower")
  / sum("OfferedPower")
FROM 
  public."Tender_PRL"
WHERE 
  "Award" =  True AND 
  "TimeStamp" > '2014-01-01' AND 
  "TimeStampEnd" < '2015-01-01' 
