﻿SELECT 
sum(weeklyMax)/count(weeklyMax) 
FROM (
SELECT
"TimeStamp", 
  max("CapacityPrice")/100.0*75.0 as weeklyMax
FROM 
  public."Tender_PRL"
WHERE 
  "Award" =  True AND 
  "TimeStamp" >= '2014-01-01' AND 
  "TimeStampEnd" < '2015-01-01' 
GROUP BY "TimeStamp"
ORDER BY "TimeStamp") as alias;
