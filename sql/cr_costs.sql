﻿SELECT 
	max(DATE_TRUNC('Date', "TimeStamp")),
	SUM("CapacityPrice"*"OfferedPower")/1000/1000  
FROM 
	public."Tender_MRL"
WHERE 
	"Award" = True AND
	"TimePeriode" = 'NT'
GROUP BY  
	DATE_TRUNC('YEAR', "TimeStamp")
order by 
	max(DATE_PART('YEAR', "TimeStamp"))
 
