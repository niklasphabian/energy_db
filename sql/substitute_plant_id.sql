﻿DROP FUNCTION substitue_plantid(integer, integer);

CREATE OR REPLACE FUNCTION substitue_plantid(
    old_id integer,
    new_id integer)
  RETURNS void AS
$BODY$
  DECLARE
  BEGIN
    UPDATE eextrans_blockwise_de SET plant_id = new_id WHERE plant_id = old_id;
    UPDATE power_plants SET power_electric = (SELECT power_electric FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET renovation_year = (SELECT renovation_year FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET construction_year = (SELECT construction_year FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET plz = (SELECT plz FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET power_thermal = (SELECT power_thermal FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET urban = (SELECT urban FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET disposal_plant = (SELECT disposal_plant FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    END;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION substitue_plantid(integer, integer)
  OWNER TO griessbaum;