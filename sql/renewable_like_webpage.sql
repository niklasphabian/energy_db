﻿SELECT 
  date_trunc('hour', "EEXTrans_Wind_expected_DE"."TimeStamp"),
  AVG("EEXTrans_Solar_expected_DE"."FZHertz") +
  AVG("EEXTrans_Solar_expected_DE"."Amprion") + 
  AVG("EEXTrans_Solar_expected_DE"."Tennet") + 
  AVG("EEXTrans_Solar_expected_DE"."TransnetBW") as solar_expected,
  AVG("EEXTrans_Wind_expected_DE"."offshore_FZHertz") +  
  AVG("EEXTrans_Wind_expected_DE"."offshore_Tennet") as offshore_expected,
  AVG("EEXTrans_Wind_expected_DE"."onshore_FZHertz") +
  AVG("EEXTrans_Wind_expected_DE"."onshore_Amprion") +
  AVG("EEXTrans_Wind_expected_DE"."onshore_Tennet") +
  AVG("EEXTrans_Wind_expected_DE"."onshore_TransnetBW") as onshore_expected,
  AVG("EEXTrans_Solar_actual_DE"."FZHertz") +
  AVG("EEXTrans_Solar_actual_DE"."Amprion") + 
  AVG("EEXTrans_Solar_actual_DE"."Tennet") + 
  AVG("EEXTrans_Solar_actual_DE"."TransnetBW") as solar_actual,  
  AVG("EEXTrans_Wind_actual_DE"."offshore_FZHertz") +
  AVG("EEXTrans_Wind_actual_DE"."offshore_Tennet")  as offshore_actual,
  AVG("EEXTrans_Wind_actual_DE"."onshore_FZHertz") +
  AVG("EEXTrans_Wind_actual_DE"."onshore_Amprion") +
  AVG("EEXTrans_Wind_actual_DE"."onshore_Tennet") +
  AVG("EEXTrans_Wind_actual_DE"."onshore_TransnetBW") as onshore_actual
FROM 
  public."EEXTrans_Wind_actual_DE", 
  public."EEXTrans_Wind_expected_DE",
  public."EEXTrans_Solar_expected_DE", 
  public."EEXTrans_Solar_actual_DE"
WHERE 	
  "EEXTrans_Wind_expected_DE"."TimeStamp" = "EEXTrans_Wind_actual_DE"."TimeStamp" AND 
  "EEXTrans_Wind_expected_DE"."TimeStamp" = "EEXTrans_Solar_expected_DE"."TimeStamp" AND
  "EEXTrans_Wind_expected_DE"."TimeStamp" = "EEXTrans_Solar_actual_DE"."TimeStamp" AND
  "EEXTrans_Wind_expected_DE"."TimeStamp" >= '2016-03-01'
GROUP BY  date_trunc('hour', "EEXTrans_Wind_expected_DE"."TimeStamp")	
ORDER BY date_trunc('hour', "EEXTrans_Wind_expected_DE"."TimeStamp") DESC
