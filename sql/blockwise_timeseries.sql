﻿SELECT 
  "TimeStamps"."TimeStamp", 
  "EEXTrans_Blockwise"."Power", 
  "Climate_TemperatureByStations"."AirTemp"
FROM 
  public."EEXTrans_Blockwise", 
  public."TimeStamps", 
  public."PowerPlants", 
  public."PLZToClimateStation", 
  public."Climate_Stations",
  public."Climate_TemperatureByStations"
WHERE 
  "TimeStamps"."timeID" = "EEXTrans_Blockwise"."timeID" AND
  "PowerPlants"."PlantID" = "EEXTrans_Blockwise"."PlantID" AND
  "PLZToClimateStation"."PLZ" = "PowerPlants"."ZipCode" AND
  "Climate_Stations"."StationID" = "PLZToClimateStation"."ClimateStationID" AND
  "Climate_TemperatureByStations"."StationID" = "PlantPLZToClimateStation"."ClimateStationID" AND
  "TimeStamps"."TimeStamp" = "ClimateTemperatureStationsTS"."TimeStamp" AND
  "PowerPlants"."PlantID" = 8112
 order by "TimeStamps"."TimeStamp";
