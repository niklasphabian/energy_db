import energy_db.climate.dwd_ftp as CDC
import energy_db.climate.handles as handles
tmp_folder = '../DataArchive/climate/'


def download_all(tmp_folder):
    dwd_ftp = CDC.DWDFTP()
    dwd_ftp.set_target_folder(tmp_folder)
    print("download solar")
    dwd_ftp.download_solar_files()
    print("download wind")
    dwd_ftp.download_wind_files()
    print("download temperature")
    dwd_ftp.download_temp_files()
    dwd_ftp.close_connection()


def upload(tmp_folder):
    handles.upload_temp(tmp_folder)
    handles.upload_solar(tmp_folder)
    handles.upload_wind(tmp_folder)


if __name__ == '__main__':
    download_all(tmp_folder)
    upload(tmp_folder)
    print('Done.')


