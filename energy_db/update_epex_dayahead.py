from energy_db.epex_spot import day_ahead
from energy_db.epex_spot.db_table import DBTableHourFR
import pytz
import datetime
cet = pytz.timezone('CET')


def latest_entry():
    hourly_table_fr = DBTableHourFR()
    latest_entry = hourly_table_fr.latest_entry()
    if latest_entry is None:
        latest_entry = cet.localize(datetime.datetime(2007, 7, 12))
    return latest_entry


def update_to_today():
    start_date = latest_entry()
    end_date = cet.localize(datetime.datetime.now())
    updater = day_ahead.Updater()
    updater.update_range(start_date=start_date, end_date=end_date)


if __name__ == '__main__':
    update_to_today()

print('Done.')
