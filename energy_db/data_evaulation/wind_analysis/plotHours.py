import energyDB.eex_trans.sectors_table
import matplotlib.pyplot as plt

daTab = energyDB.eex_trans.sectors_table.WindActualDE()


values = []
for hour in range(0,24):
    print(hour)
    values.append(daTab.get_hour(hour))

plt.plot(values)
plt.grid()
plt.xlabel('Hours of the day')
plt.ylabel('Power in MW')
plt.suptitle('Average wind production')
plt.xlim(0,23)
plt.show()

