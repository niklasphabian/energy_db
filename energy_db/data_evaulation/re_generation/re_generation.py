'''
Created on Apr 14, 2016

@author: EIFERKITEDU\griessbaum
'''
import energyDB.database.database
import pandas
import matplotlib.pyplot as plt 
import datetime

db = energyDB.database.database.Database()

with open('renewable_like_webpage.sql', 'r') as sql_file:
    query = sql_file.read()

start_date = datetime.datetime(2016,1,1)
end_date = datetime.datetime(2017,1,1)
ret = db.execute_fetchall(query, (start_date, end_date))
df = pandas.DataFrame(ret, columns=['timestamp','solar_expected', 'offshore_expected', 'onshore_expected', 'solar_actual', 'offshore_actual', 'onshore_actual'])

df['timestamp'] = pandas.DatetimeIndex(data=df['timestamp'])
df.set_index('timestamp', inplace=True)
df = df.resample('w').sum()

df['onshore_actual'].plot()
df['offshore_actual'].plot()
df['solar_actual'].plot()

plt.legend()
plt.grid('on')
plt.show()