SELECT  
  date_trunc('hour', eextrans_wind_expected_de.time_stamp),
  AVG(eextrans_solar_expected_de.fz_hertz) +
  AVG(eextrans_solar_expected_de.amprion) + 
  AVG(eextrans_solar_expected_de.tennet) + 
  AVG(eextrans_solar_expected_de.transnet_bw) as solar_expected,
  AVG(eextrans_wind_expected_de.offshore_fz_hertz) +  
  AVG(eextrans_wind_expected_de.offshore_tennet) as offshore_expected,
  AVG(eextrans_wind_expected_de.onshore_fz_hertz) +
  AVG(eextrans_wind_expected_de.onshore_amprion) +
  AVG(eextrans_wind_expected_de.onshore_tennet) +
  AVG(eextrans_wind_expected_de.onshore_transnet_bw) as onshore_expected,
  AVG(eextrans_solar_actual_de.fz_hertz) +
  AVG(eextrans_solar_actual_de.amprion) + 
  AVG(eextrans_solar_actual_de.tennet) + 
  AVG(eextrans_solar_actual_de.transnet_bw) as solar_actual,  
  AVG(eextrans_wind_actual_de.offshore_fz_hertz) +
  AVG(eextrans_wind_actual_de.offshore_tennet)  as offshore_actual,
  AVG(eextrans_wind_actual_de.onshore_fz_hertz) +
  AVG(eextrans_wind_actual_de.onshore_amprion) +
  AVG(eextrans_wind_actual_de.onshore_tennet) +
  AVG(eextrans_wind_actual_de.onshore_transnet_bw) as onshore_actual
FROM 
  public.eextrans_wind_actual_de, 
  public.eextrans_wind_expected_de,
  public.eextrans_solar_expected_de, 
  public.eextrans_solar_actual_de
WHERE   
  eextrans_wind_expected_de.time_stamp = eextrans_wind_actual_de.time_stamp AND 
  eextrans_wind_expected_de.time_stamp = eextrans_solar_expected_de.time_stamp AND
  eextrans_wind_expected_de.time_stamp = eextrans_solar_actual_de.time_stamp AND
  eextrans_wind_expected_de.time_stamp >= %s AND
  eextrans_wind_expected_de.time_stamp <= %s  
GROUP BY date_trunc('hour', eextrans_wind_expected_de.time_stamp)
ORDER BY date_trunc('hour', eextrans_wind_expected_de.time_stamp) ASC;