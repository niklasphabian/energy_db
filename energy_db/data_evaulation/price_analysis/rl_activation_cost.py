import datetime
import energyDB.regelleistung.tender_table
import energyDB.regelleistung.activation_table
import pandas
import numpy


start = datetime.datetime(2015,1,1,0)
start = start-datetime.timedelta(days = start.weekday())
end = datetime.datetime(2015,11,17)
#end = start + datetime.timedelta(days=365)

def pandaMOL(mol):
    newMol = []
    for row in mol:
        newMol.append(list(row))    
    pdMol = pandas.DataFrame(newMol, columns=['price','power'])        
    return pdMol

def lookupCost(mol, demand):
    if not numpy.isnan(demand):        
        demand = demand
    else: 
        demand =0
    activated = mol[mol['power'].cumsum()<=demand]
    return (activated['power']*activated['price']).sum()/4
    
def srlCost(date, end):
    rlTenderTab = energyDB.regelleistung.tender_table.RLTenderTableSRL()
    rlDemandTab = energyDB.regelleistung.activation_table.SRLActivationTableNRV()
    totCost = 0
    totDem = 0
    while date < end:
        mol = pandaMOL(rlTenderTab.get_mol(date, 'POS', 'HT'))        
        demand = rlDemandTab.get_pos_ht(date, date + datetime.timedelta(days=7))
        totDem = totDem + sum(demand['Dem'].fillna(0))/4
        for row in demand.iterrows():
            activation = row[1]['Dem']
            totCost = totCost + lookupCost(mol, activation)            
        date = date + datetime.timedelta(days=7)
    print(totCost/1000/1000)
    print(totDem/1000/1000)  
    print(totCost/totDem)

def mrlCost(date, end):
    rlTenderTab = energyDB.regelleistung.tender_table.RLTenderTableMRL()
    rlDemandTab = energyDB.regelleistung.activation_table.MRLActivationTableNRV()
    demand = rlDemandTab.get_pos(date, end)
    totDem = demand.sum()
    totCost = 0
    c = 16
    for row in demand.iterrows():        
        date = row[0]
        print(date)
        activation = row[1]['Dem']
        if c == 16:
            c = 0
            mol = pandaMOL(rlTenderTab.get_mol(date, 'POS'))
        totCost = totCost + lookupCost(mol, activation)
        c = c +1
    print(totCost/1000/1000)
    print(totDem/1000/1000)
    print(totCost/totDem)
        

mrlCost(start, end)

