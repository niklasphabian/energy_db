import matplotlib.pyplot as plt
import datetime
import energyDB.regelleistung.tender_table
import numpy
import pandas
from pandas.tools.plotting import autocorrelation_plot
rlTab = energyDB.regelleistung.tender_table.RLTenderTableSRL()

def averageEP(mol, percentile):
    power = []
    ep = []
    for row in mol:
        ep.append(row[0])
        power.append(row[1])
    ep = numpy.array(ep)
    power = numpy.array(power)
    energy = numpy.cumsum(power)
    totEn = energy[-1]
    end = len(energy[energy<=totEn*percentile])
    start = len(energy[energy<=totEn*(percentile-0.1)])
    if len(power[start:end]) > 0:
        return numpy.average(ep[start:end], weights=power[start:end])
    else: 
        return None
       
def mrlEP(percentile, date, endDate):    
    avg = []
    datearray = []    
    while date < endDate:
        print(date)
        mol = rlTab.get_mol(date, 'POS')
        avg.append(averageEP(mol,percentile))
        datearray.append(date)
        date = date + datetime.timedelta(hours=4)
    avg = pandas.TimeSeries(avg, index=datearray)
    return avg

def srlEP(percentile, date, endDate):
    avg = []
    datearray = []
    while date < endDate:    
        print(date)
        molHT = rlTab.get_mol(date, 'NEG', 'HT')
        molNT = rlTab.get_mol(date, 'NEG', 'NT')    
        avg.append((averageEP(molHT, percentile)+averageEP(molNT, percentile))*0.5)
        datearray.append(date)
        date = date + datetime.timedelta(days=7)
    avgTS = pandas.TimeSeries(avg, index=datearray)
    return avgTS


date = datetime.datetime(2011,7,1)
endDate = datetime.datetime(2016,7,1)

avgTS10 = srlEP(0.1, date, endDate)
avgTS60 = srlEP(0.6, date, endDate)

fig = plt.figure()

avgTS10.resample('A').mean().plot(color='r', label='HT+NT', legend=True, linewidth=3, marker='s', markersize=10)
avgTS60.resample('A').mean().plot(color='b', label='HT+NT', legend=True, linewidth=3, marker='s', markersize=10)
avgTS10.resample('W').mean().plot(color='r', label='HT+NT', legend=True)
avgTS60.resample('W').mean().plot(color='b', label='HT+NT', legend=True)

def makeAutoCorrPlot():
    autocorrelation_plot(avgTS10)
    autocorrelation_plot(avgTS60)
    plt.grid('on')
    #plt.legend(['','','','','','Percentile 10', 'Percentile 60'])
    print(avgTS10.autocorr())
    
#makeAutoCorrPlot()

def formatPlot():
    ax = fig.add_subplot(111)
    fontsize = 14
    ax.tick_params(labelsize=14)
    ax.legend(['10% Percentile','60% Percentile', 'Annual Average 10%', 'Annual Average 60%'], loc='best', fontsize=fontsize)
    plt.xlabel('Time in years', fontsize=fontsize)
    plt.ylabel('Price in EUR/MWh', fontsize=fontsize)
    plt.suptitle('Annual average Energy Prices of 10% and 60% percentile', fontsize=fontsize)
    plt.grid('on')

formatPlot()
plt.show()