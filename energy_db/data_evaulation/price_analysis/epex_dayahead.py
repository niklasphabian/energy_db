import pandas
from energyDB.database.database import Database
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)
ax.tick_params(axis = 'both', which = 'major', labelsize = 12)
ax.tick_params(axis = 'both', which = 'minor', labelsize = 6)

db = Database()

query = "SELECT time_stamp,  price::float FROM public.epex_dayahead_hour_DE ORDER BY time_stamp DESC;"
data = db.execute_fetchall(query)
dataFrame = pandas.DataFrame(data, columns=['time_stamp','price'])
dataFrame['time_stamp'] = pandas.to_datetime(dataFrame['time_stamp'], utc=True)
dataFrame.set_index('time_stamp', inplace=True)
dataFrame.resample('A').mean().plot(ax=ax, linewidth=3, marker='.', markersize=20)
dataFrame.resample('M').mean().plot(ax=ax, linewidth=1)
dataFrame.resample('W').mean().plot(ax=ax, linewidth=1)


year_ticks = dataFrame.resample('A').mean().index
month_ticks = dataFrame.resample('A').mean().index


ax.set_xticks(year_ticks)
ax.set_xticks(month_ticks, minor=True)
plt.ylabel('Price in EUR/MWh')
plt.legend(['Annual', 'Monthly', 'Weekly'])
plt.grid('on')
plt.show()