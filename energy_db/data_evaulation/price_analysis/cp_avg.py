import matplotlib.pyplot as plt
import datetime
import energyDB.regelleistung.tender_table

from pandas.tools.plotting import autocorrelation_plot

rlTab = energyDB.regelleistung.tender_table.RLTenderTablePRL()
rlTab = energyDB.regelleistung.tender_table.RLTenderTablePRL()
startDate = datetime.datetime(2011,1,1)
endDate = datetime.datetime(2016,7,12)

resType = 'NEG'
datearray = []

marTS = rlTab.get_max_accepted_cp(startDate, endDate, resType)
avgTS = rlTab.get_weight_average(startDate, endDate, resType)

fig = plt.figure()
ax = fig.add_subplot(111)

avgTS.resample(rule='A').mean().plot(ax=ax, color='r', label='Weighted Average CP', legend=True, linewidth=3, marker='s', markersize=10)
marTS.resample(rule='A').mean().plot(ax=ax, color='b', label='Marginal CP', legend=True,linewidth=3, marker='s', markersize=10) 

avgTS.resample('W').mean().plot(ax=ax, color='r', linestyle='-')
marTS.resample('W').mean().plot(ax=ax, color='b', linestyle='-')

fontsize = 14
plt.ylim(0,7000)
#plt.xlim(datetime.datetime(2011,1,1),datetime.datetime(2016,1,1))
ax.tick_params(labelsize=fontsize)
ax.legend(['Weighted Average CP', 'Marginal CP'], fontsize=fontsize)
plt.suptitle('Marginal and weighted averaged CP', fontsize=fontsize)
plt.xlabel('Time in years', fontsize=fontsize)
plt.ylabel('Price in EUR/MW', fontsize=fontsize)
plt.grid('on')
    
    

def plotAutoCorr():
    fig = plt.figure()
    ax2 = fig.add_subplot(111)
    autocorrelation_plot(marTS, ax=ax2)
    autocorrelation_plot(avgTS, ax=ax2)
    ax2.grid('on')

plt.show()
