import pandas
from energyDB.database.database import Database
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)
ax.tick_params(axis = 'both', which = 'major', labelsize = 12)
ax.tick_params(axis = 'both', which = 'minor', labelsize = 6)

db = Database()
query = 'SELECT time_stamp, price_base FROM epex_dayahead_basepeak_de WHERE time_stamp >=\'2006-01-01\' ORDER BY time_stamp ASC;'
data = db.execute_fetchall(query)
dataFrame = pandas.DataFrame(data, columns=['timestamp', 'price'])
dataFrame['timestamp'] = pandas.to_datetime(dataFrame['timestamp'])
dataFrame.set_index('timestamp', inplace=True)

dataFrame.resample('A').mean().plot(ax=ax, linewidth=1, marker='.', markersize=20)
dataFrame.resample('4M').mean().plot(ax=ax, linewidth=1, marker='.', markersize=5)
#dataFrame.resample('D', how='mean').plot(ax=ax, linewidth=1)

year_ticks = dataFrame.resample('A').mean().index
#ax.set_xticks(year_ticks)


plt.ylabel('Price in EUR/MWh')
plt.legend(['Annual', 'Quarter'])
plt.grid('on')
plt.show()


