import energyDB.regelleistung.tender_table
import datetime

rlTab = energyDB.regelleistung.tender_table.RLTenderTablePRL()

date = datetime.datetime(2014,1,1)
n = 0
totMoney = 0
while date < datetime.datetime(2015,1,1):
    mol = rlTab.get_cp_mol(date)
    percentilePrice = mol[int(len(mol)/100.0*75.0)][0]
    print(str(date), percentilePrice)
    totMoney = totMoney + percentilePrice
    date=date+datetime.timedelta(weeks=1)
    n += 1
    
print(totMoney/n)