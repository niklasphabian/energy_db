'''
Created on Apr 29, 2016

@author: EIFERKITEDU\griessbaum
'''

from energyDB.regelleistung.tendered_table import TenderedTab
import datetime
import matplotlib.pyplot as plt

table = TenderedTab()

start_date = datetime.datetime(2014,1,1)
end_date = datetime.datetime.now()

df = table.get_df(start_date, end_date)
df.plot()

print(df.resample('1M').first())

plt.grid('on')
plt.show()
