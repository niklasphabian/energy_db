import energyDB.epex_spot.db_table
import matplotlib.pyplot as plt
import pandas
import datetime


daTab = energyDB.epex_spot.db_table.DBTableHourDE()

date = datetime.datetime(2005,1,1)
endDate = datetime.datetime(2017,1,1)
avgDA = []
datearray = []

while date < endDate:
    year = date.year
    avgDA.append(round(float(daTab.get_avg_price(year)[0])))
    datearray.append(datetime.datetime(year,1,1))
    date = date + datetime.timedelta(weeks=52)

date = datetime.datetime(2011,1,1)

epFit = []
datearray2 = []
epFit.append(51)
datearray2.append(date)
while date <= datetime.datetime(2035,1,1):
    date = date + datetime.timedelta(weeks=52)
    epFit.append(epFit[-1]*0.7+7.4)    
    datearray2.append(date)


daTS = pandas.Series(avgDA, index=datearray)
marFitTS = pandas.Series(epFit, index=datearray2)
print(marFitTS)

fig = plt.figure()
ax = fig.add_subplot(111)

fontsize = 20
ax.tick_params(labelsize=18)

daTS.plot(label='observed annual average EPEX Spot price', legend=True)
marFitTS.plot(label='Fit: p(n+1) = 0.7*p(n)+7.4', color='b', linestyle='--')

plt.legend(prop={'size':fontsize})
plt.grid('on')
plt.ylim(0,70)
plt.xlim(datetime.datetime(2005,1,1),datetime.datetime(2035,1,1))
plt.xlabel('Time in years', fontsize=fontsize)
plt.ylabel('Price in EUR/MWh', fontsize=fontsize)
plt.suptitle('Annual average EPEX Price', fontsize=fontsize)
plt.show()