import energyDB.regelleistung.activation_table
import datetime
import matplotlib.pyplot as plt
from pandas.tools.plotting import autocorrelation_plot

db = energyDB.regelleistung.activation_table.SRLActivationTableNRV()

#fig = plt.figure()
#ax = plt.subplot()

start = datetime.datetime(2011,1,1)
end = datetime.datetime(2015,10,27)
ts = db.get_neg_ht(start, end)
ts.index = ts.index.to_datetime()
print(ts)



#ts.plot(ax=ax)
#ts.resample(rule='M', how='mean').plot(ax=ax, linewidth=3, )
#ts.resample(rule='A', how='mean').plot(ax=ax, linewidth=3, marker='s', markersize=10)
#plt.ylabel('Activated nSRL in MW')

autocorrelation_plot(ts.resample('1W').sum())

plt.show()
