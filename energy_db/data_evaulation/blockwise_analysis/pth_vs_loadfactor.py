from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise
import pandas
import matplotlib.pyplot as plt


db = Blockwise()

plant_ids = db.plant_ids_chp(chp=True, fuel=None)

df = pandas.DataFrame()
for plant_id in plant_ids:
    fuel = db.fuel(plant_id)
    p_el = db.nom_power_el(plant_id)
    p_th = db.nom_power_th(plant_id)
    pth = p_el/p_th
    avg_power = db.avg_power(plant_id)    
    load_factor = avg_power/p_el
    df = df.append({'pth':pth, 'load_factor':load_factor, 'plant_id':plant_id, 'fuel':fuel}, ignore_index=True)


df.set_index('plant_id', inplace=True)
print(df)

markersize = 12

plt.plot(df[df['fuel']=='lignite']['pth'], df[df['fuel']=='lignite']['load_factor'],linestyle='None', marker='.', markersize=markersize, color='b')
plt.plot(df[df['fuel']=='gas']['pth'], df[df['fuel']=='gas']['load_factor'], linestyle='None', marker='.', markersize=markersize,color='r')
plt.plot(df[df['fuel']=='coal']['pth'], df[df['fuel']=='coal']['load_factor'], linestyle='None', marker='.', markersize=markersize,color='g')
plt.plot(df[df['fuel']=='garbage']['pth'], df[df['fuel']=='garbage']['load_factor'], linestyle='None', marker='.',markersize=markersize, color='c')
plt.plot(df[df['fuel']=='coal-derived-gas ']['pth'], df[df['fuel']=='coal-derived-gas ']['load_factor'], linestyle='None', marker='.', markersize=markersize, color='c')

plt.legend(['lignite', 'gas', 'coal', 'other'])
plt.grid('on')
plt.xlabel('El. power to Th. power ratio')
plt.ylabel('Load factor')
#plt.xlim([0, 20])
plt.savefig('pth_vs_loadfactor.pdf')
plt.show()    
    
