from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise
import pandas
import matplotlib.pyplot as plt
import datetime
blockwise_table = Blockwise()

start_timestamp = datetime.datetime(2017, 3, 1)
end_timestamp = datetime.datetime(2017, 3, 14)


def make_plot(plant_ids):
    tss = []
    fuels = []
    for plant_id in plant_ids:
        time_series = blockwise_table.get_time_series(plant_id, start_timestamp, end_timestamp)
        fuels.append(blockwise_table.fuel(plant_id))
        time_series[str(plant_id[0])] = time_series['power']

        time_series.drop(labels='power', axis=1, inplace=True)
        if not time_series.empty:
            tss.append(time_series)
        df = pandas.concat(tss, axis=1)
    df.fillna(0)

    plt.plot(df.isnull().sum(axis=1))
    df.to_csv('/home/phabian/Dropbox/UCSB/259/HW5/HW5/data/esm_259.csv', index=False, header=fuels)


if __name__ == '__main__':
    query = 'SELECT power_plants.plant_id \
    FROM blockwise.power_plants, blockwise.blockwise_de, blockwise.time_stamps \
    WHERE power_plants.plant_id = blockwise_de.plant_id AND \
    time_stamps.time_id = blockwise_de.time_id AND \
    time_stamps.time_stamp > %s AND \
    time_stamps.time_stamp < %s AND \
    (fuel = \'coal\' OR fuel=\'lignite\' OR fuel=\'gas\') \
    GROUP BY power_plants.plant_id \
    HAVING max(blockwise_de.power) > 0 \
    ORDER BY max(blockwise_de.power) DESC \
    LIMIT 200'.format()

    query = query.format()
    plant_ids = blockwise_table.execute_plant_query(query, (start_timestamp, end_timestamp))
    make_plot(plant_ids=plant_ids)


