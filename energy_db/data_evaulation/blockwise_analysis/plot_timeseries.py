from energyDB.eex_trans.blockwise_table import PlantTable, PowerGenTable
from energyDB.climate.db_table import TemperatureTable
from energyDB.epex_spot.db_table import DBTableHourDE
import datetime
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

plant_table = PlantTable()
power_gen_table = PowerGenTable()
day_ahead_tab = DBTableHourDE()
climate_table = TemperatureTable()

startTime = datetime.datetime(2014, 1, 2, 0)
endTime = datetime.datetime(2014, 1, 10, 0)

def convert2XY(timeSeries):
    x = []
    y = []
    for row in timeSeries:
        x.append(row[0])
        y.append(row[1])    
    return x, y


def plotPlant(startTime, endTime, plant, unit, ax):
    plantID = plant_table.query_plant(plant, unit)
    print(plantID)
    timeSeries = power_gen_table.get_time_series(plantID, startTime, endTime)
    x, y = convert2XY(timeSeries)
    ax.plot(x, y, label=plant)


def plotPrices(startTime, endTime, ax):
    prices = day_ahead_tab.get_time_series(startTime, endTime)
    x, y = convert2XY(prices)    
    ax.plot(x, y,color='red', label="EPEXDayAhead")


def plotTemperature(startTime, endTime, ax):
    temps = climate_table.get_time_series(startTime, endTime, 2928)    
    x, y = convert2XY(temps)    
    ax.plot(x, y, color='green', label="OutsideAirTemp")


def make2Dplot(x,y,xlabel,ylabel,xname,yname,powerPlantName): 
    path = '../../../VisualOutput/'
    filePath = path+yname+'Over'+xname+'.pdf'
    output = PdfPages(filePath)                            
    plt.scatter(x,y)
    plt.title(powerPlantName+': '+yname+' over '+xname)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)  
    plt.savefig(output, format='pdf')
    plt.close()
    output.close()     
        
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2 = ax1.twinx()

unit_name = 'HKW Nord'
plant_name = 'HKW Nord GuD Nord'

plotPlant(startTime, endTime, unit_name, plant_name, ax1)
plotPrices(startTime, endTime, ax2)
plotTemperature(startTime, endTime, ax2)

ax1.legend(loc=1)
ax2.legend(loc=2)

ax1.grid('on', 'major')
ax1.set_xlabel('Time')
ax1.set_ylabel('Power in MW')
ax2.set_ylabel('Energy price in EUR/MWh')

plt.show()


pricelabel = 'Price in EUR/MWh'
templabel  = 'Outside Air Temperature in degC'
powerlabel = 'Electric Power in MW'
make2Dplot(temp, power, templabel, powerlabel,'Temperature','Power',powerPlantName)
make2Dplot(price, power, pricelabel, powerlabel,'Price','Power',powerPlantName)
make2Dplot(temp, price, templabel, pricelabel,'Temperature','Price',powerPlantName)