import energyDB.eex_trans.blockwise_table

plantTable = energyDB.eex_trans.blockwise_table.PlantTable()
plantList = plantTable.unique_chp_plants()

        

def colName(plant):    
    fuel = plant[2]    
    if fuel == 'gas':
        fName = 'ga'
    elif fuel == 'coal':
        fName = 'hc'
    elif fuel == 'lignite' :
        fName = 'li'
    elif fuel == 'oil':
        fName = 'oi'    
    name = 'p_' +str(plant[1])+'_' + fName + '_' + str(int(plant[3]))
    return name


## First Line
print("CREATE VIEW jochen2 AS")
query1 = 'SELECT "TimeStamps"."TimeStamp", ' 
for plant in plantList:    
    query1 = query1 + colName(plant) + ', '    
query1 = query1[0:-2] + ' FROM'
print(query1)
    
## Second Line
print('(SELECT "Power" AS foo, "timeID" FROM public."EEXTrans_Blockwise" WHERE "plantID" = 8152) AS foo left join')

## Mid section
query2 = '(SELECT "Power" AS {cName}, "timeID" FROM public."EEXTrans_Blockwise" WHERE "plantID" = {pID}) as {cName} on foo."timeID" = {cName}."timeID" left join'
for plant in plantList[1:]  : 
    plantID = plant[1]
    query = query2.format(pID = plantID, cName = colName(plant))
    print(query)
    
## Last section
print('public."TimeStamps"')
print('WHERE "TimeStamps"."timeID" = foo."timeID"')
print('ORDER BY "TimeStamps"."TimeStamp"')
    
    
    
    