from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise
blockwise_table = Blockwise()
import pytz
import pandas
utc = pytz.timezone('UTC')
cet = pytz.timezone('CET')
pst = pytz.timezone('US/Pacific')



time_series1 = blockwise_table.get_time_series(plant_id=1).tz_convert(cet)
#time_series2 = blockwise_table.get_time_series(plant_id=243)
time_series1.to_csv('out.csv')
print(time_series1)

