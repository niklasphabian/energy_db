import datetime
import pandas
import matplotlib.pyplot as plt
import energyDB.eex_trans.blockwise_table
import energyDB.climate.db_table
import energyDB.epex_spot.db_table


plant_table  = energyDB.eex_trans.blockwise_table.PlantTable()
power_gen_table = energyDB.eex_trans.blockwise_table.PowerGenTable()
day_ahead_table  = energyDB.epex_spot.db_table.DBTableHourDE()
climate_table   = energyDB.climate.db_table.TemperatureTable()

startTime = datetime.datetime(2016,1,3,0)
endTime   = datetime.datetime(2016,1,3,6)

unit_name = 'HKW Nord GuD Nord'
plant_name = 'HKW Nord'
plantID = plant_table.query_plant(plant_name, unit_name)

generation = power_gen_table.get_time_series_pandas(plantID, startTime, endTime)
prices = day_ahead_table.get_time_series_pandas(startTime, endTime)
temps = climate_table.get_time_series_pandas(startTime, endTime, 2928)

df = pandas.concat([generation, prices, temps], axis=1)
print(df['price'])
print(df['power'])

df['price'].plot()
plt.show()

missing = list(set(timePrice)-set(timePlant))
if len(missing) > 0:
    idx = timePrice.index(missing[0])
    timePlant.insert(idx, missing)
    power.insert(idx, 0)
    print(str(len(missing)))

tmp=[]
for row in price:         
    tmp.append(float(row))
    
price = tmp

fig2 = plt.figure()
ax = fig2.add_subplot(111, projection ='3d')

price.pop()
print(len(price))
print(len(temp))
print(len(power))
ax.scatter(price, temp, power)
ax.set_xlabel('Price in EUR/MWh')
ax.set_ylabel('Outside Air Temperature in degC')
ax.set_zlabel('Electric Power in MW')

plt.show()
