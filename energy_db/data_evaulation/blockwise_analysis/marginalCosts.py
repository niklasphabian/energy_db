#-*- coding: UTF-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from math import floor
from scipy.optimize import curve_fit
from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise

database = Blockwise()
    
class PlantDataCost:

    def __init__(self, dataFrame, plant_id):
        self.dataFrame = dataFrame   
        self.plant_id = plant_id    
        self.nom_pow_el = database.nom_power_el(plant_id)
        self.nom_pow_th = database.nom_power_th(plant_id)
        self.plantName = database.plant_name(plant_id)   
        self.fuel = database.fuel(plant_id).capitalize()
    
    def plotFileName(self):
        if self.nom_pow_th:
            chp = 'CHP_'
        else:
            chp = ''
        return '{chp}{fuel}/{plant_id}.pdf'.format(chp=chp, fuel=self.fuel, plant_id=self.plant_id)
    
    def bin_data(self):
        price = self.dataFrame['price']

        bins = np.linspace(price.min(), price.max(), 100)
        groups = self.dataFrame.groupby(np.digitize(self.dataFrame.price, bins))
        self.priceBins = bins
        self.binnedMedian = groups.median()
        self.binnedAv = groups.mean()

    def calcMarginalCosts(self):
        self.bin_data()
        self.mySigmoid = Sigmoid(self.binnedAv['price'], self.binnedAv['power'])
        self.TurningPoint = self.mySigmoid.getTurnings()
        print('{pid}, {mc}, {power}, {fuel}, {pName}'.format(pid=self.plant_id, fuel=self.fuel, power=self.nom_pow_el, mc=round(100*self.TurningPoint)/100, pName=self.plantName))
        return self.TurningPoint
    
    def plot_marginal_costs(self):
        fig, axes = plt.subplots(nrows=1, ncols=1)    
        self.dataFrame.plot(kind='scatter', x='price', y='power', color='red', ax=axes);    
        self.binnedMedian.plot(kind='scatter', x='price', y='power', color ='green', ax=axes, label = 'binnedMedian');
        self.binnedAv.plot(kind='scatter', x='price', y='power',ax=axes, label = 'binnedMean');
        axes.set_xlabel('Energy Price in EUR/MWh')
        axes.set_ylabel('Power in MW')
        axes.set_xlim(0, 100)    
        axes.grid('on')
        fig.suptitle('{pName} ({pid}). \n Marginal costs: {magC} EUR/MWh'.format(pName = self.plantName, pid = self.plant_id, magC = round(self.TurningPoint,2)))
        self.mySigmoid.plotSigmoidFit(axes)
        axes.axvline(self.TurningPoint)
        plt.savefig(self.plotFileName())
        plt.close()
    
    def plot_maginal_costs_differe(self):
        fig, axes = plt.subplots(nrows=2, ncols=1)    
        self.dataFrame.plot(kind='scatter', x='price', y='power', color='red', ax=axes[0]);    
        self.binnedMedian.plot(kind='scatter', x='price', y='power', color ='green', ax=axes[0], label = 'binnedMedian');
        self.binnedAv.plot(kind='scatter', x='price', y='power',ax=axes[0], label = 'binnedMean');
        axes[0].set_xlabel('Energy Price in EUR/MWh')
        axes[0].set_ylabel('Power in MW')
        axes[0].set_xlim(0, 100)    
        axes[0].grid('on')
        fig.suptitle('{pName} ({pid}). \n Marginal costs: {magC} EUR/MWh'.format(pName = self.plantName, pid = self.plant_id, magC = round(self.TurningPoint,2)))
        axes[0].axvline(self.TurningPoint)
        self.mySigmoid.plotSigmoidFit(axes[0])
        self.mySigmoid.plotDifDif(axes[1])
        differe = self.binnedAv.diff()        
        differe['dif1'] = differe['power']/differe['price']        
        axes[1].scatter(self.binnedAv['price'], differe['dif1'])    
        axes[1].grid('on')
        axes[1].set_xlim(0, 100)
        axes[1].set_ylim(-10, 10)
        axes[1].set_xlabel('Energy Price in EUR/MWh')
        axes[1].set_ylabel('Power/Price in MW/(EUR/MWh)')                    
        plt.savefig(self.plotFileName())
        plt.close()


class Sigmoid():    
    def __init__(self, xSeries, ySeries):
        self.xSeries = xSeries
        self.ySeries = ySeries 
        self.xmin = floor(min(self.xSeries))
        self.x = np.linspace(self.xmin,100,100)
        self.calcSigmoidTrendline()
        self.getMax()
    
    def calcSigmoidTrendline(self):         
        xData = np.array(self.xSeries) 
        yData = np.array(self.ySeries)        
        self.popt, pcov = curve_fit(self.sigmoid1, xData, yData,[37, 45, 5, 10], maxfev=1000000)        

    def sigmoid1(self, x, a, b, c, d):    
        y = a /(1+ (b/(x-(self.xmin-1)))**c) + d
        return y
    
    def dif1(self, x, a, b, c, d):
        x0 = self.xmin        
        dy = (a * b * c * (b/(x-x0))**(-1+c)) / ((1+(b/(x-x0))**c)**2 * (x-x0)**2)        
        return dy
    
    def difdif1(self, x, a, b, c, d):
        x0 = self.xmin
        dy1 = (2 * a* b**2 * c**2 * (b/(x - x0))**(-2 + 2 * c))/((1 + (b/(x - x0))**c)**3 *(x - x0)**4)
        dy2 = -((a * b**2 *(-1 + c)* c* (b/(x - x0))**(-2 + c))/((1 + (b/(x - x0))**c)**2 * (x - x0)**4))
        dy3 = - (2 * a * b *c * (b/(x - x0))**(-1 + c))/((1 + (b/(x - x0))**c)**2 * (x - x0)**3)
        dy = dy1 + dy2 + dy3        
        return dy
    
    def getMax(self):
        dif = self.dif1(self.x, *self.popt).tolist()                
        idx = np.nanargmax(dif)        
        return self.x[idx]
    
    def getTurnings(self):
        dif = self.difdif1(self.x, *self.popt).tolist()
        idx = np.nanargmin(dif)
        return self.x[idx]                
                  
    def plotSigmoidFit(self, ax):        
        y = self.sigmoid1(self.x, *self.popt)        
        ax.plot(self.x,y, label='sigmoidFit', linewidth = 3)
    
    def plotDif(self, ax):
        y = self.dif1(self.x, *self.popt)
        ax.plot(self.x,y, label='sigmoidFit', linewidth = 3)
    
    def plotDifDif(self, ax):
        y = self.difdif1(self.x, *self.popt)
        ax.plot(self.x,y, label='sigmoidFit', linewidth = 3)


def download_all(chp=False, fuel='gas'):    
    plantIDList = database.plant_ids_chp(chp, fuel)          
    mcList = []
    for plantID in plantIDList:
        mc = getOnePlant(plantID)
        mcList.append(mc)   
    return mcList


def getOnePlant(plant_id):    
    #mol = database.get_mol2015(plant_id)
    mol = database.get_mol(plant_id)
    if len(mol) > 1000:
        myPlantData = PlantDataCost(mol, plant_id)
        mc = myPlantData.calcMarginalCosts()
        myPlantData.plot_marginal_costs()
        return round(mc*100)/100   


def makeBoxPlots():    
    coal = list(filter(None, download_all(chp=False, fuel='coal')))
    gas = list(filter(None, download_all(chp=False, fuel='gas')))
    lig = list(filter(None, download_all(chp=False, fuel='lignite')))
    coalCHP = list(filter(None, download_all(chp=True, fuel='coal')))
    gasCHP = list(filter(None, download_all(chp=True, fuel='gas')))
    ligCHP = list(filter(None, download_all(chp=True, fuel='lignite')))
    
    fig, axes = plt.subplots(nrows=1, ncols=1)
    axes.boxplot([coalCHP, gasCHP, ligCHP, coal, gas, lig])

    axes.set_xticklabels(['Coal CHP', 'Gas CHP', 'Lignite CHP', 'Coal', 'Gas', 'Lignite'])
    axes.set_yscale('linear')
    axes.grid('on')
    
    fig.subplots_adjust(hspace=0.4)
    plt.savefig('boxplot.pdf')
    plt.show()
    
    
#getOnePlant(8101)
#makeBoxPlots()

import pandas


def save_bins():
    plant_ids = database.plant_ids_all()
    tss = []
    for plant_id in plant_ids:
        print(plant_id)
        mol = database.get_mol(plant_id)
        if len(mol) > 5000:
            plant_data = PlantDataCost(mol, plant_id)
            plant_data.bin_data()
            lc = plant_data.binnedMedian['power']
            lc.name = plant_data.plantName

            tss.append(plant_data.binnedMedian['power'])
            df = pandas.concat(tss, axis=1)
    df.to_csv('out.csv')



save_bins()