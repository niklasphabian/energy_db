from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise
import pandas
import matplotlib.pyplot as plt
import datetime
blockwise_table = Blockwise()
import pytz
cet = pytz.timezone('CET')
start_timestamp = datetime.datetime(2016, 1, 1)
end_timestamp = datetime.datetime(2017, 1, 1)

def make_plot(fuel, frequency, plant_ids):
    tss = []
    for plant_id in plant_ids:
        time_series = blockwise_table.get_time_series(plant_id, start_timestamp, end_timestamp)
        nom_power = blockwise_table.max_power(plant_id)
        time_series[str(plant_id[0])] = time_series['power'] #/ nom_power
        time_series.drop(labels='power', axis=1, inplace=True)
        if not time_series.empty:
            tss.append(time_series)
        df = pandas.concat(tss, axis=1)

    plt.plot(df.isnull().sum(axis=1))
    nop = len(df.columns) - df.isnull().sum(axis=1) # Number of plants
    df['sum'] = df.sum(axis=1) / nop
    df['nop'] = nop
    if frequency == 'daily':
        df = df.resample('D')
    elif frequency == 'weekly':
        df = df.resample('W')
    elif frequency == 'monthly':
        df = df.resample('M')

    df.to_csv('load_curves/{fuel}_{frequency}.csv'.format(fuel=fuel, frequency=frequency))
    df['sum'].plot()
    plt.ylim([0, 1])
    plt.ylabel('Normalized power')
    plt.title('{fuel}'.format(fuel=fuel, frequency=frequency))
    plt.grid('on')
    plt.savefig('load_curves/{fuel}_{frequency}.pdf'.format(fuel=fuel, frequency=frequency))
    plt.close()
    #plt.show()


fuels = ['gas', 'coal', 'lignite']
frequencies = ['hourly', 'daily', 'weekly', 'monthly']


for fuel in fuels:
    print(fuel)
    for frequency in frequencies:
        query = 'SELECT plant_id ' \
                'FROM "blockwise"."power_plants"' \
                'WHERE fuel=\'{fuel}\' '
        query = query.format(fuel=fuel)
        plant_ids = blockwise_table.execute_query(query)
        make_plot(fuel=fuel, frequency=frequency, plant_ids=plant_ids)

