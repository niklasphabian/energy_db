from energyDB.eex_trans.blockwise_table import PowerGenTable, PlantTable
import pandas
import pytz
cet = pytz.timezone('CET')

class Blockwise:
    def __init__(self):
        self.genTable = PowerGenTable()
        self.plantTable = PlantTable()
        
    def get_time_series(self, plant_id, start_timestamp, end_timestamp):
        query_trunk = 'SELECT \
                            time_stamps.time_stamp, \
                            blockwise_de.power \
                        FROM \
                            blockwise.blockwise_de,\
                            blockwise.time_stamps\
                        WHERE \
                            blockwise_de.time_id = time_stamps.time_id AND \
                            blockwise_de.plant_id = %s AND \
                            time_stamps.time_stamp >= %s AND\
                            time_stamps.time_stamp <= %s \
                        ORDER BY time_stamp'
        self.genTable.cursor.execute(query_trunk, (plant_id, start_timestamp, end_timestamp))
        ret = self.genTable.cursor.fetchall()
        df = pandas.DataFrame(ret, columns=['time_stamp', 'power'])
        df['time_stamp'] = pandas.DatetimeIndex(data=df['time_stamp'])
        df.set_index('time_stamp', inplace=True)
        return df  
    
    def get_mol(self, plant_id):
        queryTrunk = 'SELECT \
            power, \
            price \
        FROM \
            blockwise.time_stamps, \
            blockwise.blockwise_de,\
            epexspot.dayahead_hour_de\
        WHERE \
            blockwise_de.time_id = time_stamps.time_id AND \
            time_stamps.time_stamp = dayahead_hour_de.time_stamp AND \
            plant_id = %s AND \
            price > 0 AND \
            price < 100 AND \
            power > 0 \
        ORDER BY price'
        
        self.genTable.cursor.execute(queryTrunk,(plant_id,))
        ret = self.genTable.cursor.fetchall()        
        return self.pg2pd(ret, 'power', 'price')
    
    def get_mol2015(self, plant_id):
        queryTrunk = 'SELECT \
            eextrans_blockwise_de.power, \
            epex_dayahead_hour_de.price \
        FROM \
            public.time_stamps, \
            public.eextrans_blockwise_de,\
            public.epex_dayahead_hour_de\
        WHERE \
            eextrans_blockwise_de.time_id = time_stamps.time_id AND \
            time_stamps.time_stamp = epex_dayahead_hour_de.time_stamp AND \
            time_stamps.time_stamp < \'2015-04-01\' AND \
            eextrans_blockwise_de.plant_id = %s AND \
            epex_dayahead_hour_de.price > 0 AND \
            epex_dayahead_hour_de.price < 100 AND \
            eextrans_blockwise_de.power > 0 \
        ORDER BY price'
        
        self.genTable.cursor.execute(queryTrunk,(plant_id,))
        ret = self.genTable.cursor.fetchall()        
        return self.pg2pd(ret, 'power', 'price')
    
    def getTOL(self, plantID):
        queryTrunk = 'SELECT \
                    eextrans_blockwise_de.power, \
                    climate_temperature_by_station."AirTemp" \
                FROM  \
                    public.power_plants, \
                    public.eextrans_blockwise_de, \
                    public.time_stamps,\
                    public."PlantPLZToClimateStation", \
                    public.climate_temperature_by_station \
                WHERE \
                    eextrans_blockwise_de.plant_id = power_plants.plant_id AND \
                    eextrans_blockwise_de.time_id = time_stamps.time_id AND \
                    "PlantPLZToClimateStation"."PlantPLZ" = power_plants."ZipCode" AND \
                    "PlantPLZToClimateStation"."ClimateStationID" = climate_temperature_by_station."StationID" AND \
                    climate_temperature_by_station.time_stamp = time_stamps.time_stamp AND \
                    time_stamps.time_stamp >= \'2010-04-30\' AND \
                    eextrans_blockwise_de.plant_id = %s \
                ORDER BY \
                    climate_temperature_by_station."AirTemp";'
        self.genTable.cursor.execute(queryTrunk,(plantID,))
        ret = self.genTable.cursor.fetchall()        
        return self.pg2pd(ret)
    
    def pg2pd(self, inList, xValName,  yValName):
        yVals = []
        xVals = []
        for row in inList:
            xVals.append(float(row[0]))
            yVals.append(float(row[1]))                            
        xVals = pandas.Series(xVals)
        yVals = pandas.Series(yVals)
        return pandas.DataFrame({xValName:xVals, yValName:yVals})

    def pg2List(self, inList, col):
        outList = []
        for entry in inList:
            outList.append(entry[col])
        return outList

    def plant_ids_all(self):
        query = 'SELECT   plant_id \
                FROM  blockwise.blockwise_de \
                GROUP BY plant_id \
                ORDER BY max(power)'
        self.plantTable.cursor.execute(query)
        tempList = self.plantTable.cursor.fetchall()
        plantIDList = self.pg2List(tempList, 0)
        return plantIDList

    def plant_ids_chp(self, chp, fuel):
        query = 'SELECT plant_id FROM blockwise.power_plants WHERE power_thermal {chp} AND fuel {fuel}'
        if fuel:
            fuelStr = '= \'{}\''.format(fuel)
        else:
            fuelStr = 'IS NOT NULL'
        if chp :
            query = query.format(chp = '> 0', fuel = fuelStr)
        else:            
            query = query.format(chp = 'IS NULL', fuel = fuelStr)        
        self.plantTable.cursor.execute(query)
        tempList = self.plantTable.cursor.fetchall()
        plantIDList = sorted(self.pg2List(tempList, 0))
        return plantIDList
    
    def plant_ids_urban_chp(self):
        query = 'SELECT plant_id FROM blockwise.power_plants WHERE urban=TRUE AND power_thermal>0'
        self.plantTable.cursor.execute(query)
        return self.plantTable.cursor.fetchall()
    
    def execute_plant_query(self, query, arg_list=None):
        self.plantTable.cursor.execute(query, arg_list)
        return self.plantTable.cursor.fetchall()

    def plant_ids_industrial_chp(self):
        query = 'SELECT plant_id FROM public.power_plants WHERE industrial=TRUE AND power_thermal>0'
        self.plantTable.cursor.execute(query)
        return self.plantTable.cursor.fetchall()
    
    def plant_name(self, plant_id):
        self.plantTable.cursor.execute('SELECT full_name FROM blockwise.power_plants WHERE plant_id = %s', (plant_id,))
        tempList = self.plantTable.cursor.fetchone()
        plantName = tempList[0]
        plantName = plantName.replace('ü', 'ue')
        plantName = plantName.replace('ö', 'oe')
        plantName = plantName.replace('ä', 'ae')
        plantName = plantName.replace('ß', 'ss')
        return plantName    
    
    def fuel(self, plant_id):
        self.plantTable.cursor.execute('SELECT fuel FROM blockwise.power_plants WHERE plant_id = %s', (plant_id,))
        fuel = self.plantTable.cursor.fetchone()[0]
        return fuel    
    
    def nom_power_el(self, plant_id):
        self.plantTable.cursor.execute('SELECT power_electric FROM blockwise.power_plants WHERE plant_id = %s', (plant_id,))
        return self.plantTable.cursor.fetchone()[0]
    
    def nom_power_th(self, plant_id):
        self.plantTable.cursor.execute('SELECT power_thermal FROM blockwise.power_plants WHERE plant_id = %s', (plant_id,))
        return self.plantTable.cursor.fetchone()[0]
    
    def avg_power(self, plant_id):
        query = 'SELECT sum(power)/count(power) FROM blockwise.blockwise_de WHERE plant_id = %s'
        self.plantTable.cursor.execute(query, (plant_id,))
        return self.plantTable.cursor.fetchone()[0]

    def max_power(self, plant_id):
        query = 'SELECT max(power) FROM blockwise.blockwise_de WHERE plant_id = %s'
        self.plantTable.cursor.execute(query, (plant_id,))
        return self.plantTable.cursor.fetchone()[0]

