from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise
import numpy


class PlantData:
    
    def __init__(self, dataFrame, plant_id):
        production_table = Blockwise()
        self.dataFrame = dataFrame   
        self.plant_id = plant_id    
        self.plantName = production_table.plant_name(plant_id)
        
    def binData(self):
        xVals = self.dataFrame['xVals']
        bins = numpy.linspace(xVals.min(), xVals.max(), 100)
        groups = self.dataFrame.groupby(numpy.digitize(self.dataFrame.xVals, bins))
        self.binnedMedian = groups.median()
        self.binnedAv = groups.mean()
        
        
    def plotFileName(self):
        name = 'Plant_{}.png'.format(self.plant_id)
        return name         