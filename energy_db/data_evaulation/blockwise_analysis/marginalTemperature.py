#-*- coding: UTF-8 -*-

from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise
from energyDB.dataEvaluation.blockWiseAnalysis.plantData import PlantData
import matplotlib.pyplot as plt
import numpy

class PlantDataTemp(PlantData):
       
    def binData(self):
        xVals = self.dataFrame['xVals']
        bins = numpy.linspace(xVals.min(), xVals.max(), 100)        
        groups = self.dataFrame.groupby(numpy.digitize(self.dataFrame.xVals, bins))                
        self.binnedMedian = groups.median()
        self.binnedAv = groups.mean()       
        
        
    def plot_marginal_costs(self):
        fig, axes = plt.subplots(nrows=1, ncols=1)    
        self.dataFrame.plot()
        self.binnedMedian.plot(kind='scatter', x='xVals', y='yVals', color ='green', ax=axes, label='binnedMedian')
        self.binnedAv.plot(kind='scatter', x='xVals', y='yVals', color ='blue', ax=axes, label = 'binnedAverage', legend=True)
        plt.legend(['All Data', 'Binned Median', 'Binned Average'], loc=4)
        axes.set_xlabel('Temperature in degC')
        axes.set_ylabel('Power in MW')
        axes.set_xlim(-10, 35)
        axes.set_ylim(bottom = 0)    
        axes.grid('on')
        
        fig.suptitle('{pName} ({pid})'.format(pName = self.plantName, pid = self.plant_id))
        plt.savefig(self.plotFileName())  
        
    
    
        
        


def update_all():
    production_table = Blockwise()
    plantIDList = production_table.plant_ids_chp(False, 'coal')
    for plantID in plantIDList:
        mc = getOnePlant(plantID, production_table)
    

def getOnePlant(plant_id, production_table):    
    tol = production_table.getTOL(plant_id)
    if len(tol) > 2000:
        myPlantData = PlantDataTemp(tol, plant_id)
        
        myPlantData.binData()                
        myPlantData.plot_marginal_costs()   
        

update_all()