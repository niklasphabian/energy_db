import energyDB.database
import pandas
import numpy
import pickle
import xlsxwriter
from matplotlib import cm
from cmath import isnan
from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise


class BlockwiseAnalyzer():
    plantTable = energyDB.eexTrans.blockwise_table.PlantTable()
    genTable = energyDB.eexTrans.blockwise_table.PowerGenTable()

    def __init__(self,plantIDList):
        self.plantIDList = plantIDList
    
    def pg2MOL(self, inList, col):
        outList = []
        for entry in inList:
            outList.append(entry[col])
        return outList
    
    def setTimes(self, startTime, endTime):
        self.startTime = startTime
        self.endTime = endTime
    
    def get_time_series(self):                
        queryTrunk = 'SELECT "myPow" FROM \
                    (SELECT "Power" as foo, "timeID" FROM public."EEXTrans_Blockwise" WHERE "PlantID" = 8152) as foo left join \
                    (SELECT "Power" as "myPow", "timeID" FROM public."EEXTrans_Blockwise" WHERE "PlantID" = {pid}) AS "myPow" ON foo."timeID" = "myPow"."timeID", \
                    public."TimeStamps" \
                    WHERE "TimeStamps"."timeID" = foo."timeID" AND "TimeStamps"."TimeStamp" >= %s AND  "TimeStamps"."TimeStamp" <= %s\
                    ORDER BY "TimeStamps"."TimeStamp" asc;'
        self.timeSeries = []    
        for plant_id in self.plantIDList:
            query = queryTrunk.format(pid = plant_id)                        
            self.genTable.cursor.execute(query,(self.startTime, self.endTime))
            ret = self.genTable.cursor.fetchall()
            ret = self.pg2MOL(ret, 0)
            self.timeSeries.append(ret)
        
    def getMedianDownTime(self):
        downtime = [0]        
        for ts in self.timeSeries:
            downtime.append(ts.count(0))        
        print('med downtime is: {MDT}'.format(MDT = int(round(numpy.median(numpy.array(downtime))))))
        print('ave downtime is: {ADT}'.format(ADT = int(round(numpy.average(numpy.array(downtime))))))        
                
    def getMedianFPH(self):
        fph = [0]
        for ts in self.timeSeries:
            nomPower = max(ts)
            energy = sum(ts)
            if nomPower > 0:
                fph.append(energy/nomPower)
            else :
                fph.append(0) 
        print('med FPH is: {mFPH}'.format(mFPH = int(round(numpy.median(numpy.array(fph))))))
        print('ave FPH is: {aFPH}'.format(aFPH = int(round(numpy.average(numpy.array(fph))))))            
           
    def dropNan(self):
        nPlant = 0
        new_plantIDList = []
        new_timeSeries = []        
        for ts in self.timeSeries:
            ts = pandas.Series(ts)            
            numberOfNans = len(ts) - ts.count()
            pid = self.plantIDList[nPlant]                  
            if numberOfNans > 1000 :
                print 'Plant {pid} had {nnan} nans and was dropped'.format(pid = pid, nnan = numberOfNans)                
            else :
                ts = ts.fillna(0).tolist()
                new_plantIDList.append(pid)
                new_timeSeries.append(ts)        
            nPlant = nPlant + 1
        self.plantIDList = new_plantIDList
        self.timeSeries = new_timeSeries
            
    def makeCorCoeff(self):
        self.cc = numpy.corrcoef(self.timeSeries)
        
    def makeCorCoeff2(self):
        nTs = len(self.timeSeries)
        lTs = len(self.timeSeries[0])
        self.cc = [[0 for x in range(nTs)] for x in range(nTs)] 
        for row in range(nTs) :            
            for col in range(nTs) :
                iTot = 0.01   
                for z in range(lTs):
                    if self.timeSeries[col][z] > 0 :
                        iTot = iTot + 1.0            
                        if self.timeSeries[row][z] > 0:
                            self.cc[row][col] = self.cc[row][col] + 1
                self.cc[row][col] = self.cc[row][col]/iTot
        
    def writeXLS(self):
        xlsxFile = 'cc.xlsx'
        workbook  = xlsxwriter.Workbook(xlsxFile)
        worksheet = workbook.add_worksheet()    
        height = len(self.cc)
         
        for row in range(height):
            worksheet.write(row+1,0, self.header[row])            
            for col in range(height):
                worksheet.write(0, col+1, self.header[col])
                colorFormat = workbook.add_format()
                val = self.cc[row][col]
                if isnan(val):
                    val = 0
                rgb = cm.get_cmap('autumn')
                rgbColor = rgb(int(val*300))
                hexColor = RGBtoHexColor((rgbColor[0]*255,rgbColor[1]*255, rgbColor[2]*255))
                colorFormat.set_bg_color(hexColor)
                worksheet.write(row+1, col+1, val, colorFormat)                     
        workbook.close()   
        
    def makeHeader(self):     
        self.header = []   
        for pID in self.plantIDList:
            self.plantTable.cursor.execute('SELECT "Fuel", "PowerElectric","PowerThermal" FROM public."PowerPlants" WHERE "PlantID" = {pID}'.format(pID = pID))
            ret = self.plantTable.cursor.fetchall()
            fuel = ret[0][0]
            pEl = str(int(ret[0][1]))
            if ret[0][2]:
                pTh =  str(int(ret[0][2]))
            else:
                pTh =  '0'
            self.header.append(str(pID) +'_' + fuel+ '_' + pEl + '_' + pTh)
            
            
    def tempCorrelator(self):
        queryTrunk = 'SELECT "EEXTrans_Blockwise"."Power", "ClimateTemperatureStationsTS"."AirTemp" \
                FROM   public."PowerPlants", public."EEXTrans_Blockwise", public."TimeStamps",public."PlantPLZToClimateStation", public."ClimateTemperatureStationsTS" \
                WHERE "EEXTrans_Blockwise"."PlantID" = "PowerPlants"."PlantID" AND \
                      "EEXTrans_Blockwise"."timeID" = "TimeStamps"."timeID" AND \
                      "PlantPLZToClimateStation"."PlantPLZ" = "PowerPlants"."ZipCode" AND \
                      "PlantPLZToClimateStation"."ClimateStationID" = "ClimateTemperatureStationsTS"."StationID" AND \
                      "ClimateTemperatureStationsTS"."TimeStamp" = "TimeStamps"."TimeStamp" AND \
                      "TimeStamps"."TimeStamp" >= %s AND \
                      "TimeStamps"."TimeStamp" <= %s \
                      "EEXTrans_Blockwise"."PlantID" = {pid};'
        
        for plantID in self.plantIDList:
            query = queryTrunk.format(pid = plantID)                        
            self.genTable.cursor.execute(query,(self.startTime, self.endTime))
            ret = self.genTable.cursor.fetchall()
            power = self.pg2MOL(ret, 0)
            temp = self.pg2MOL(ret, 1)
            ts = pandas.Series(power)            
            numberOfNans = len(ts) - ts.count()                         
            if numberOfNans < 1000 :
                cc = numpy.corrcoef(power, temp)
                try:                        
                    print(str(plantID) + ', ' + str(cc[0,1]))
                except:
                    print(str(plantID) + ', ' + str('nan'))
            

def dump(dumped):
    with open('dump.pickle', 'w') as dumpFile:
        pickle.dump(dumped, dumpFile)
                    
def readDump():
    with open('dump.pickle', 'r') as dumpFile:
        res = pickle.load(dumpFile)
    return res

def RGBtoHexColor(rgbTuple):    
    hexColor = '#%02x%02x%02x' % rgbTuple
    return hexColor
  
def update_all():
    production_table = Blockwise()
    plantIDList = production_table.plant_ids_chp(CHP = False, fuel='coal')    
    myCorrelator = BlockwiseAnalyzer(plantIDList)
    myCorrelator.setTimes('2014-10-01', '2015-04-30')        
    #myCorrelator.tempCorrelator()    
    myCorrelator.get_time_series()
    myCorrelator.dropNan()
    #myCorrelator.getMedianDownTime()
    myCorrelator.getMedianFPH()
    #myCorrelator.makeHeader()
    #myCorrelator.makeCorCoeff2()           
    #myCorrelator.writeXLS()    
    
update_all()
