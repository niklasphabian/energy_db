'''
Created on Jun 2, 2015

@author: griessbaum
'''

from energyDB.dataEvaluation.blockWiseAnalysis.database import Blockwise
from energyDB.dataEvaluation.blockWiseAnalysis.plantData import PlantData
import matplotlib.pyplot as plt
import datetime
import numpy
import pandas

class downtime(PlantData):
    
    def binData(self):        
        time = self.dataFrame['xVals']
        power = self.dataFrame['yVals']
        bins = []
        bins.append(time[0])
        binnedMedian = []
        binnedAverag = []
        delta = datetime.timedelta(hours = 72)
        r = 0
        powerBin = []
        for row in time:
            if row < bins[-1] + delta:
                powerBin.append(power[r])
            else:
                binnedMedian.append(numpy.median(powerBin))
                binnedAverag.append(numpy.average(powerBin))
                bins.append(row)
                powerBin = []                
            r = r + 1
        binnedMedian.append(numpy.median(powerBin))
        xVals = pandas.Series(bins)
        yVals = pandas.Series(binnedMedian)
        self.binnedMedian = pandas.DataFrame({'yVals':yVals, 'xVals':xVals})
        yVals = pandas.Series(binnedAverag)
        self.binnedAverag = pandas.DataFrame({'yVals':yVals, 'xVals':xVals})
        
    def makeFigureTimeSeries(self):
        fig, axes = plt.subplots(nrows=1, ncols=1)        
        #self.dataFrame.plot(x='xVals', y='yVals', color='red', ax=axes, label='AllData')
        #self.binnedMedian.plot(x='xVals', y='yVals', color ='green', ax=axes, label='binnedMedian')
        self.binnedAverag.plot(x='xVals', y='yVals', color ='green', ax=axes, label='binnedMedian')
        
        plt.legend(['All Data'], loc=4)
        axes.set_xlabel('Time')
        axes.set_ylabel('Power in MW')
        #axes.set_xlim(-10, 35)
        axes.set_ylim(bottom = 0)    
        axes.grid('on')
        
        #plt.show()
        fig.suptitle('{pName} ({pid})'.format(pName = self.plantName, pid = self.plant_id))
        plt.savefig(self.plotFileName())  
        
    def powerLevel(self) :
        power = self.dataFrame['yVals']
        maxP = power.max()
        totTime = float(len(power))
        p0 = sum(power > 0)/totTime
        p25 = sum(power > 0.25*maxP)/totTime
        p50 = sum(power > 0.50*maxP)/totTime
        p75 = sum(power > 0.75*maxP)/totTime
        print('{p0}, {p25}, {p50}, {p75}'.format(p0=p0, p25 = p25, p50 = p50, p75 = p75))
        
        
            
                  

def update_all():
    production_table = Blockwise()
    plantIDList = production_table.plant_ids_chp(False, 'lignite')
    for plantID in plantIDList:
        getOnePlant(plantID, production_table)
        
    
def getOnePlant(plant_id, production_table):    
    ts = production_table.get_time_series(plant_id)
    if len(ts) > 2000:
        myPlantData = downtime(ts, plant_id)
        myPlantData.powerLevel()      
        #myPlantData.binData()       
        #myPlantData.makeFigureTimeSeries()   
        
        
update_all()
