import datetime
import pytz
import glob
import energy_db.climate.data_files as Files
cet = pytz.timezone('CET')


class StatusPrinter:    
    def __init__(self, total_number):
        self.startTime = datetime.datetime.now()
        self.idx = 1
        self.totNum = total_number
        
    def update(self, file_path):
        file_name = file_path.rsplit('/', 1)[1]
        now = datetime.datetime.now()
        elapsed_time = now - self.startTime
        time_left = (elapsed_time / self.idx) * (self.totNum - self.idx)
        print(now.strftime('%Y-%m-%d %H:%M') + ': ' + file_name + ' done. Time Left: ' + str(time_left).split('.')[0])
        self.idx += 1


def upload_wind(folder):
    file_list = sorted(glob.glob(folder + '*_ff_*'))
    status = StatusPrinter(len(file_list))
    for file_path in file_list:
        file = Files.WindFile(file_path)
        file.upload_missing()
        file.delete_file()
        status.update(file_path)


def upload_solar(folder):
    file_list = sorted(glob.glob(folder + '*_st_*'))
    status = StatusPrinter(len(file_list))
    for file_path in file_list:
        file = Files.SolarFile(file_path)
        file.upload_missing()
        file.delete_file()
        status.update(file_path)


def upload_temp(folder):
    file_list = sorted(glob.glob(folder + '*_tu_*'))
    status = StatusPrinter(len(file_list))
    for file_path in file_list:
        file = Files.TemperatureFile(file_path)
        file.upload_missing()
        file.delete_file()
        status.update(file_path)

