import ftplib
import os
import datetime


class DLStatus:
    def __init__(self, file_list):
        self.fileList = file_list
        self.n_files = len(file_list)
        self.counter = 0
        self.start_time = datetime.datetime.now()
        
    def time_left(self):
        tot_time = datetime.datetime.now() - self.start_time
        avg_time = tot_time/self.counter
        avg_time = avg_time
        time_left = avg_time * (self.n_files - self.counter)
        time_left = time_left - datetime.timedelta(microseconds=time_left.microseconds)
        return time_left
        
    def update(self):
        self.counter = self.counter + 1
        n_left = self.n_files - self.counter
        print('{n_left} files left. Time left: {t_left}'.format(n_left=n_left,
                                                                     n_tot=self.n_files,
                                                                     t_left=self.time_left()))


class DWDFTP:
    def __init__(self):
        self.ftp = ftplib.FTP('ftp-cdc.dwd.de')        
        self.ftp.login()
        self.target_folder = None
    
    def set_target_folder(self, target_folder):
        self.target_folder = target_folder
            
    def download_temp_files(self):
        self.ftp.cwd('/pub/CDC/observations_germany/climate/hourly/air_temperature/recent/')
        self.download_files()
    
    def download_wind_files(self):
        self.ftp.cwd('/pub/CDC/observations_germany/climate/hourly/wind/recent/')
        self.download_files()
        
    def download_solar_files(self):
        self.ftp.cwd('/pub/CDC/observations_germany/climate/hourly/solar/')
        self.download_files()
    
    def download_files(self):
        file_list = self.ftp.nlst('*.zip')
        dl_status = DLStatus(file_list)
        for fileName in file_list:
            self.download_file(fileName)
            dl_status.update()
        
    def download_file(self, file_name):
        tmp_file_path = self.target_folder + 'tmp.zip'
        with open(tmp_file_path, 'wb') as outFile:
            self.ftp.retrbinary('RETR {}'.format(file_name), outFile.write)
        self.extract_file(tmp_file_path)
    
    def extract_file(self, tmp_file_path):
        climate_file = ClimateZipFile(tmp_file_path)
        climate_file.extract_file()
        climate_file.delete()
    
    def close_connection(self):
        self.ftp.quit()
        

class ClimateZipFile:
    def __init__(self, file_path):
        self.file_path = file_path
        self.folder_name = file_path.rsplit('/', 1)[0]
        self.file_name = file_path.rsplit('/', 1)[1]

    def extract_file(self):
        os_string = 'unzip -q -o {filePath} -d {targetFolder} -x {excluded}'
        os_string = os_string.format(filePath=self.file_path, targetFolder=self.folder_name, excluded='Metadaten_*')
        os.system(os_string)
            
    def delete(self):
        os.remove(self.file_path)
        

def proxy():
    import urllib2
    import requests
    import socks
    import socket #socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "proxy4.rz.uni-karlsruhe.de", 3128)
    socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "proxy.ira.uka.de", 3128)
    socket.socket = socks.socksocket
    #print urllib2.urlopen('http://www.google.com').read()
    #proxy_handler = urllib2.ProxyHandler({'ftp':'proxy4.rz.uni-karlsruhe.de:3128'})
    proxy_handler = urllib2.ProxyHandler({'ftp':'proxy.ira.uka.de:3128'})    
    opener = urllib2.build_opener(proxy_handler, urllib2.HTTPBasicAuthHandler(), urllib2.HTTPHandler, urllib2.HTTPSHandler, urllib2.FTPHandler)
    urllib2.install_opener(opener)
    opener.open('http://ftp-cdc.dwd.de')
    resp = urllib2.urlopen('http://ftp-cdc.dwd.de') #resp=urllib2.urlopen('http://google.com')
    print(resp)
#proxy()


if __name__ == '__main__':
    dwd_ftp = DWDFTP()
    dwd_ftp.set_target_folder('../../DataArchive/climate/')
    dwd_ftp.download_solar_files()