from energy_db.climate.db_table import StationTable


statTab = StationTable()

query = 'SELECT * FROM "stations"'
ret = statTab.execute_fetchall(query,())

for row in ret:
    stationID= row[0]
    name = row[4]
    strippedName = name.strip()
    query = 'UPDATE "stations" SET "station_name" = %s WHERE "station_id" = %s;'
    statTab.cursor.execute(query, (strippedName, stationID,))
    statTab.commit()
    

