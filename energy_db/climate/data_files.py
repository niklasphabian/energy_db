import csv
import pandas
import pytz
import datetime
from energy_db.climate.db_table import TemperatureTable, SolarTable, WindTable
import os
cet = pytz.timezone('CET')
utc = pytz.timezone('UTC')


class ClimateFile:
        
    def __init__(self, file_path):
        self.db_table = None
        self.station_id = None
        self.connect_db()
        self.file_path = file_path
        self.timestamp_list = []
        self.df = None
        if self.verify_name():
            self.parse_station_id()
            self.read()
           
    def move_file(self):
        file_name = self.file_path.rsplit('/', 1)[1]
        folder = self.file_path.rsplit('/', 1)[0]
        destination = folder + '/done/' + file_name
        os.rename(self.file_path, destination)

    def delete_file(self):
        os.remove(self.file_path)
        
    def verify_name(self):
        if self.file_path.find(self.key_word) >= 0:
            return True
        else:
            print('WARNING: Filename does not contain keyword')
            return False    
            
    def parse_station_id(self):
        self.station_id = int(self.file_path.split('_')[-1].split('.')[0])

    def upload_missing(self):
        last_entry = self.db_table.last_timestamp(self.station_id)
        start_date = last_entry - datetime.timedelta(days=7)
        self.df = self.df.loc[self.df.index > start_date]
        file_name = self.file_path.rsplit('/', 1)[1]
        print('uploading file {file_name} (start date: {start_date})'.format(start_date=start_date, file_name=file_name))
        self.db_table.upsert_dataframe(self.df)


class TemperatureFile(ClimateFile):
    key_word = '_tu_'

    def connect_db(self):
        self.db_table = TemperatureTable()
       
    def read(self):
        self.df = pandas.read_csv(self.file_path, sep=';')
        self.df.rename(columns={'MESS_DATUM': 'time_stamp',
                           'STATIONS_ID': 'station_id',
                           'TT_TU': 'air_temp',
                           'RF_TU': 'rel_humidity'}, inplace=True)
        self.df['time_stamp'] = pandas.to_datetime(self.df['time_stamp'], format='%Y%m%d%H', utc=True)
        self.df = self.df.set_index('time_stamp')


class WindFile(ClimateFile):
    key_word = '_ff_'

    def connect_db(self):
        self.db_table = WindTable()
        
    def read(self):
        self.df = pandas.read_csv(self.file_path, sep=';')
        self.df.rename(columns={'MESS_DATUM': 'time_stamp',
                           'STATIONS_ID': 'station_id',
                           '   F': 'wind_speed',
                           '   D': 'wind_direction'}, inplace=True)
        self.df['time_stamp'] = pandas.to_datetime(self.df['time_stamp'], format='%Y%m%d%H', utc=True)
        self.df = self.df.set_index('time_stamp')

    

class SolarFile(ClimateFile):
    key_word = '_st_'

    def connect_db(self):
        self.db_table = SolarTable()
            
    def read(self):
        self.df = pandas.read_csv(self.file_path, sep=';')
        self.df.rename(columns={'MESS_DATUM': 'time_stamp',
                           'STATIONS_ID': 'station_id',
                           'ATMO_LBERG': 'atmosphere',
                           'FD_LBERG': 'diffuse_power',
                           'FG_LBERG': 'global_power',
                           'SD_LBERG': 'sunshine_duration'}, inplace=True)
        self.df['time_stamp'] = pandas.to_datetime(self.df['time_stamp'], format='%Y%m%d%H:%M', utc=True)
        self.df = self.df.set_index('time_stamp')
