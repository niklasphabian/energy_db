import datetime
import pytz
import pandas
from energy_db.database.db_table import DBTable
cet = pytz.timezone('CET')


class ClimateTable(DBTable):

    def delete_if_already_exists(self, time_stamp, station_id):
        query = 'DELETE FROM "{schema_name}"."{table_name}"  \
                WHERE time_stamp = %s AND station_id = %s;'
        query = query.format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (time_stamp, station_id))
            
    def delete_row_pkey(self, pkey_list):
        query = 'DELETE FROM "{}" WHERE id = %s'
        query = query.format(table_name=self.table_name, schema_name=self.schema_name)
        for pkey in pkey_list:
            self.cursor.execute(query, (pkey,))
                
    def last_timestamp(self, station_id):
        query = 'SELECT time_stamp \
                FROM "{schema_name}"."{table_name}" \
                ORDER BY time_stamp ASC \
                LIMIT 1;'
        query = query.format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchone(query, (station_id,))
        if ret is None:
            start_day = cet.localize(datetime.datetime(2010, 1, 1))
            print('No entry for station {s_id}, using {date} as startdate'.format(date=start_day, s_id=station_id))
            return start_day
        else:
            return ret[0]

    def insert_dataframe(self, df):
            df.to_sql(name=self.table_name, con=self.database.engine, schema=self.schema_name, if_exists='append')


class TemperatureTable(ClimateTable):
    schema_name = 'climate'
    table_name = 'temperature_by_station'
    
    def get_time_series(self, start_time, end_time, station_id):
        query = 'SELECT time_stamp, air_temp \
                FROM "{schema_name}"."{table_name}" \
                WHERE time_stamp >= %s \
                AND time_stamp <= %s \
                AND station_id = %s \
                ORDER BY time_stamp ASC' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        return self.execute_fetchall(query, (start_time, end_time, station_id,))

    def get_time_series_pandas(self, start_time, end_time, station_id):
        ret = self.get_time_series(start_time, end_time, station_id)
        return pandas.DataFrame.from_records(ret, columns=['time_stamp', 'air_temp'], index='time_stamp')
           
    def insert_row(self, ts_id, station_id, air_temp, rel_humidity):
        self.delete_if_already_exists(ts_id, station_id)
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, station_id, air_temp, rel_humidity) \
                SELECT %s,%s,%s,%s'
        query = query.format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (ts_id, station_id, air_temp, rel_humidity,))

    def upsert_dataframe(self, df):
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, station_id, air_temp, rel_humidity) ' \
                'SELECT %s, %s, %s, %s'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        for row in df.iterrows():
            time_stamp = row[0]
            station_id = row[1]['station_id']
            air_temp = row[1]['air_temp']
            rel_humidity = row[1]['rel_humidity']
            self.delete_if_already_exists(time_stamp, station_id)
            self.database.cursor.execute(query, (time_stamp, station_id, air_temp, rel_humidity))
        self.commit()


class WindTable(ClimateTable):
    schema_name = 'climate'
    table_name = 'wind_by_station'

    def get_time_series(self, start_time, end_time, stationID):
        query = 'SELECT time_stamp, "wind_speed"  \
                FROM "{schema_name}"."{table_name}" \
                WHERE time_stamp >= %s \
                AND time_stamp <= %s \
                AND time_stamp.station_id = %s \
                ORDER BY time_stamp ASC'
        query = query.format(table_name=self.table_name, schema_name=self.schema_name)
        return self.execute_fetchall(query, (start_time, end_time, stationID,))

    def insert_row(self, time_stamp, station_id, wind_speed, wind_direction):
        self.delete_if_already_exists(time_stamp, station_id)
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, station_id, wind_speed, wind_direction) \
                SELECT %s,%s,%s,%s' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (time_stamp, station_id, wind_speed, wind_direction))

    def upsert_dataframe(self, df):
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, station_id, wind_speed, wind_direction)' \
                'SELECT %s, %s, %s, %s'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        for row in df.iterrows():
            time_stamp = row[0]
            station_id = row[1]['station_id']
            wind_speed = row[1]['wind_speed']
            wind_direction = row[1]['wind_direction']
            self.delete_if_already_exists(time_stamp, station_id)
            self.database.cursor.execute(query, (time_stamp, station_id, wind_speed, wind_direction))
        self.commit()


class SolarTable(ClimateTable):
    schema_name = 'climate'
    table_name = 'solar_by_station'
    
    def insert_row(self, time_stamp, station_id, sunshine_duration, diffuse_power, global_power, atmosphere):
        self.delete_if_already_exists(time_stamp, station_id)
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, station_id, sunshine_duration, diffuse_power, global_power, atmosphere) ' \
                'SELECT %s, %s, %s, %s, %s,% s'
        query = query.format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (time_stamp, station_id, sunshine_duration, diffuse_power, global_power, atmosphere,))

    def upsert_dataframe(self, df):
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, station_id, sunshine_duration, diffuse_power, global_power, atmosphere) ' \
                'SELECT %s, %s, %s, %s, %s, %s'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        for row in df.iterrows():
            time_stamp = row[0]
            station_id = row[1]['station_id']
            sun_duration = row[1]['sunshine_duration']
            diffuse_p = row[1]['diffuse_power']
            global_p = row[1]['global_power']
            atmosphere = row[1]['atmosphere']
            self.delete_if_already_exists(time_stamp, station_id)
            self.database.cursor.execute(query, (time_stamp, station_id, sun_duration, diffuse_p, global_p, atmosphere))
        self.commit()
        
        
class StationTable(DBTable):
    pass
