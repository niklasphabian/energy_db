import bs4
import requests
import datetime
import pandas
import matplotlib.pyplot as plt
from energy_db.regelleistung.tendered_table import TenderedTab


def download(start_date, reserve_type):
    product_dict = {'PRL': 1, 'SRL': 2, 'MRL': 3}
    payload = {'from': start_date.strftime('%d.%m.%Y'), 'productId': product_dict[reserve_type]}
    response = requests.post('https://www.regelleistung.net/ext/tender/', params=payload)    
    soup = bs4.BeautifulSoup(response.text, 'lxml')    
    table = soup.find(lambda tag: tag.name == 'table' and tag.has_attr('id') and tag['id'] == 'tender-table')
    if reserve_type == 'PRL':
        df = parse_prl(table, reserve_type)
    else:
        df = parse_mrl_srl(table, reserve_type)
    df.set_index('date', inplace=True)
    df = df.resample('W').mean()
    df = df.groupby(df.index).sum()
    df.index = df.index - datetime.timedelta(days=6)
    return df


def parse_mrl_srl(table, reserve_type):
    data = []
    for row in table.find_all('tr'):
        col = row.find_all('td')
        if col:
            date = datetime.datetime.strptime(col[0].getText()[9:19], '%d.%m.%Y')
            if reserve_type == 'SRL':
                date = date - datetime.timedelta(days=6)
            neg = float(col[3].getText())
            pos = float(col[4].getText())
            data.append([date, neg, pos])                
    df = pandas.DataFrame(data, columns=['date', 'Negative {}'.format(reserve_type), 'Positive {}'.format(reserve_type)])
    return df


def parse_prl(table, reserve_type):
    data = []
    for row in table.find_all('tr'):
        col = row.find_all('td')
        if col:        
            date = datetime.datetime.strptime(col[0].getText()[9:19], '%d.%m.%Y')-datetime.timedelta(days=6)
            negpos= float(col[3].getText())            
            data.append([date, negpos])                
    df = pandas.DataFrame(data, columns=['date', 'NegPos {}'.format(reserve_type)])
    return df


def plot():
    fig = plt.figure()
    ax = fig.add_subplot(111)
    date = datetime.datetime(2011, 1, 1)
    download(date, 'MRL').plot(ax=ax, linewidth=3, linestyle='-')
    download(date, 'SRL').plot(ax=ax, linewidth=3, linestyle='--')
    download(date, 'PRL').plot(ax=ax, linewidth=3, linestyle='-.')
    font_size = 14
    plt.ylim(0, 3500)
    plt.xlim(datetime.datetime(2011,2,1),datetime.datetime(2015,11,5))
    ax.tick_params(labelsize=font_size)
    plt.suptitle('Tendered Control Reserve', fontsize=font_size)
    plt.xlabel('Time', fontsize=font_size)
    plt.ylabel('Power in MW', fontsize=font_size)
    plt.show()

    
def update(start_date):
    prl = download(start_date, 'PRL')
    srl = download(start_date, 'SRL')
    mrl = download(start_date, 'MRL')
    df = pandas.concat([prl, srl, mrl], axis=1)
    db_tab = TenderedTab()
    db_tab.insert_df(df)


def download_file(start_date, rl_type):
    rl_type_dict = {'PRL': 1, 'SRL': 2, 'MRL': 3}
    url = 'https://www.regelleistung.net/ext/tender/download'
    payload = {'from': start_date.strftime('%d.%m.%Y'), 'productId': rl_type_dict[rl_type]}
    response = requests.post(url, params=payload)
    response.encoding = 'ISO-8859-1'
    fname_trunc = response.headers['content-disposition'].split('=')[1].replace('"','')
    fname = str(start_date) + '_' + fname_trunc 
    with open(fname, 'w') as outfile:
        outfile.write(response.text.encode('utf8').replace(' - ', ';'))
    df = pandas.read_csv(fname, delimiter=';', header=0, index_col=1, parse_dates=True )
    df.index.rename('time_stamp', inplace=True)
    print(df)    
    

    


