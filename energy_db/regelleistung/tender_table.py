from energy_db.database.db_table import DBTable
import datetime
import pandas
import pytz
cet = pytz.timezone('CET')


class RLTenderTable(DBTable):

    def to_pandas_ts(self, ret):
        new_data = []
        for row in ret:
            new_data.append([datetime.datetime.combine(row[0], datetime.time.min), row[1]])
        df = pandas.DataFrame(new_data, columns=['time', 'price'])
        df.set_index('time', inplace=True)
        return df 
     
    def remove_existing_rows(self, start_timestamp):
        self.delete_if_already_exists(start_timestamp)
    
    def get_max_accepted_cp(self, start_date, end_date, res_type):
        query = 'SELECT time_stamp, max(capacity_price) \
                    FROM "{schema_name}"."{table_name}" \
                    WHERE time_stamp >= %s \
                    AND time_stamp_end<= %s \
                    AND reserve_type = %s \
                    AND award = TRUE \
                    GROUP BY time_stamp \
                    ORDER by time_stamp '\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_date, end_date, res_type))
        ret = self.cursor.fetchall()
        return self.to_pandas_ts(ret)
    
    def get_min_accepted_c_p(self, start_date, end_date, res_type):
        query = 'SELECT time_stamp, min(capacity_price) \
                    FROM "{schema_name}"."{table_name}" \
                    WHERE time_stamp >= %s \
                    AND time_stamp_end<= %s \
                    AND reserve_type = %s \
                    AND award = TRUE \
                    GROUP BY time_stamp \
                    ORDER by time_stamp ' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_date, end_date, res_type))
        ret = self.cursor.fetchall()
        return self.to_pandas_ts(ret)
        
    def get_weight_average(self, start_date, end_date, res_type):
        query = 'SELECT ' \
                    'time_stamp, sum(capacity_price * offered_power)/sum(offered_power) \
                FROM ' \
                    '"{schema_name}"."{table_name}" \
                WHERE time_stamp >= %s \
                AND time_stamp_end<= %s \
                AND reserve_type = %s \
                AND award = TRUE \
                GROUP BY ' \
                    'time_stamp \
                ORDER BY ' \
                    'time_stamp '\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_date, end_date, res_type))
        ret = self.cursor.fetchall()
        return self.to_pandas_ts(ret)

    def latest_entry(self):
        query = 'SELECT max(time_stamp) FROM "{schema_name}"."{table_name}"'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.database.cursor.execute(query)
        latest_date = self.database.cursor.fetchone()[0]
        if latest_date is None:
            latest_date = cet.localize(datetime.datetime(2007, 11, 12))
        return latest_date
    
      
class RLTenderTableMRL(RLTenderTable):
    schema_name = 'regelleistung'
    table_name = 'tender_mrl'
    
    def add_record(self, start_time_stamp, end_time_stamp, res_type, time_slice,
                   power_price, energy_price, offered_power, award):
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, time_stamp_end, reserve_type, capacity_price, energy_price, offered_power, award) ' \
                'SELECT %s,%s,%s,%s,%s,%s,%s'
        query = query.format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_time_stamp, end_time_stamp, res_type,
                                    power_price, energy_price, offered_power, award,))
        
    def get_mol(self, timestamp, res_type):
        timestamp_end = timestamp-datetime.timedelta(hours=4)
        query = 'SELECT \
                    energy_price, offered_power \
                FROM "{schema_name}"."{table_name}" \
                WHERE \
                    award = TRUE AND \
                    reserve_type = %s AND \
                    time_stamp <= %s AND \
                    time_stamp >= %s \
                ORDER BY energy_price ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchall(query, (res_type, timestamp, timestamp_end))
        return ret

  
class RLTenderTableSRL(RLTenderTable):
    schema_name = 'regelleistung'
    table_name = 'tender_srl'
    
    def add_record(self, start_time_stamp, end_time_stamp, res_type,
                   time_slice, power_price, energy_price, offered_power, award):
        query = 'INSERT INTO \
                    "{schema_name}"."{table_name}" \
                    (time_stamp, time_stamp_end,reserve_type, time_period, \
                    capacity_price, energy_price, offered_power, award) \
                SELECT  \
                    %s,%s,%s,%s,%s,%s,%s,%s' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_time_stamp, end_time_stamp, res_type,
                                    time_slice, power_price, energy_price, offered_power, award,))
        
    def get_mol(self, timestamp, res_type, time_periode):
        query = 'SELECT \
                    energy_price, offered_power \
                FROM "{schema_name}"."{table_name}" \
                WHERE \
                    award = TRUE AND \
                    reserve_type = %s AND \
                    time_period = %s AND \
                    time_stamp <= %s AND \
                    time_stamp_end >= %s \
                ORDER BY energy_price ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchall(query, (res_type, time_periode, timestamp, timestamp))
        return ret
    
    
class RLTenderTablePRL(RLTenderTable):
    schema_name = 'regelleistung'
    table_name = 'tender_prl'
    
    def add_record(self, start_time_stamp, end_time_stamp, power_price, offered_power, award, accepted_power):
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                 '(time_stamp,time_stamp_end, capacity_price, offered_power, award, accepted_power) ' \
                 'SELECT %s,%s,%s,%s,%s,%s'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_time_stamp, end_time_stamp, power_price, offered_power, award, accepted_power))

    def get_max_accepted_cp(self, start_date, end_date, res_type):
        query = 'SELECT time_stamp, max(capacity_price) \
                    FROM "{schema_name}"."{table_name}" \
                    WHERE time_stamp >= %s \
                    AND time_stamp_end<= %s \
                    AND award = TRUE \
                    GROUP BY time_stamp \
                    ORDER by time_stamp ' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_date, end_date))
        ret = self.cursor.fetchall()
        return self.to_pandas_ts(ret)
    
    def get_min_accepted_c_p(self, start_date, end_date, res_type):
        query = 'SELECT time_stamp, min(capacity_price) \
                    FROM "{schema_name}"."{table_name}" \
                    WHERE time_stamp >= %s \
                    AND time_stamp_end<= %s \
                    AND award = TRUE \
                    GROUP BY time_stamp \
                    ORDER by time_stamp '\
            .format(tabName=self.table_name)
        self.cursor.execute(query, (start_date, end_date))
        ret = self.cursor.fetchall()
        return self.to_pandas_ts(ret)
        
    def get_weight_average(self, start_date, end_date, res_type):
        query = 'SELECT time_stamp, sum(capacity_price * offered_power)/sum(offered_power) \
                    FROM "{schema_name}"."{table_name}" \
                    WHERE time_stamp >= %s \
                    AND time_stamp_end<= %s \
                    AND award = TRUE \
                    GROUP BY time_stamp \
                    ORDER by time_stamp ' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_date, end_date))
        ret = self.cursor.fetchall()
        return self.to_pandas_ts(ret)
    
    def get_cp_mol(self, start_date):
        query = 'SELECT capacity_price, offered_power \
                    FROM "{schema_name}"."{table_name}" \
                    WHERE time_stamp < %s \
                    AND time_stamp_end> %s \
                    AND award = TRUE \
                    ORDER by capacity_price ' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start_date, start_date))
        ret = self.cursor.fetchall()
        return ret

