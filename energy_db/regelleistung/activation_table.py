from energy_db.database.db_table import DBTable
import pandas


class RLActivationTable(DBTable):

    def add_record(self, time_stamp, betr_neg, betr_pos, qual_neg, qual_pos):
        self.delete_if_already_exists(time_stamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" \
        (time_stamp, betr_neg,  betr_pos, qual_neg, qual_pos) \
        SELECT %s,%s,%s,%s, %s'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (time_stamp, betr_neg, betr_pos, qual_neg, qual_pos,))


class SRLActivationTableNRV(RLActivationTable):
    schema_name = 'regelleistung'
    table_name = 'srl_activation_nrv'

    def get_neg_ht(self, start, end):
        query = 'SELECT time_stamp, betr_neg \
                FROM "{schema_name}"."{table_name}"  \
                WHERE time_stamp >= %s AND time_stamp <= %s \
                AND DATE_PART(\'HOUR\', time_stamp) >= 8  \
                AND DATE_PART(\'HOUR\', time_stamp) < 20 \
                AND DATE_PART(\'DOW\', time_stamp) > 0 \
                AND DATE_PART(\'DOW\', time_stamp) < 6 \
                ORDER BY time_stamp ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start, end))
        ret = self.cursor.fetchall()
        df = pandas.DataFrame(ret, columns=['time_stamp', 'Dem'])
        df.set_index('time_stamp', inplace=True)        
        return df
        
    def get_neg_nt(self, start, end):
        query = 'SELECT time_stamp, betr_neg \
                FROM "{schema_name}"."{table_name}"  \
                WHERE time_stamp >= %s AND time_stamp <= %s AND ( \
                DATE_PART(\'HOUR\', time_stamp) < 08 OR \
                DATE_PART(\'HOUR\', time_stamp) >= 20 OR \
                DATE_PART(\'DOW\', time_stamp) = 0 OR \
                DATE_PART(\'DOW\', time_stamp) = 6  ) \
                ORDER BY time_stamp ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start, end))
        ret = self.cursor.fetchall()
        df = pandas.DataFrame(ret, columns=['time_stamp', 'Dem'])
        df.set_index('time_stamp', inplace=True)        
        return df

    def get_pos_ht(self, start, end):
        query = 'SELECT time_stamp, betr_pos \
                FROM "{schema_name}"."{table_name}"  \
                WHERE time_stamp >= %s AND time_stamp <= %s \
                AND DATE_PART(\'HOUR\', time_stamp) >= 8  \
                AND DATE_PART(\'HOUR\', time_stamp) < 20 \
                AND DATE_PART(\'DOW\', time_stamp) > 0 \
                AND DATE_PART(\'DOW\', time_stamp) < 6 \
                ORDER BY time_stamp ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start, end))
        ret = self.cursor.fetchall()
        df = pandas.DataFrame(ret, columns=['time_stamp','Dem'])                
        df.set_index('time_stamp', inplace=True)        
        return df
        
    def get_pos_nt(self, start, end):
        query = 'SELECT time_stamp, betr_pos \
                FROM "{schema_name}"."{table_name}"  \
                WHERE time_stamp >= %s AND time_stamp <= %s AND ( \
                DATE_PART(\'HOUR\', time_stamp) < 08 OR \
                DATE_PART(\'HOUR\', time_stamp) >= 20 OR \
                DATE_PART(\'DOW\', time_stamp) = 0 OR \
                DATE_PART(\'DOW\', time_stamp) = 6  ) \
                ORDER BY time_stamp ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start, end))
        ret = self.cursor.fetchall()
        df = pandas.DataFrame(ret, columns=['time_stamp','Dem'])                
        df.set_index('time_stamp', inplace=True)        
        return df

    
class MRLActivationTableNRV(RLActivationTable):
    schema_name = 'regelleistung'
    table_name = 'mrl_activation_nrv'
    
    def get_neg(self, start, end):
        query = 'SELECT time_stamp, betr_neg \
                FROM "{schema_name}"."{table_name}"  \
                WHERE time_stamp >= %s AND time_stamp <= %s \
                ORDER BY time_stamp ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start, end))
        ret = self.cursor.fetchall()
        df = pandas.DataFrame(ret, columns=['time_stamp', 'Dem'])
        df.set_index('time_stamp', inplace=True)        
        return df
    
    def get_pos(self, start, end):
        query = 'SELECT time_stamp, betr_pos \
                FROM "{schema_name}"."{table_name}"  \
                WHERE time_stamp >= %s AND time_stamp <= %s \
                ORDER BY time_stamp ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (start, end))
        ret = self.cursor.fetchall()
        df = pandas.DataFrame(ret, columns=['time_stamp', 'Dem'])
        df.set_index('time_stamp', inplace=True)        
        return df 