from bs4 import BeautifulSoup
import datetime
import pytz
from energy_db.regelleistung.activation_table import SRLActivationTableNRV
from energy_db.regelleistung.activation_table import MRLActivationTableNRV
import requests
import time

cet = pytz.timezone('CET')

amsterdamTZ = pytz.timezone('Europe/Amsterdam')
tso_id_dict = {'NRV': 6}


class RLDemand:

    def __init__(self):
        self.html = None
        self.table = None

    def download(self, date):
        payload = {'from': date.strftime('%d.%m.%Y'),
                   '_download': 'on',
                   'tsoId': tso_id_dict[self.tsoID],
                   'dataType': self.reserve_type}
        response = requests.post('https://www.regelleistung.net/ext/data/', params=payload)
        self.html = response.text
        self.html2table()
        self.adjust_style()

    def sleep(self, date):
        now = time.strftime("%Y-%m-%d %H:%M")
        date = date.strftime("%Y-%m-%d")
        print('{now}: Updated {rl_type} {date}'.format(now=now, rl_type=self.reserve_type, date=date))
        time.sleep(1)
            
    def load_html(self, html):
        self.html = html
        self.html2table()
        self.adjust_style()
                
    def adjust_style(self):
        self.modify_date()
        self.cleanup_data()
    
    def html2table(self):    
        soup = BeautifulSoup(self.html, 'lxml')
        html_table = soup.find(lambda tag: tag.name == 'table' and tag.has_attr('id') and tag['id'] == "data-table")
        
        self.table = []
        for row in html_table.findAll('tr'):
            temp_row = []
            for val in row.findAll('td'):
                value = val.text
                value = str(value)
                if value == '\n':
                    temp_row.append(' ')
                else:  
                    temp_row.append(value)            
            self.table.append(temp_row)        
        if not self.table[0]:
            self.table.pop(0)
        
    def cleanup_data(self):
        r = 0
        for row in self.table:
            c = 0
            for value in row:  
                if value == '-':                
                    self.table[r][c] = None
                else:                            
                    value = value.replace('.', '')
                    value = value.replace(',', '.')
                    value = value.replace('*', '')
                    self.table[r][c] = value
                c += 1
            r += 1
        
    def modify_date(self):                
        for r in range(len(self.table)):           
            my_date = datetime.datetime.strptime(self.table[r][0], '%d.%m.%Y')
            my_date = my_date.date().isoformat()
            self.table[r][0] = my_date
 
    def upload_table(self):
        first_two_passed = False
        dst = True
        for rows in self.table:
            date = datetime.datetime.strptime(rows[0], '%Y-%m-%d').date()
            start_time = datetime.datetime.strptime(rows[1][0:5], '%H:%M').time()
            if start_time == datetime.time(2, 0, 0):
                if first_two_passed:
                    dst = False
                else:
                    first_two_passed = True    
            timestamp = amsterdamTZ.localize(datetime.datetime.combine(date, start_time), is_dst=dst)
            betrNeg = rows[3]
            betrPos = rows[4]
            qualNeg = rows[5]
            qualPos = rows[6]
            self.dbTable.add_record(timestamp, betrNeg, betrPos, qualNeg, qualPos)
        self.dbTable.commit()
    
    def latest_entry(self):
        date = self.dbTable.latest_entry()
        if date is None:
            date = cet.localize(datetime.datetime(2012, 1, 1))
        return date


class SRLActivationNRV(RLDemand):
    dbTable = SRLActivationTableNRV()
    reserve_type = 'SRL'
    tsoID = 'NRV'


class MRLActivationNRV(RLDemand):
    dbTable = MRLActivationTableNRV()
    reserve_type = 'MRL'
    tsoID = 'NRV'


def update_srl():
    srl_demand_nrv = SRLActivationNRV()
    date = srl_demand_nrv.latest_entry()
    while date < cet.localize(datetime.datetime.now()):        
        srl_demand_nrv.download(date)
        srl_demand_nrv.upload_table()
        srl_demand_nrv.sleep(date)
        date = date + datetime.timedelta(days=1)


def update_mrl():
    mrl_demand_nrv = MRLActivationNRV()
    date = mrl_demand_nrv.latest_entry()
    while date < cet.localize(datetime.datetime.now()):
        mrl_demand_nrv.download(date)
        mrl_demand_nrv.upload_table()
        mrl_demand_nrv.sleep(date)
        date = date + datetime.timedelta(days=1)


def test():
    date = datetime.datetime(2011, 6, 27)

    mrl_demand_nrv = MRLActivationNRV()
    mrl_demand_nrv.download(date)
    mrl_demand_nrv.upload_table()

    srl_demand_nrv = SRLActivationNRV()
    srl_demand_nrv.download(date)
    srl_demand_nrv.upload_table()

#test()

