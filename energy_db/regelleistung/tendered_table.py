'''
Created on Apr 29, 2016

@author: EIFERKITEDU\griessbaum
'''

from energy_db.database.db_table import DBTable
import pandas
import datetime


class TenderedTab(DBTable):
    schema_name = 'regelleistung'
    table_name = 'tendered'

    def get_df(self, start_date, end_date):
        query = 'SELECT time_stamp, negpos_prl, neg_srl, pos_srl, neg_mrl, pos_mrl \
                FROM "{schema_name}"."{table_name}" \
                WHERE time_stamp >= %s AND time_stamp <= %s \
                ORDER BY time_stamp ASC;'.\
            format(schema_name=self.schema_name, table_name=self.table_name)
        self.cursor.execute(query, (start_date, end_date))
        ret = self.cursor.fetchall()
        df = self.to_dataframe(ret)
        return df
        
    def to_dataframe(self, ret):
        new_data = []
        for row in ret :
            row = list(row)
            row[0] = datetime.datetime.combine(row[0], datetime.datetime.min.time())
            new_data.append(row)
        df = pandas.DataFrame(new_data, columns=['time_stamp', 'negpos_prl', 'neg_srl', 'pos_srl', 'neg_mrl', 'pos_mrl'])
        df.set_index('time_stamp', inplace=True)
        return df 
    
    def insert_df(self, df):
        for row in df.itertuples():
            self.insert_row(row)
        self.commit()
            
    def insert_row(self, row):
        timestamp = row[0]
        self.delete_if_already_exists(timestamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" \
        (time_stamp, negpos_prl, neg_srl, pos_srl, neg_mrl, pos_mrl) \
        SELECT %s,%s,%s,%s,%s,%s'.\
            format(schema_name=self.schema_name, table_name=self.table_name)
        self.cursor.execute(query, row)

