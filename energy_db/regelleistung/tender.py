import csv
import datetime
from energy_db.regelleistung.tender_table import RLTenderTableMRL, RLTenderTableSRL, RLTenderTablePRL
import requests
import io
import pytz
import time

utc = pytz.timezone('UTC')
url_trunk = 'https://www.regelleistung.net/ext/tender/'


def energy_price_cast(energy_price, payment_direction):
    payment_direction = payment_direction.lower().replace('_', ' ')
    if payment_direction == 'netz an anbieter':
        energy_price_signed = float(energy_price)
    elif payment_direction == 'anbieter an netz':
        energy_price_signed = float(energy_price) * -1.0
    return energy_price_signed


def award_cast(award):
    return award == 'yes' or award == 'ja'


def one_column(table):
    row = next(table)
    if len(row[0]) == 10:
        return False
    elif len(row[0]) == 19:
        return True


class TenderFile:
    def __init__(self):
        self.startTimeStamps = []
        self.endTimeStamps = []
        self.reserveType = []
        self.powerPrice = []
        self.energyPrice = []
        self.offeredPower = []
        self.award = []
        self.timeSlice = []
        self.acceptedPower = []
        self.file_name = None
        self.dataset = None
        self.session = requests.Session()
        self.session.headers.update({'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0',
                                     'Referer': 'https://www.regelleistung.net/ext/tender/',
                                     'Upgrade-Insecure-Requests': '1'})

    def download(self, date):
        n = self.dataset_number(date)
        url = url_trunk + 'results/anonymousdownload/{n}'.format(n=n)
        remote_file = self.session.get(url)
        if remote_file.status_code == 200:
            self.dataset = io.StringIO(remote_file.text)
            self.file_name = remote_file.headers.get('content-disposition').split('=')[1].strip('"')
            now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            print(now + ': Downloaded {file_name}'.format(file_name=self.file_name))

    def dataset_number(self, date):
        payload = {'from': date.strftime('%d.%m.%Y'), 'productId': str(self.rl_type)}
        success = False
        while not success:
            try:
                r = self.session.post(url_trunk, params=payload)
                dataset_num = int(r.text.split('/ext/tender/results/anonymousdownload/')[1].split('"')[0])
                success = True
            except requests.exceptions.ReadTimeout:
                print('timeout. retrying')
                time.sleep(10)
            except requests.exceptions.ConnectionError:
                print('Connection Error. retrying')
                time.sleep(10)
        return dataset_num

    def parse(self):
            csv_reader = csv.reader(self.dataset, delimiter=';')
            next(csv_reader)  # skip header
            if one_column(csv_reader):
                for row in csv_reader:
                    date_str = row[0]
                    product = row[1]
                    energy_price = row[3]
                    pay_dir = row[4]
                    energy_price_signed = energy_price_cast(energy_price.replace(',', '.'), pay_dir)
                    self.startTimeStamps.append(self.start_time_stamp(date_str, product))
                    self.endTimeStamps.append(self.end_time_stamp(date_str, product))
                    self.reserveType.append(product[0:3])
                    self.timeSlice.append(product[4:6])
                    self.powerPrice.append(row[2].replace(',', '.'))
                    self.energyPrice.append(energy_price_signed)
                    self.offeredPower.append(row[5].replace(',', '.'))
                    self.award.append(award_cast(row[6]))
            else:
                for row in csv_reader:
                    date_str = '{}{}{}'.format(row[0][0:6], ' - ', row[1])
                    res_type = row[2][0:3]
                    time_slice = row[2][4:6]
                    hour_start = time_slice
                    hour_end = row[2][7:9]
                    energy_price = row[4]
                    pay_dir = row[5]
                    energy_price_signed = energy_price_cast(energy_price.replace(',', '.'), pay_dir)
                    self.startTimeStamps.append(self.start_time_stamp(date_str, hour_start))
                    self.endTimeStamps.append(self.end_time_stamp(date_str, hour_end))
                    self.reserveType.append(res_type)
                    self.timeSlice.append(time_slice)
                    self.powerPrice.append(row[3].replace(',', '.'))
                    self.energyPrice.append(energy_price_signed)
                    self.offeredPower.append(row[6].replace(',', '.'))
                    self.award.append(award_cast(row[7]))

    def drop_rows(self, db_table):
        unique_ts = set(self.startTimeStamps)
        for timeStamp in unique_ts:
            db_table.remove_existing_rows(timeStamp)
        db_table.commit()

    def upload(self, db_table):
        self.drop_rows(db_table)
        r = 0
        for timeStamp in self.startTimeStamps:
            db_table.add_record(timeStamp, self.endTimeStamps[r], self.reserveType[r], self.timeSlice[r],
                                self.powerPrice[r], self.energyPrice[r], self.offeredPower[r], self.award[r])
            r += 1
        db_table.commit()


class TenderFileMRL(TenderFile):
    rl_type = 3

    def start_time_stamp(self, date_str, hour):
        date = datetime.datetime.strptime(date_str[9:19], '%d.%m.%Y').date()
        hour_start = datetime.time(hour=int(hour))
        time_stamp_start = datetime.datetime.combine(date, hour_start)
        time_stamp_start = utc.localize(time_stamp_start)
        return time_stamp_start

    def end_time_stamp(self, date_str, hour):
        date = datetime.datetime.strptime(date_str[9:19], '%d.%m.%Y').date()
        hour_end = datetime.time(hour=int(hour) - 1, minute=59)
        time_stamp_end = datetime.datetime.combine(date, hour_end)
        time_stamp_end = utc.localize(time_stamp_end)
        return time_stamp_end


class TenderFileSRL(TenderFile):
    dbTable = RLTenderTableSRL()
    rl_type = 2

    def start_time_stamp(self, date_str, hour=None):
        day = int(date_str[0:2])
        month = int(date_str[3:5])
        year = int(date_str[15:19])
        time_stamp_start = datetime.datetime(year, month, day)
        time_stamp_start = utc.localize(time_stamp_start)
        if time_stamp_start > self.end_time_stamp(date_str):
            time_stamp_start = datetime.datetime(time_stamp_start.year - 1, time_stamp_start.month, time_stamp_start.day)
        return time_stamp_start

    def end_time_stamp(self, date_str, hour=None):
        time_stamp_end = datetime.datetime.strptime(date_str[9:19], '%d.%m.%Y')
        time_stamp_end = utc.localize(time_stamp_end)
        return time_stamp_end


class TenderFilePRL(TenderFileSRL):
    dbTable = RLTenderTablePRL()
    rl_type = 1

    def parse(self):
        csv_reader = csv.reader(self.dataset, delimiter=';')
        next(csv_reader)  # skip header

        if one_column(csv_reader):
            for row in csv_reader:
                date_str = row[0]
                product = None
                self.startTimeStamps.append(self.start_time_stamp(date_str, product))
                self.endTimeStamps.append(self.end_time_stamp(date_str, product))
                self.powerPrice.append(row[2].replace(',', '.'))
                self.offeredPower.append(row[3].replace(',', '.'))
                self.award.append(True)
                try:
                    self.acceptedPower.append(float(row[4].replace(',', '.')))
                except:
                    self.acceptedPower.append(row[3].replace(',', '.'))
        else:
            for row in csv_reader:
                date_str = '{}{}{}'.format(row[0][0:6], ' - ', row[1])
                product = None
                self.startTimeStamps.append(self.start_time_stamp(date_str, product))
                self.endTimeStamps.append(self.end_time_stamp(date_str, product))
                self.powerPrice.append(row[3].replace(',', '.'))
                self.offeredPower.append(row[4].replace(',', '.'))
                self.award.append(True)
                try:
                    self.acceptedPower.append(float(row[5].replace(',', '.')))
                except:
                    self.acceptedPower.append(row[4].replace(',', '.'))

    def upload(self, db_table):
        self.drop_rows(db_table)
        r = 0
        for timeStamp in self.startTimeStamps:
            db_table.add_record(timeStamp,
                                self.endTimeStamps[r],
                                self.powerPrice[r],
                                self.offeredPower[r],
                                self.award[r], self.acceptedPower[r])
            r += 1
        db_table.commit()


def upsert_range(start_date, end_date, db_table, tender_file):
    date = start_date
    while date < end_date:
        if date.isoweekday() == 1 or isinstance(tender_file, TenderFileMRL):
            tender_file.__init__()
            tender_file.download(date)
            tender_file.parse()
            tender_file.upload(db_table)
        date += datetime.timedelta(days=1)


def update_to_today(db_table, tender_file):
    latest_entry = db_table.latest_entry()
    now = utc.localize(datetime.datetime.now())
    upsert_range(start_date=latest_entry, end_date=now, db_table=db_table, tender_file=tender_file)


def update_to_today_mrl():
    db_table = RLTenderTableMRL()
    tender_file = TenderFileMRL()
    update_to_today(db_table, tender_file)


def update_to_today_srl():
    db_table = RLTenderTableSRL()
    tender_file = TenderFileSRL()
    update_to_today(db_table, tender_file)


def update_to_today_prl():
    db_table = RLTenderTablePRL()
    tender_file = TenderFilePRL()
    update_to_today(db_table, tender_file)


def test():
    start_date = utc.localize(datetime.datetime(2007, 11, 11))
    end_date = utc.localize(datetime.datetime(2007, 11, 20))
    db_table = RLTenderTableMRL()
    tender_file = TenderFileMRL()
    upsert_range(start_date, end_date, db_table, tender_file)


if __name__ == '__main__':
    test()
