'''
Created on Jun 8, 2015

@author: griessbaum
'''

from energy_db.database.db_table import DBTable


class EEXEmissionTable(DBTable):
    schema_name = 'eex'
    table_name = 'emission_allowances'
    
    def add_record(self, timeStamp, setPrice, lowPrice, higPrice, opePrice, cloPrice, volTotal):
        self.delete_if_already_exists(timeStamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" \
        (time_stamp, settlement_price, low_price, high_price, opening_price, closing_price, volume_total) \
        SELECT %s,%s,%s,%s,%s,%s,%s'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (timeStamp, setPrice, lowPrice, higPrice, opePrice, cloPrice, volTotal,))
