'''
Created on Feb 25, 2015

@author: griessbaum
'''
import requests
import time


class EEXPage:
    def __init__(self, date):
        self.year = None
        self.month = None
        self.day = None
        self.url = None
        self.session = requests.Session()
        self.session.headers.update({'Referer': self.urlRefer})
        self.parse_date(date)
    
    def parse_date(self, date):
        self.year = str(date.year)
        self.month = '{:02d}'.format(date.month)
        self.day = '{:02d}'.format(date.day)
    
    def generate_url(self):
        self.url = self.urlTrunk + self.year + '/' + self.month + '.' + self.day + '.json'
        
    def download(self):
        self.generate_url()
        fail = True
        while fail is True:
            try:
                payload = self.session.get(self.url)                                              
                fail = False 
            except :
                tFail = 60 
                print('DL fail. Try again in %s sec' %tFail) 
                time.sleep(tFail)
        payload.encoding = 'utf-8'
        return payload.json()    
    
    
class Phelix(EEXPage):   
    urlRefer = 'http://www.eex.com/en/market-data/power/derivatives-market/phelix-futures'
    urlTrunk = 'http://www.eex.com/data/view/data/detail/phelix-power-futures/'
              
    
class DayAhead(EEXPage):
    urlRefer = 'http://www.eex.com/en/market-data/power/spot-market/auction'
    urlTrunk = 'http://www.eex.com/data/view/data/detail/power-auction-spot-v2/'
    

class EUA(EEXPage):
    urlRefer = 'http://www.eex.com/en/market-data/emission-allowances/spot-market/european-emission-allowances'
    urlTrunk = 'http://www.eex.com/data/view/data/detail/emission-eua-spot-v2/'

