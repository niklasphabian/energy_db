import energy_db.eex.eex_webpage as webpage
import energy_db.epex_spot.db_table as table

class DataSet():
    tabHourDE = table.DBTableHourDE()
    tabHourFR = table.DBTableHourFR()
    tabHourCH = table.DBTableHourCH()
    tabBasePeakDE = table.DBTableBasePeakDE()
    tabBasePeakFR = table.DBTableBasePeakFR()
    tabBasePeakCH = table.DBTableBasePeakCH()
    tabBlocksDE = table.DBTableBlocksDE()
    tabBlocksFR = table.DBTableBlocksFR()
    tabBlocksCH = table.DBTableBlocksCH()
    
    def __init__(self, date):
        self.date = date
        
    def download(self):    
        dataSet = webpage.DayAhead(self.date)
        self.payload = dataSet.download()
            
    def parseAndUpload(self):         
        for dataSet in self.payload['data']:            
            identifier = dataSet['identifier']
            if identifier == 'I-Power-S-DEAT-Hour':
                self.uploadHour(dataSet, self.tabHourDE)
            elif identifier == 'I-Power-S-FR-Hour':
                self.uploadHour(dataSet, self.tabHourFR)                
            elif identifier == 'I-Power-S-CH-Hour':
                self.uploadHour(dataSet, self.tabHourCH)
            elif identifier == 'I-Power-S-DEAT-Index':
                self.uploadIndex(dataSet, self.tabBasePeakDE)
            elif identifier == 'I-Power-S-FR-Index':
                self.uploadIndex(dataSet, self.tabBasePeakFR)
            elif identifier == 'I-Power-S-CH-Index':
                self.uploadIndex(dataSet, self.tabBasePeakCH)
            elif identifier == 'I-Power-S-DEAT-Block':
                self.uploadOffPeaks(dataSet, self.tabBlocksDE)
            elif identifier == 'I-Power-S-FR-Block':
                self.uploadOffPeaks(dataSet, self.tabBlocksFR)
            elif identifier == 'I-Power-S-CH-Block':
                self.uploadOffPeaks(dataSet, self.tabBlocksCH)
                
    def uploadHour(self, dataSet, dbTable):
        for row in dataSet['rows']:            
            timeStamp = row['data']['deliveryPeriodStart']            
            price = row['data']['indexPrice']
            volume = row['data']['volumeExchange']            
            dbTable.insert_row(timeStamp, price, volume)
        dbTable.commit()        
            
    def uploadIndex(self, dataSet, dbTable):
        timeStamp = dataSet['rows'][0]['data']['deliveryPeriodStart']                            
        volumeBase = dataSet['rows'][0]['data']['volumeExchange']
        priceBase = dataSet['rows'][0]['data']['indexPrice']
        volumePeak = dataSet['rows'][1]['data']['volumeExchange']
        pricePeak = dataSet['rows'][1]['data']['indexPrice']
        dbTable.insert_row(timeStamp, priceBase, volumeBase, pricePeak, volumePeak)
        dbTable.commit()
    
    def uploadOffPeaks(self, dataSet, dbTable):
        timeStamp = dataSet['rows'][0]['data']['deliveryPeriodStart']
        offPeak1 = dataSet['rows'][0]['data']['indexPrice']
        offPeak2 = dataSet['rows'][1]['data']['indexPrice']
        dbTable.injectOffPeaks(timeStamp, offPeak1, offPeak2)
        dbTable.commit()
