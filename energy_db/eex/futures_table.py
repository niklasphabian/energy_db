'''
Created on Feb 25, 2015

@author: griessbaum
'''


from energy_db.database.db_table import DBTable


class EEXFutureSetPrices(DBTable):
    table_name = 'future_set_prices'
    schema_name = 'eex'
    
    def add_record(self, time_stamp, wBase, wPeak, weBase, wePeak):
        self.delete_if_already_exists(time_stamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" \
        (time_stamp, next_week_base,  next_week_peak, next_we_base, next_we_peak) \
        SELECT %s,%s,%s,%s,%s'
        query = query.format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (time_stamp, wBase, wPeak, weBase, wePeak))
        self.commit()
