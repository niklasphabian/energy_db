import datetime
from energy_db.eex.futures_table import EEXFutureSetPrices
import energy_db.eex.eex_webpage as webpage


class DataSet:
    def __init__(self, date):
        self.date = date
        self.payload = None
        self.week_base = None
        self.week_peak = None
        self.weekend_base = None
        self.weekend_peak = None
        
    def download(self):    
        phelix = webpage.Phelix(self.date)
        try:
            self.payload = phelix.download()
        except:
            print('No data')
                    
    def parse(self):
        for row in self.payload['data']:
            if 'Base-Day' in row['identifier'] and len(row['rows']) > 0:
                if 'settlementPrice' in row['rows'][0]['data']:
                    self.week_base = row['rows'][0]['data']['settlementPrice']
            elif 'Peak-Day' in row['identifier'] and len(row['rows']) > 0:
                if 'settlementPrice' in row['rows'][0]['data']:
                    self.week_peak = row['rows'][0]['data']['settlementPrice']
            elif 'Base-Weekend' in row['identifier'] and len(row['rows']) > 0:
                if 'settlementPrice' in row['rows'][0]['data']:
                    self.weekend_base = row['rows'][0]['data']['settlementPrice']
            elif 'Peak-Weekend' in row['identifier'] and len(row['rows']) > 0:
                if 'settlementPrice' in row['rows'][0]['data']:
                    self.weekend_peak = row['rows'][0]['data']['settlementPrice']
        
    def upload(self):
        db_table = EEXFutureSetPrices()
        db_table.add_record(self.date, self.week_base, self.week_peak, self.weekend_base, self.weekend_peak)
        

def update_to_today():
    tab = EEXFutureSetPrices()
    start_date = tab.latest_entry()
    if start_date is None:
        start_date = datetime.datetime(2010, 1, 3)
    end_date = datetime.datetime.now()
    date = start_date
    while date < end_date:
        print(date)
        my_ds = DataSet(date)
        my_ds.download()
        if my_ds.payload:
            my_ds.parse()
            my_ds.upload()
        date = date + datetime.timedelta(days=1)


if __name__ == '__main__':
    update_to_today()
