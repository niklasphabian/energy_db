import energy_db.eex.eex_webpage as webpage
from energy_db.eex.emission_allowance_table import EEXEmissionTable
import datetime
import time
import random


class DataSet:
    def __init__(self, date):
        self.json = None
        self.downloaded = False
        self.date = date

    def download(self):
        allowance_page = webpage.EUA(self.date)
        try:
            self.json = allowance_page.download()
            self.downloaded = True
        except:
            print('No data available for {}'.format(self.date))

    def parse_data(self):
        data = self.json['data'][0]['rows'][-1]['data']
        try:
            self.setPrice = data['settlementPrice']
        except KeyError:
            self.setPrice = None
            print('No settlement price provided')
        
        try:
            self.lowPrice = data['lowPrice']
            self.higPrice = data['highPrice']
            self.opePrice = data['openingPrice']
            self.cloPrice = data['closingPrice']
            self.volTotal = data['volumeTotal']
        except:
            print('Only settlement Price available')
            self.lowPrice = 0
            self.higPrice = 0
            self.opePrice = 0
            self.cloPrice = 0
            self.volTotal = 0                            
        
    def upload(self, db_table):
        db_table.add_record(self.date, self.setPrice, self.lowPrice, self.higPrice, self.opePrice, self.cloPrice, self.volTotal)
        db_table.commit()
        

def sleep(day):
    now = time.strftime("%Hh%M")
    t_sleep = int(round(1 * random.random(), 0))
    print(now + ": Downloading %s and then Sleeping for %s sec" % (str(day), t_sleep))
    time.sleep(t_sleep)


def update_to_today():
    emission_table = EEXEmissionTable()
    start_date = emission_table.latest_entry()
    if start_date is None:
        start_date = datetime.date(2010, 1, 1)
    end_date = datetime.datetime.now().date()-datetime.timedelta(days=2)
    date = start_date

    while date <= end_date:
        my_data = DataSet(date)
        my_data.download()
        if my_data.downloaded:
            my_data.parse_data()
            my_data.upload(emission_table)
        date = date + datetime.timedelta(days=1)
        sleep(date)



        


