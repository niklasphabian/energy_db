from energy_db.plz.plz_table import PLZTable
import csv


dbtable = PLZTable()


def read_areas():
    dbtable.wipe()
    dbtable.commit()
    with open('/home/likewise-open/EIFERKITEDU/griessbaum/workspace/energyDB/DataArchive/Stations2PP/plz_areas.csv') as infile:
        reader = csv.reader(infile)
        next(reader)
        for row in reader:
            plz = row[0]
            area = row[2]
            dbtable.insert_row(plz, area)
    dbtable.commit()


def read_inhabitants():
    with open('/home/likewise-open/EIFERKITEDU/griessbaum/workspace/energyDB/DataArchive/Stations2PP/plz_einwohner.csv') as infile:
        reader = csv.reader(infile)
        next(reader)
        for row in reader:
            plz = int(row[0])
            inhabitants = row[1]
            dbtable.update_inhabitants(inhabitants, plz)
    dbtable.commit()


def read_stations():
    with open('/home/likewise-open/EIFERKITEDU/griessbaum/workspace/energyDB/DataArchive/Stations2PP/plz_to_station.csv') as infile:
        reader = csv.reader(infile)
        header = next(reader)
        for row in reader:
            plz = row[0]
            stationID = row[1]
            dbtable.update_station(stationID, plz)
    dbtable.commit()
    
read_areas()
read_inhabitants()
read_stations()

