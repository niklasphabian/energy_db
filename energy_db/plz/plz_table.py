from energy_db.database.db_table import DBTable

class PLZTable(DBTable):
    table_name = 'station2plz'
    
    def wipe(self):
        self.cursor.execute('DELETE FROM "{}"'.format(self.table_name))
        
    def insert_row(self, plz, area):
        if self.execute_fetchone('SELECT postleitzahl from "{}" WHERE postleitzahl=%s'.format(self.table_name), argList=(plz,)) is None: 
            self.cursor.execute('INSERT INTO "{}" (area, postleitzahl, inhabitants) VALUES (%s, %s, 0);'.format(self.table_name), (area,plz))
        else :
            self.cursor.execute('UPDATE "{}" SET area = area + %s WHERE postleitzahl = %s;'.format(self.table_name), (area, plz))
            
    def update_station(self, stationID, plz):        
        self.cursor.execute('UPDATE "{}" SET "ClosestClimateStationID" = %s WHERE postleitzahl = %s;'.format(self.table_name), (stationID, plz))

    def update_inhabitants(self, inhabitants, plz):
        self.cursor.execute('UPDATE "{}" SET inhabitants = inhabitants + %s WHERE postleitzahl = %s;'.format(self.table_name), (inhabitants, plz))
        
