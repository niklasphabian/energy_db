from energy_db.database.database import Database


class DBTable:
    def __init__(self):
        self.database = Database()
        self.cursor = self.database.cursor
        self.dict_cursor = self.database.dict_cursor
    
    def drop_table(self):
        drop_query = 'DROP TABLE IF EXISTS "{schema_name}"."{table_name}"'.format(self.table_name)
        self.database.cursor.execute(drop_query)
        self.commit()        

    def finish(self):
        self.database.commit()
        self.database.disconnect()
    
    def commit(self):
        self.database.commit()
        
    def latest_entry(self):
        query = 'SELECT max(time_stamp) FROM "{schema_name}"."{table_name}"'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.database.cursor.execute(query)
        latest_date = self.database.cursor.fetchall()[0][0]
        return latest_date
        
    def time_stamp_does_exist(self, time_stamp):
        query = 'SELECT time_stamp FROM "{schema_name}"."{table_name}" WHERE time_stamp = %s' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.database.cursor.execute(query, (time_stamp,))
        ret = self.database.cursor.fetchall()
        if len(ret) == 0:
            return False
        else:
            return True
        
    def delete_row_ts(self, time_stamp):
        query = 'DELETE FROM "{schema_name}"."{table_name}" ' \
                'WHERE "{schema_name}"."{table_name}".time_stamp = %s' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (time_stamp,))
               
    def delete_if_already_exists(self, time_stamp):
        if self.time_stamp_does_exist(time_stamp):
            self.delete_row_ts(time_stamp)
        
    def execute_fetchone(self, query, args=None):
        self.cursor.execute(query, args)
        return self.cursor.fetchone()  
    
    def execute_fetchall(self, query, args=None):
        self.cursor.execute(query, args)
        return self.cursor.fetchall()         
            
    def wipe(self):
        self.cursor.execute('DELETE FROM "{schema_name}"."{table_name}"'.format(table_name=self.table_name))
