import configparser
import psycopg2
import psycopg2.extras
import sqlalchemy


class Database:
    def __init__(self):
        self.user = None
        self.pwd = None
        self.con = None
        self.cursor = None
        self.dict_cursor = None
        self.host = None
        self.db_name = None
        self.uri = None
        self.engine = None
        self.load_config()       
        self.connect()
        self.make_engine()
        
    def load_config(self):        
        config = configparser.ConfigParser(allow_no_value=True)
        config.optionxform = str        
        config.read('database/database_config.ini')
        host_name = config.sections()[0]
        
        self.host = config[host_name]['host']
        self.user = config[host_name]['user']
        self.pwd = config[host_name]['pwd']
        self.db_name = config[host_name]['database']        
          
    def connect(self):        
        connection_str = "host='{}' dbname='{}' user='{}' password='{}'".format(self.host, self.db_name, self.user, self.pwd)
        self.con = psycopg2.connect(connection_str)
        self.cursor = self.con.cursor()   
        self.dict_cursor = self.con.cursor(cursor_factory=psycopg2.extras.DictCursor)

    def make_engine(self):
        self.uri = 'postgresql+psycopg2://{user}:{pwd}@{host}/{db_name}'
        self.uri = self.uri.format(user=self.user, pwd=self.pwd, host=self.host, db_name=self.db_name)
        self.engine = sqlalchemy.create_engine(self.uri)
        
    def disconnect(self):        
        self.con.close()
        
    def commit(self):
        self.con.commit()
        
    def execute(self, query):
        self.cursor.execute(query)
        
    def execute_fetchone(self, query, arg_list=None):
        self.cursor.execute(query, arg_list)
        ret = self.cursor.fetchone()
        return ret        
    
    def execute_fetchall(self, query, arg_list=None):
        self.cursor.execute(query, arg_list)
        ret = self.cursor.fetchall()
        return ret        
