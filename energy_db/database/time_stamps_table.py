from database.db_table import DBTable


class TimeStampTable(DBTable):

    def query_time_stamp(self, time_stamp):
        query = 'SELECT time_id FROM {schema_name}.{table_name} WHERE time_stamp = (%s)'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchone(query, (time_stamp,))        
        if ret is None:
            return None
        else: 
            return ret[0]        
    
    def add_time_stamp(self, time_stamp):
        query = 'INSERT INTO {schema_name}.{table_name} (time_stamp) SELECT %s' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (time_stamp,))
        self.commit()
        return self.query_time_stamp(time_stamp)        
    
    def get_time_stamp_id(self, time_stamp):
        ts_id = self.query_time_stamp(time_stamp)
        if ts_id is None:
            ts_id = self.add_time_stamp(time_stamp)
        return ts_id
    
    def create_index(self):
        self.cursor.execute('CREATE INDEX tsIDX ON {schema_name}.{table_name} (time_stamp);')\
            .format(table_name=self.table_name, schema_name=self.schema_name)


class TimeStampTableBlockwise(TimeStampTable):
    schema_name = 'blockwise'
    table_name = 'time_stamps'


class TimeStampTableClimate(TimeStampTable):
    schema_name = 'climate'
    table_name = 'time_stamps'
