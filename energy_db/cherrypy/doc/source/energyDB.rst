.. webinterface documentation master file, created by
   sphinx-quickstart on Wed Jul 29 15:41:46 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to webinterface's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: test 
.. automodule:: treeview_interface
.. autoclass:: WebInterface 

    .. automethod:: default(self)
    .. automethod:: index(self)
    .. automethod:: epex_spot(self)
    .. automethod:: activation_high_res()

    
    

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

