function options_export() {
    document.getElementById("data_export").style.color = "#000000";
    document.getElementById("data_howtouse").style.color = "#909090";
    document.getElementById("data_print").style.color = "#909090";
    document.getElementById("select_options").innerHTML="<form method='post' action='set_params_csv/' id='select_options'>Delimiter : <input type='text' name='delimiter_to_set'/> Date format : <input type='text' name='date_format_to_set'/> <button type='submit'>Set options</button><a href='javascript:print_csvdownload();'>Reset link</a>";
}

function options_howtouse() {
    document.getElementById("data_export").style.color = "#909090";
    document.getElementById("data_howtouse").style.color = "#000000";
    document.getElementById("data_print").style.color = "#909090";
    document.getElementById("select_options").innerHTML="<table id='select_options'><tr><td><form action='javascript:csvexport_howto()'><input type='submit' value='CSV export and its options'></form></td><td><form action='javascript:printoptions_howto()'><input type='submit' value='Print data options'></form></td></tr></table>";
}

function options_print() {
    document.getElementById("data_export").style.color = "#909090";
    document.getElementById("data_howtouse").style.color = "#909090";
    document.getElementById("data_print").style.color = "#000000";
    document.getElementById("select_options").innerHTML="<form method='post' action='set_params_print/' id='select_options'>Excluded plants : <input type='text' name='excluded_plants_to_set'/> Date format : <input type='text' name='date_format_to_set'/> <button type='submit'>Set options</button> <a href='javascript:reset_plot();'>Reset plot</a>";
}

function csvexport_howto() {
    document.getElementById("page_content").innerHTML = "<p><div id='horizontalmenu'> " +
"        <ul> <li><a href='#'>Language</a> " +
"                <ul> <li><a href='javascript:guide_python();'>Python</a></li> <li><a href='javascript:guide_r()'>R &nbsp;&nbsp;</a></li> <li><a href='javascript:guide_pgadmin()'>pgAdmin</a></li> </ul>" +
"            </li>" +
"        </ul>" +
"</div></p>";
}

function guide_python() {
    document.getElementById("tutorial_area").innerHTML = "<p><br/><br/>Using CSV data : <br/>" +
    "- The reader function implemented in the CSV module lets import data by returning an object that iterates over lines in a CSV file. <br/>" +
    "Example of a reader usage : <br/>" +
    ">>> import csv<br/>" +
    ">>> with open('eggs.csv', 'rb') as csvfile: <br/>" +
    "...     spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')<br/>" +
    "...     for row in spamreader: <br/>" +
    "...         print ', '.join(row)<br/>" +
    "Spam, Spam, Spam, Spam, Spam, Baked Beans<br/>" +
    "Spam, Lovely Spam, Wonderful Spam<br/>" +
    "More information on : <a href='https://docs.python.org/2/library/csv.html'>the python documentation</a><br/>" +
    "</p>";
}

function guide_r() {
    document.getElementById("tutorial_area").innerHTML = "<p><br/><br/>Using CSV data : <br/>" +
    "You can read directly a CSV file using the function read.csv. <br/>" +
    "Example : <br/>" +
    "MyData <- read.csv(file='TheDataIWantToReadIn.csv', header=TRUE, sep=';') <br/>" +
    "- sep is your separator. If you have not specified anything, it is ;.<br/>" +
    "- header should be set to true, because in our case, we have the names of the columns on the first line.<br/>" +
    "</p>";
}

function guide_pgadmin() {
    document.getElementById("tutorial_area").innerHTML = "<p><br/><br/>Using CSV data : <br/><br/>" +
    "- Import into a table : <br/><br/>" +
    "First of all, if your table is not created, you can create it using the create table command. :<br/>" +
    "The structure of the CSV file is as follows :<br/><br/>" +
    "Timestamps | Plant 1 | Plant 2 | ...<br/><br/>" +
    "Here is an example for nuclear power plants table creation :<br/><br/>" +
    "<span style='font-family:sans-serif;'>CREATE TABLE nuclearplants (<br/>" +
    "timestamps date,<br/>" +
    "brokdorf integer,<br/>" +
    "emsland integer,<br/>" +
    "grafenrheinfeld integer,<br/>" +
    "grohnde integer,<br/>" +
    "gundremmingen_b integer,<br/>" +
    "gundremmingen_c integer,<br/>" +
    "isar_2 integer,<br/>" +
    "neckarwestheim integer,<br/>" +
    "philippsburg integer,<br/>" +
    ");<br/><br/></span>" +
    "You can then simply copy the data onto the table if you made sure that you have the same columns. Using our example : <br/><br/>" +
    "COPY nuclearplants FROM '/path/to/the/file.csv' WITH DELIMITER ';' CSV HEADER;<br/><br/>" +
    "</p>";
}

function printoptions_howto() {
    document.getElementById("page_content").innerHTML = "<p>Date format : allows you to change the way the date is displayed. The default is %Y-%m-%d. The options are as follows :" +
    "<ul><li>%a - Abbreviated weekday name</li><li>%A - Full weekday name</li>" +
    "<li>%b - abbreviated month name</li><li>%B - full month name</li>" +
    "<li>%C - century number (the year divided by 100, range 00 to 99)</li>" +
    "<li>%d - day of the month (01 to 31)</li><li>%D - same as %m/%d/%y</li>" +
    "<li>%e - day of the month (1 to 31)</li><li>%g - like %G, but without the century</li>" +
    "<li>%G - 4-digit year corresponding to the ISO week number (see %V)</li><li>%h - same as %b</li>" +
    "<li>%H - hour, using a 24-hour clock (00 to 23)</li><li>%I - hour, using a 12-hour clock (01 to 12)</li>" +
    "<li>%j - day of the year (001 to 366)</li><li>%m - month (01 to 12)</li>" +
    "<li>%M - minute</li><li>%n - newline character</li><li>%p - either am or pm according to the given time value</li>" +
    "<li>%r - time in a.m. and p.m. notation</li><li>%R - time in 24 hour notation</li>" +
    "<li>%S - second</li><li>%t - tab character</li><li>%T - current time, equal to %H:%M:%S</li>" +
    "<li>%u - weekday as a number (1 to 7), Monday=1. Warning: In Sun Solaris Sunday=1</li>" +
    "<li>%U - week number of the current year, starting with the first Sunday as the first day of the first week</li>" +
    "<li>%V - The ISO 8601 week number of the current year (01 to 53), where week 1 is the first week that has at least 4 days in the current year, and with Monday as the first day of the week</li>" +
    "<li>%W - week number of the current year, starting with the first Monday as the first day of the first week</li>" +
    "<li>%w - day of the week as a decimal, Sunday=0</li>" +
    "<li>%x - preferred date representation without the time</li><li>%X - preferred time representation without the date</li>" +
    "<li>%y - year without a century (range 00 to 99)</li><li>%Y - year including the century</li>" +
    "<li>%Z or %z - time zone or name or abbreviation</li><li>%% - a literal % character</li></ul></p>" +
    "<p>Plants to exclude : To exclude plants, you have to input the number of the plants separated by commas and starting at 0. The order is given by the plot.<br/>" +
    "Example : You have Plant A, plant B, plant C, plant D, plant E, and you want to exclude plants A and D. In that case, that would be 0,3 that you have to input.</p>"
}

function print_text()
{
    console.log("Tested successfully");
}

function print_csvdownload() {
    document.getElementById("page_content").innerHTML = "<p><a href='/results.csv' download>Download CSV file</a></p>"
}

function read_file(file) {
    var reader = new FileReader();
    reader.onload = function(event) {
        var contents = event.target.result;
        console.log("File contents: " + contents);
    };

    reader.onerror = function(event) {
        console.error("File could not be read! Code " + event.target.error.code);
    };

    reader.readAsText(file);
}

function reset_plot() {
    document.getElementById("page_content").innerHTML = "<br/><a href='/images/results.png' class='download_plot' download>Download image</a><a href='/results.csv' style='position:relative;top:10px;left:30px;' download>Download csv data</a><br/><img src='/images/results.png' alt='Graph' class='plot'>";
}

//function LoadFile() {
//    var oFrame = document.getElementById("frmFile");
//    var strRawContents = oFrame.contentWindow.document.body.childNodes[0].innerHTML;
//    while (strRawContents.indexOf("\r") >= 0)
//        strRawContents = strRawContents.replace("\r", "");
//    var arrLines = strRawContents.split("\n");
//    alert("File " + oFrame.src + " has " + arrLines.length + " lines");
//    for (var i = 0; i < arrLines.length; i++) {
//        var curLine = arrLines[i];
//        alert("Line #" + (i + 1) + " is: '" + curLine + "'");
//    }
//}