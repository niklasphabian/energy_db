#!/usr/bin/env python
# coding=utf-8

class WebInterfaceTests(helper.CPWebCase):
    # First part : tests on EEX Trans page
    def test_missing_provider(self): # should fail
        self.getPage('/download_csv_eex_trans?startdate=2014-01-01+00%3A00&enddate=2015-01-01+00%3A00&type=solar&actual=False&provider=')
        self.assertHeader('Content-Type', 'text/html;charset=utf-8')
        self.assertBody("""
<div id="page">
<div id="header">
<div style="width: 260px; float: left">
<a href="index"><h1 id="logo">Title</h1></a><br><br>
</div>
<h1>Web interface to electricity production and pricing database</h1>
</div>
<div style="clear: both;"></div>
<div id="breadcrumb">Index &gt; <span style="color:red;">Choosing data tables</span> &gt; Choosing dates and data to extract</div>
<div id="navigation">
<!-- navigation here -->

<a href="tutorial">Tutorial</a><br><br>
<a href="whatisthis">What is this</a><br><br></div>

</div>
<div id="page_content">
<h2>Query execution failed</h2>
<p>This is most likely due to an error in the parameters.<br>
You have to enter data with this format :<br>
<br>
Startdate : YYYY-MM-DD HH:MM (for example 2014-01-20 21:42)<br>
Enddate : YYYY-MM-DD HH:MM (for example 2014-04-20 23:42)<br>
Then check the boxes for the providers (Amprion, Tennet...).<br>
You have to check a box in every data group.</p></div>

            """)

        # Second part : tests on EPEX Spot
    def test_missing_type_epex_spot(self): # should fail
        self.getPage('/download_csv_epex_spot?startdate=2014-01-01+00%3A00&enddate=2015-01-01+00%3A00&type=&country=DE&resolution=quarterhourly')
        self.assertHeader('Content-Type', 'text/html;charset=utf-8')
        self.assertBody("""
<div id="page">
<div id="header">
<div style="width: 260px; float: left">
<a href="index"><h1 id="logo">Title</h1></a><br><br>
</div>
<h1>Web interface to electricity production and pricing database</h1>
</div>
<div style="clear: both;"></div>
<div id="breadcrumb">Index &gt; <span style="color:red;">Choosing data tables</span> &gt; Choosing dates and data to extract</div>
<div id="navigation">
<!-- navigation here -->

<a href="tutorial">Tutorial</a><br><br>
<a href="whatisthis">What is this</a><br><br></div>

</div>
<div id="page_content">
<h2>Query execution failed</h2>
<p>This is most likely due to an error in the parameters.<br>
You have to enter data with this format :<br>
<br>
Startdate : YYYY-MM-DD HH:MM (for example 2014-01-20 21:42)<br>
Enddate : YYYY-MM-DD HH:MM (for example 2014-04-20 23:42)<br>
Then check the boxes for the providers (Amprion, Tennet...).<br>
You have to check a box in every data group.</p></div>

            """)
