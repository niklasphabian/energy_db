#!/usr/bin/env python
# coding=utf-8

__author__ = 'logerais'

import cherrypy
import os
import doctest
import pylab
import matplotlib.pyplot as plt
import psycopg2
import matplotlib.dates
import datetime
import time
from cherrypy.test import helper
from mako.template import Template
from mako.lookup import TemplateLookup
import csv

class FormError(Exception):
    def __init__(self, strcode):

        # Call the base class constructor with the parameters it needs
        super(FormError, self).__init__(strcode)

        self.errors = strcode

class FormChecks:
    def __init__(self):
        pass

    def check_date(self, date_as_string):
        # Checks if a string corresponding to a date is actually a properly formatted date

        try:
            date_in_datetime_format = time.strptime(date_as_string, "%Y-%m-%dT%H:%M")
            date = time.strftime("%Y-%m-%dT%H:%M", date_in_datetime_format)
            return date
        except ValueError:
            print "Form check : Chrome-specific format not working"

        try:
            date_in_datetime_format = time.strptime(date_as_string, "%Y-%m-%d %H:%M:%S")
            date = time.strftime("%Y-%m-%d %H:%M:%S", date_in_datetime_format)
            return date
        except ValueError:
            print "Form check : detailed format not working"

        try:
            date_in_datetime_format = time.strptime(date_as_string, "%Y-%m-%d %H:%M")
            date = time.strftime("%Y-%m-%d %H:%M", date_in_datetime_format)
            return date
        except ValueError:
            print "Form check : format without seconds not working"

        try:
            date_in_datetime_format = time.strptime(date_as_string, "%Y-%m-%d %H")
            date = time.strftime("%Y-%m-%d %H", date_in_datetime_format)
            return date
        except ValueError:
            print "Form check : format without minutes not working"

        try:
            date_in_datetime_format = time.strptime(date_as_string, "%Y-%m-%d")
            date = time.strftime("%Y-%m-%d", date_in_datetime_format)
            return date
        except ValueError:
            print "Form check : format without hours not working"

        try:
            date_in_datetime_format = time.strptime(date_as_string, "%Y-%m")
            date = time.strftime("%Y-%m", date_in_datetime_format)
            return date
        except ValueError:
            print "Form check : format without days not working"

        try:
            date_in_datetime_format = time.strptime(date_as_string, "%Y")
            date = time.strftime("%Y", date_in_datetime_format)
            return date
        except ValueError:
            print "Form check : format without months not working"

        return 0
"""

The WebInterface module contains every page of the server, as well as algorithms to generate files.
Every exposed function is a webpage.

"""

class WebInterface:
    form_check = FormChecks

    def __init__(self, **kwargs):
        self.form_check = FormChecks()
        pass

    @cherrypy.expose()
    def default(self, *args, **kwargs):
        """
        Default page. Contains an error message for when a page doesn't exist.
        """

        html_template = Template(filename="template/default/default_page.html")
        return html_template.render()

    @cherrypy.expose()
    def index(self, **kwargs):
        """
        Homepage. It contains links to the following pages :

        tutorial & whatisthis : for the menu

        eex_trans
        epex_spot
        blockwise
        climate_data
        control_reserve_tenders
        control_reserve_tenders_global
        activation_high_res
        rz_saldo

        """
        html_template = Template(filename="template/index/index_page.html")
        return html_template.render()

    @cherrypy.expose()
    def epex_spot(self, **kwargs):
        """
        The page used to input data on the EPEX Spot tables.

        List of tables covered by this :

        EPEX_DayAheadHour_CH (Day Ahead, Hourly, Switzerland)
        EPEX_DayAheadHour_FR (Day Ahead, Hourly, France)
        EPEX_DayAheadHour_DE (Day Ahead, Hourly, Germany)

        EPEX_DayAheadBlocks_DE (Day Ahead, Quarterhourly, Germany)
        EPEX_DayAheadBlocks_FR (Day Ahead, Quarterhourly, France)
        EPEX_DayAheadBlocks_CH (Day Ahead, Quarterhourly, Switzerland)

        The data is sent via a GET method.

        See documentation on corresponding download_csv function for the data it sends back.
        """
        form_action = "download_csv_epex_spot"
        input_date_format = "YYYY-MM-DD HH:MM:SS+timezone (GMT +2 is written +02)"
        group_name = "Epex spot pricing"

        mylookup = TemplateLookup(directories=['template'])
        html_template = Template(filename="template/form_template.html", lookup=mylookup)

        return html_template.render(input_date_format=input_date_format, group_name=group_name, form_action=form_action)

    def control_form_epex_spot(self, **kwargs):
        """
        The following variables are controlled by this function :

        - startdate
        - enddate
        - type : Should be input via a form, just check whether or not it's been changed manually
        - country : Same deal. Note : if you want to add another country, it must be 2 letters long and appear in the table name.
        Example : for sweden, if you have tables ending with _SE, it should be "se"
        - resolution : Should be either hourly or quarterhourly.

        Each of the exceptions does the following things :

        """
        startdate = ""
        enddate = ""
        type = ""
        time = ""
        resolution = ""
        country = ""

        for key, value in kwargs.iteritems():
            print "%s = %s" % (key, value)
            if key == "startdate":
                print value
                if self.form_check.check_date(value) != 0:
                    startdate = value
                else:
                    raise FormError(strcode="bad_startdate")

            elif key == "type":
                if value == "intraday" or value == "dayahead":
                    type = value
                else:
                    type = ""
                    raise FormError(strcode="bad_type")

            elif key == "country":
                if value == "fr" or value == "de" or value == "ch":
                    country = value
                else:
                    raise FormError(strcode="bad_resolution")

            elif key == "enddate":
                print value
                if self.form_check.check_date(value) != 0:
                    enddate = value
                else:
                    raise FormError(strcode="bad_enddate")

            elif key == "resolution":
                if value == "hourly" or value == "quarterhourly":
                    resolution = value

            elif key == "time":
                if value == "basepeak" or value == "blocks":
                    time = value

        return (startdate ,enddate, type, time, resolution, country,)

    @cherrypy.expose()
    def download_csv_epex_spot(self, **kwargs):
        """
        This function generates a CSV file for the EPEX_Spot tables.
        The list of tables can be found in the documentation for epex_spot.

        First, it checks the form with the control_form_epex_spot function. If the form returns an error, we show the error too (return FormError.message)

        Then we build the query. First, we make a list of the columns in the table "columns". These must be the exact names of the columns, with the right case.
        At the same time, we create the table name, which will be used in the query creation.

        The query creation is in 3 parts :

        - First part : it is the part that goes from "SELECT" to "FROM" (FROM not included). We include the table name multiple times, with the columns to extract.
        We don't make a full loop (we separate the last element when we call "for i in xrange(0, len(columns) - 1)"), because we don't add a comma at the end

        - Second part : from "FROM" to "WHERE" (WHERE not included). We just use the table name here.

        - Third part : from "WHERE" to the end, and we once again need the table name only.

        Then we use a dictionnary to connect to the database.
        The reason why we don't use the connection data directly is because in case of an error message, we could end up sending back a password.

        In case the query failed, we raise an exception and return a page with a few hints (not too specific however).

        Then we just fetch the query results and we format them for the CSV file.
        The data is sent back in the form of tuples, where each line of data is 1 tuple.
        So for example, result[3] is the line number 3, with a timestamp and every piece of data for this timestamp.

        We transform this to a table of tables with the same kind of structure, and we also add the columns names.
        Then we use a CSV writer and we send a page containing the link to the CSV page.
        """

        fields = self.control_form_epex_spot(**kwargs)
        print fields
        if not fields:
            print "Error during form control"
            return FormError.message
        else:
            startdate, enddate, type, time, resolution, country = fields
        # Query selection/creation
        # First and foremost, selection by type and actual/expected production
        type = str(type)

        if type == "intraday":
            if resolution == "hourly":
                table_name = "EPEX_IntraDay_" + country.upper() + "_Hourly"
                print table_name
                columns = ["TimeStamp", "Low", "High", "Last", "WeightAvg", "Index", "BuyVol", "SellVol"]
            else:
                table_name = "EPEX_IntraDay_" + country.upper()
                columns = ["TimeStamp", "Low", "High", "Last", "WeightAvg", "Index", "BuyVol", "SellVol"]
                print table_name
        else: # type = "dayahead"
            if resolution == "hourly":
                table_name = "EPEX_DayAheadHour_" + country.upper()
                print table_name
                columns = ["TimeStamp", "Price", "Volume"]
            else: # either for blocks or dayahead
                if time == "basepeak":
                    table_name = "EPEX_DayAheadBasePeak_" + country.upper()
                    columns = ["TimeStamp", "PriceBase", "VolumeBase", "PricePeak", "VolumePeak"]
                    print table_name
                elif time == "blocks":
                    table_name = "EPEX_DayAheadBlocks_" + country.upper()
                    columns = ["TimeStamp", "MiddleNight", "EarlyMorning", "LateMorning", "EarlyAfternoon", "RushHour", "OffPeak2", "Night", "OffPeak1", "Business", "OffPeak", "Morning", "HighNoon", "Afternoon", "Evening", "SunPeak"]

        query = "SELECT " + ', '.join('\"{}\"'.format(i) for v, i in enumerate(columns)) + """
        FROM
            public.\"""" + table_name + "\" WHERE " + "\"" + table_name + "\".\"TimeStamp\" >= %s AND \"" + table_name + "\".\"TimeStamp\" <= %s;"

        print query
        dictionnary = {'dbname': 'energy_price', 'user': 'logerais', 'host': 'eifpostgres02', 'port': '5432', 'database': 'griessbaum', 'password': 'Logerais'};
        conn = psycopg2.connect(**dictionnary)
        cur = conn.cursor()

        print startdate
        print enddate
        print query
        try:
            cur.execute(query, (startdate, enddate,))
            print "Query successfuly ran"
        except Exception as error:
            print error

            query_failed_page = ""
            with open('template/eex_trans/eex_trans_error_query_failed.html') as page:
                for line in page:
                    query_failed_page += line
            return query_failed_page

        # Once the query is executed, we fetch the results
        # First step : create both tables for the time stamps and the rest

        result = cur.fetchall()

        # Append the list of variables
        csvoutput_list = []
        csvoutput_list.append(columns)

        for j in xrange(1, len(result)):
            csvoutput_list.append([])
            for i in xrange(0, len(result[j])):
                csvoutput_list[j].append(result[j][i])

        with open("results.csv", "w") as output:
            writer = csv.writer(output, lineterminator='\n', delimiter=';', skipinitialspace=True)
            writer.writerows(csvoutput_list)

        return """<html><head></head><body><a href="results.csv" download>Download CSV data</a>
        <br/><br/>
        <a href="epex_spot">Go back to data selection</a></body></html>"""

    @cherrypy.expose()
    def activation_high_res(self, **kwargs):
        """Extract data from table : SRL_ActivationHighRes


        """
        form_action = "download_csv_activation_high_res"
        input_date_format = "YYYY-MM-DD HH:MM:SS+timezone (GMT +2 is written +02)"
        group_name = "Activation data, high resolution"

        mylookup = TemplateLookup(directories=['template/', 'template/activation_high_res/'])
        html_template = Template(filename="template/form_template.html", lookup=mylookup)

        return html_template.render(input_date_format=input_date_format, group_name=group_name, form_action=form_action)

    @cherrypy.expose()
    def rz_saldo(self, **kwargs):
        """ For the tables : RZ_Saldo_50_Hertz, RZ_Saldo_Ampirion, RZ_Saldo_Netzelgerverbund, RZ_Saldo_Tennet, RZ_Saldo_Transnet_BW

        The tables contain a startdate as well as an enddate for each line. This is because the data is for 15 minute long intervals.

        This can create confusion about which piece of data to take or not. Here are the rules to it :

        - If the startdate corresponds exactly to a startdate (for example, 2014-01-01 00:00, take data starting from the sample that starts at this time)
        - If the startdate doesn't correspond exactly to a startdate in the table (for example 2014-01-01 00:08), we start taking data immediately afterwards
        - If the enddate corresponds exactly to something in the table, take data until this date only
        - Otherwise take data until just before the specified enddate. So for exemple, if you specify 00:07 as an end hour, you will probably get data until 00:00.

        """

        form_action = "download_csv_rz_saldo"
        input_date_format = "YYYY-MM-DD HH:MM:SS+timezone (GMT +2 is written +02)"
        group_name = "Activation data, high resolution"

        mylookup = TemplateLookup(directories=['template/', 'template/rz_saldo/'])
        html_template = Template(filename="template/form_template.html", lookup=mylookup)

        return html_template.render(input_date_format=input_date_format, group_name=group_name, form_action=form_action)

    @cherrypy.expose()
    def climate_data(self, **kwargs):
        # For the table : ClimateLeipzig (obsolete)
        # Should be updated and either look for info on Climate_Stations or Climate_TemperatureByStations


        form_action = "download_csv_climate_data"
        input_date_format = "YYYY-MM-DD HH:MM:SS+timezone (GMT +2 is written +02)"
        group_name = "Activation data, high resolution"

        mylookup = TemplateLookup(directories=['template/', 'template/climate_data/'])
        html_template = Template(filename="template/form_template.html", lookup=mylookup)

        return html_template.render(input_date_format=input_date_format, group_name=group_name, form_action=form_action)

    @cherrypy.expose()
    def download_csv_climate_data(self, startdate ,enddate, air_temp="False", rel_humidity="False", **kwargs):

        # Note : This function needs a rework ! The data structure has been changed in the table.

        # Query selection/creation

        fields = [""""ClimateLeipzig"."TimeStamp\" """]

        # First and foremost, selection by type and actual/expected production
        # Select by provider
        if air_temp.find("True") != -1:
            fields.append(""" "ClimateLeipzig"."AirTemp" """)

        if rel_humidity.find("True") != -1:
            fields.append(""" "ClimateLeipzig"."RelHumidity" """)

        query += """        SELECT {fields}
        FROM        public."ClimateLeipzig"
        WHERE
         "ClimateLeipzig"."TimeStamp" >= %s AND
         "ClimateLeipzig"."TimeStamp" <= %s
        ORDER BY
  "ClimateLeipzig"."TimeStamp" ASC;""".format(fields=", ".join(fields))

        print query
        conn = psycopg2.connect(dbname = "energy_price", user = "logerais", host = "eifpostgres02", port = "5432", database = "griessbaum", password = "Logerais")
        cur = conn.cursor()

        print startdate
        print enddate
        print query
        try:
            cur.execute(query, (startdate, enddate,))
            print "Query successfuly ran"
        except:
            print "Error in query execution"
        try:
            result = cur.fetchall()
            print "Results fetched"
        except:
            print "Could not fetch the results"
            return "Unable to execute the query"

        # Creation of the date list :
        datelist = []
        powerlists = []
        for i in xrange(len(result)):
            datelist.append(result[i][0])

        for j in xrange(0, counter): # Looping over the providers
            powerlists.append([])
            for i in xrange(0, len(result)): # Looping over time
                powerlists[j].append(result[i][j+1])

        csvoutput_list = []
        csvoutput_list.append(["TimeStamps with time zone"])

        if air_temp.find("True") != -1:
            csvoutput_list[0].append("Air temperature in celcius degrees")

        if rel_humidity.find("True") != -1:
            csvoutput_list[0].append("Relative humidity (in %)")

        print csvoutput_list
        for i in xrange(0, len(result)):
            # Adding timestamps at the start of the table (first row)
            csvoutput_list.append([])
            csvoutput_list[i+1].append(datelist[i].strftime("%Y-%m-%d %H:%M%Z"))

        for j in xrange(0, counter):
            for i in xrange(0, len(result)):
                # Adding timestamps at the start of the table (first row)
                csvoutput_list[i+1].append(powerlists[j][i])

        with open("results.csv", "w") as output:
            writer = csv.writer(output, lineterminator='\n', delimiter=';', skipinitialspace=True)
            writer.writerows(csvoutput_list)

        return """<html><head></head><body><a href="results.csv" download>Download CSV data</a>
        <br/><br/>
        <a href="climate_leipzig">Go back to data selection</a></body></html>"""

    @cherrypy.expose()
    def tutorial(self, **kwargs):
        complete_page = ""

        with open('template/tutorial.html') as page:
            for line in page:
                complete_page += line

        return complete_page
    @cherrypy.expose()
    def whatisthis(self, **kwargs):

        complete_page = ""

        with open('template/whatisthis.html') as page:
            for line in page:
                complete_page += line

        return complete_page

    @cherrypy.expose()
    def eex_trans(self, **kwargs):
        """
        For the tables : EEXTrans_Solar_actual_DE, EEXTrans_Solar_Expected_DE, EEXTrans_Wind_actual_DE, EEXTrans_Wind_expected_DE, EEXTrans_100MW_actual, EEXTrans_100MW_expected

        The data is organized in this fashion in each table :

        A row for timestamps, and a row for each provider. Every row has data in MW.

        The tables contain a startdate as well as an enddate for each line. This is because the data is for time intervals.

        This can create confusion about which piece of data to take or not. Here are the rules to it :

        - If the startdate corresponds exactly to a startdate (for example, 2014-01-01 00:00, take data starting from the sample that starts at this time)
        - If the startdate doesn't correspond exactly to a startdate in the table (for example 2014-01-01 00:08), we start taking data immediately afterwards
        - If the enddate corresponds exactly to something in the table, take data until this date only
        - Otherwise take data until just before the specified enddate. So for exemple, if you specify 00:07 as an end hour, you will probably get data until 00:00.

        Rows : TimeStampStart, TimeStampEnd, ReserveType, CapacityPrice, EnergyPrice, OfferedPower, Award, id

        Every row is taken into exported data.
        """

        form_action = "download_csv_eex_trans"
        input_date_format = "YYYY-MM-DD HH:MM:SS+timezone (GMT +2 is written +02)"
        group_name = "Form transfer"

        mylookup = TemplateLookup(directories=['template/', 'template/eex_trans/'])
        html_template = Template(filename="template/form_template.html", lookup=mylookup)

        return html_template.render(input_date_format=input_date_format, group_name=group_name, form_action=form_action)

    def control_form_eex_trans(self, **kwargs):
        startdate = ""
        enddate = ""
        type = ""
        actual = False
        provider = ""

        for key, value in kwargs.iteritems():
            print "%s = %s" % (key, value)
            if key == "startdate":
                print value
                try:
                    startdate = value
                except:
                    raise FormError(strcode="bad_startdate")

            elif key == "type":
                if len(key) > 0:
                    type = value
                else:
                    type = ""
                    raise FormError(strcode="bad_type")

            elif key == "actual":
                if value == "True":
                    actual = True
                elif value == "False":
                    actual = False
                else:
                    raise FormError(strcode="bad_actual")

            elif key == "enddate":
                try:
                    enddate = value
                except:
                    raise FormError(strcode="bad_enddate")

            elif key == "provider":
                provider = value

            else:
                raise Exception # custom exception to do

        return (startdate ,enddate, type, actual, provider,)

    @cherrypy.expose()
    def download_csv_eex_trans(self,  **kwargs):
        """
        Data fetch/CSV creation function for EEX Transparency tables :

        For the tables : EEXTrans_Solar_actual_DE, EEXTrans_Solar_Expected_DE, EEXTrans_Wind_actual_DE, EEXTrans_Wind_expected_DE, EEXTrans_100MW_actual, EEXTrans_100MW_expected

        First step is the form control, which is made thanks to the function control_form_eex_trans.
        It's mostly used to see if the dates fit the actual form.

        Second step is the query creation.
        """
        fields = self.control_form_eex_trans(**kwargs)
        print fields
        if not fields:
            raise FormError(strcode="bad_submitted_data") # To be updated
        else:
            startdate, enddate, type, actual, provider = fields
        # Query selection/creation
        # First and foremost, selection by type and actual/expected production
        type = str(type)
        print type
        print actual

        if type == "solar" and actual:
            query = """SELECT
            "EEXTrans_Solar_actual_DE"."TimeStamp",\n"""
            table_name = "\"EEXTrans_Solar_actual_DE\""
        elif type == "solar" and not actual:
            query = """SELECT
            "EEXTrans_Solar_expected_DE"."TimeStamp",\n """
            table_name = "\"EEXTrans_Solar_expected_DE\""
        elif type == "wind" and actual:
            query = """SELECT
            "EEXTrans_Wind_actual_DE"."TimeStamp", \n"""
            table_name = "\"EEXTrans_Wind_actual_DE\""
        elif type == "wind" and not actual:
            query = """SELECT
            "EEXTrans_Wind_expected_DE"."TimeStamp", \n"""
            table_name = "\"EEXTrans_Wind_expected_DE\""
        elif type == "100mw" and actual:
            query = """SELECT
            "EEXTrans_100MW_actual_DE"."TimeStamp", \n"""
            table_name = "\"EEXTrans_100MW_actual_DE\""
        elif type == "100mw" and not actual:
            query = """SELECT
            "EEXTrans_100MW_expected_DE"."TimeStamp", \n"""
            table_name = "\"EEXTrans_100MW_expected_DE\""
        else: # Only there for testing purposes !
            query = """SELECT
            "EEXTrans_100MW_expected_DE"."TimeStamp", \n"""
            table_name = "\"EEXTrans_100MW_expected_DE\""
        # Select by provider
        print provider
        provider = str(provider)
        provider_string = ''.join(provider)
        number_of_providers = len(provider)
        counter = 0
        if type != "100mw":
            if provider_string.find("fzhertz") != -1:
                query += table_name
                query += """."FZHertz",\n"""
                counter += 1

            if provider_string.find("amprion") != -1:
                query += table_name
                query += """."Amprion",\n"""
                counter += 1

            if provider_string.find("tennet") != -1:
                query += table_name
                query += """."Tennet",\n"""
                counter += 1

            if provider_string.find("transnetbw") != -1:
                query += table_name
                query += """."TransnetBW",\n"""
                counter += 1
        else:
            query += table_name
            query += """."Power",\n"""

        # Remove the comma at the end before the FROM to avoid mistakes
        query = query [:-2]
        # We have to point out the right table twice in a query
        print query
        query += "\n"
        print query

        if type == "solar" and actual:
            query += """FROM
            public."EEXTrans_Solar_actual_DE" \n """
        elif type == "solar" and not actual:
            query += """FROM
            public."EEXTrans_Solar_expected_DE" \n """
        elif type == "wind" and actual:
            query += """FROM
            public."EEXTrans_Wind_actual_DE" \n """
        elif type == "wind" and not actual:
            query += """FROM
            public."EEXTrans_Wind_expected_DE" \n """
        elif type == "100mw" and actual:
            query += """FROM
            public."EEXTrans_100MW_actual_DE" \n """
        elif type == "100mw" and not actual:
            query += """FROM
            public."EEXTrans_100MW_expected_DE" \n """
        print query

        # Adding the startdate/enddate precisions and the ordering part
#        query += """WHERE
#  "EEXTrans_Solar_actual_DE"."TimeStamp" >= '%s' AND
#  "EEXTrans_Solar_actual_DE"."TimeStamp" <= '%s' \n """
        query += "WHERE \n" + table_name + """."TimeStamp" >= %s AND \n""" + table_name + """."TimeStamp" <= %s \n"""
        if type == "solar" and actual:
            query += """ORDER BY
  "EEXTrans_Solar_actual_DE"."TimeStamp" ASC;"""
        elif type == "solar" and not actual:
            query += """ORDER BY
  "EEXTrans_Solar_expected_DE"."TimeStamp" ASC;"""
        elif type == "wind" and actual:
            query += """ORDER BY
  "EEXTrans_Wind_actual_DE"."TimeStamp" ASC;"""
        elif type == "wind" and not actual:
            query += """ORDER BY
  "EEXTrans_Wind_expected_DE"."TimeStamp" ASC;"""
        elif type == "100mw" and actual:
            query += """ORDER BY
  "EEXTrans_100MW_actual_DE"."TimeStamp" ASC;"""
        elif type == "100mw" and not actual:
            query += """ORDER BY
  "EEXTrans_100MW_expected_DE"."TimeStamp" ASC;"""

        # Query generation with formatting to be added here and tested

        print query
        dictionnary = {'dbname': 'energy_price', 'user': 'logerais', 'host': 'eifpostgres02', 'port': '5432', 'database': 'griessbaum', 'password': 'Logerais'};
        conn = psycopg2.connect(**dictionnary)
        cur = conn.cursor()

        print startdate
        print enddate
        print query
        try:
            cur.execute(query, (startdate, enddate,))
            print "Query successfuly ran"
        except:
            print "Error in query execution"

            query_failed_page = ""
            with open('template/eex_trans/eex_trans_error_query_failed.html') as page:
                for line in page:
                    query_failed_page+= line
            return query_failed_page
#        cur.execute(query, (startdate, enddate,))
#        cur.execute(query % ("'2015-01-01'", "'2015-02-01'"))
#        cur.execute(query % (startdate, enddate))
#        cur.execute(query, (str(startdate), str(enddate)))
#        cur.execute(query, dates)
        try:
            result = cur.fetchall()
            print "Results fetched"
        except:
            print "Could not fetch the results"

        if type == "100mw":
            # In the case of 100MW tables, there are only 2 columns : timestamp and power.
            # The data structure for result is as follows :
            # [(timestamp (datetime with timezone), power(number))]

            # Creation of the date list :
            datelist = []
            powerlist = []
            for i in xrange(len(result)):
                datelist.append(result[i][0])

            # Creation of the power list :
            for i in xrange(len(result)):
                powerlist.append(result[i][1])

            csvoutput_list = []
            csvoutput_list.append(["TimeStamps with time zone", "Power"])
            for i in xrange(0, len(result)):
                # Adding timestamps at the start of the table (first row)
                csvoutput_list.append([])
                csvoutput_list[i+1].append(datelist[i].strftime("%Y-%m-%d %H:%M%Z"))

            for i in xrange(0, len(result)):
                # Adding timestamps at the start of the table (first row)
                csvoutput_list[i+1].append(powerlist[i])

            with open("results.csv", "w") as output:
                writer = csv.writer(output, lineterminator='\n', delimiter=';', skipinitialspace=True)
                writer.writerows(csvoutput_list)

        else:
            # In the case of Solar or Wind production data, the tables are a bit more complicated, because the amount of lines is variable.
            # The data structure is as follows for result :
            #[(timestamp (datetime with timezone), FZ Hertz power (if selected), Amprion power (if selected), etc.)]

            # Creation of the date list :
            datelist = []
            powerlists = []
            for i in xrange(len(result)):
                datelist.append(result[i][0])
            # We still have 2 leftover variables from the query generation that will help us : provider_string and counter.
            # provider_string contains the provider names attached without caps, like this : "fzhertztennettransnetbw"
            # counter is an int that equals the number of providers chosen.
            for j in xrange(0, counter): # Looping over the providers
                powerlists.append([])
                for i in xrange(0, len(result)): # Looping over time
                    powerlists[j].append(result[i][j+1])

            csvoutput_list = []
            csvoutput_list.append(["TimeStamps with time zone"])

            if provider_string.find("fzhertz") != -1:
                csvoutput_list[0].append("Power (FZHertz) in MW")

            if provider_string.find("amprion") != -1:
                csvoutput_list[0].append("Power (Amprion) in MW")

            if provider_string.find("tennet") != -1:
                csvoutput_list[0].append("Power (Tennet) in MW")

            if provider_string.find("transnetbw") != -1:
                csvoutput_list[0].append("Power (TransnetBW) in MW")

            print csvoutput_list
            for i in xrange(0, len(result)):
                # Adding timestamps at the start of the table (first row)
                csvoutput_list.append([])
                csvoutput_list[i+1].append(datelist[i].strftime("%Y-%m-%d %H:%M%Z"))

            for j in xrange(0, counter):
                for i in xrange(0, len(result)):
                    # Adding timestamps at the start of the table (first row)
                    csvoutput_list[i+1].append(powerlists[j][i])

            with open("results.csv", "w") as output:
                writer = csv.writer(output, lineterminator='\n', delimiter=';', skipinitialspace=True)
                writer.writerows(csvoutput_list)

        return """<html><head></head><body><a href="results.csv" download>Download CSV data</a>
        <br/><br/>
        <a href="eex_trans">Go back to data selection</a></body></html>"""

    @cherrypy.expose()
    def control_reserve_tenders_global(self, **kwargs):
        """ For the tables Tender_MRL and Tender_SRL

        Data is sent via a GET request.

        Every row of data is sent back.
        """

        form_action = "download_csv_control_reserve_tenders"
        input_date_format = "YYYY-MM-DD HH:MM:SS+timezone (GMT +2 is written +02)"
        group_name = "Epex spot pricing"

        mylookup = TemplateLookup(directories=['template/', 'template/control_reserve_tenders/'])
        html_template = Template(filename="template/form_template.html", lookup=mylookup)

        return html_template.render(input_date_format=input_date_format, group_name=group_name, form_action=form_action)

    @cherrypy.expose()
    def control_reserve_tenders(self, **kwargs):
        """
        For the tables : MRL_Netzregelverbund_Neu, MRL_Tennet, MRL_Transnet, MRL_50_Hertz, MRL_Ampirion,
        SRL_Ampirion, SRL_Netzregelverbund_Neu, SRL_Tennet, SRL_Transnet

        The rows extracted are selected by the user
        """

        form_action = "download_csv_control_reserve_tenders"
        input_date_format = "YYYY-MM-DD HH:MM:SS+timezone (GMT +2 is written +02)"
        group_name = "Epex spot pricing"

        mylookup = TemplateLookup(directories=['template/', 'template/control_reserve_tenders/'])
        html_template = Template(filename="template/form_template.html", lookup=mylookup)

        return html_template.render(input_date_format=input_date_format, group_name=group_name, form_action=form_action)

    def control_form_control_reserve_tenders(self, **kwargs):
        pass

    @cherrypy.expose()
    def download_csv_control_reserve_tenders(self, **kwargs):
        """
        Arguments are sent in this order normally : startdate, enddate, reserve_type, market
        """
        fields = control_form_control_reserve_tenders(kwargs)


    @cherrypy.expose()
    def makeplot(self, startdate, enddate, fuel, chp, print_specific, specific_plants):
        if print_specific == True:
            print "Specific plants"
            plant_list = specific_plants.split(",")
            query_oneplant = """SELECT
  "EEXTrans_Blockwise"."Power",
  "TimeStamps"."TimeStamp"
FROM
  public."EEXTrans_Blockwise",
  public."PowerPlants",
  public."TimeStamps"
WHERE
  "EEXTrans_Blockwise"."timeID" = "TimeStamps"."timeID" AND
  "TimeStamps"."TimeStamp" >= %s AND
  "PowerPlants"."PlantID" = %s AND
  "TimeStamps"."TimeStamp" <= %s
ORDER BY
  "TimeStamps"."TimeStamp" ASC;"""

            query_plantname = """select "PowerPlants"."FullName" from public."PowerPlants" where "PowerPlants"."PlantID" = %s"""
            conn = psycopg2.connect(dbname = "energy_price", user = "logerais", host = "eifpostgres02", port = "5432", database = "griessbaum", password = "Logerais")
            cur = conn.cursor()

            # First, we have to get the name of each plant and input it in a list
            plant_fullnames = []
            print plant_list
            for plantid in plant_list:
                cur.execute(query_plantname, (str(int(plantid)),))
                plant_fullname = cur.fetchall()

                plant_fullname = str(plant_fullname[0])
                plant_fullname = plant_fullname[2:-3]
                plant_fullnames.append(plant_fullname)

            print plant_fullnames
            time_series = []

            try:
               os.remove('images/results.png')
            except OSError:
                print "No results file"
            for i in xrange(0, len(plant_list)):
                cur.execute(query_oneplant, (startdate, plant_list[i], enddate,))
                time_series.append(cur.fetchall())
                self.plot_series(time_series[i], plant_fullnames[i], "%Y-%m-%d %H:%M")

        else:
            conn = psycopg2.connect(dbname = "energy_price", user = "logerais", host = "eifpostgres02", port = "5432", database = "griessbaum", password = "Logerais")
            cur = conn.cursor()
            if fuel == '0':
                initial_query = """SELECT "PowerPlants"."PlantID"
                                                FROM
                                              public."PowerPlants"
                                                WHERE
                                              "PowerPlants"."Fuel" = 'gas' AND """
            elif fuel == '1':
                initial_query = """SELECT "PowerPlants"."PlantID"
                                                FROM
                                              public."PowerPlants"
                                                WHERE
                                              "PowerPlants"."Fuel" = 'coal' AND """
            elif fuel == '2':
                initial_query = """SELECT "PowerPlants"."PlantID"
                                                FROM
                                              public."PowerPlants"
                                                WHERE
                                              "PowerPlants"."Fuel" = 'lignite' AND """

            elif fuel == '3':
                initial_query = """SELECT "PowerPlants"."PlantID"
                                                FROM
                                              public."PowerPlants"
                                                WHERE
                                              "PowerPlants"."Fuel" = 'oil' AND """
            elif fuel == '4':
                initial_query = """SELECT "PowerPlants"."PlantID"
                                                FROM
                                              public."PowerPlants"
                                                WHERE
                                              "PowerPlants"."Fuel" = 'uranium'"""
            elif fuel == '5':
                initial_query = """SELECT "PowerPlants"."PlantID"
                                                FROM
                                              public."PowerPlants"
                                                WHERE
                                              "PowerPlants"."Fuel" = 'pumped-storage'"""
            else:
                initial_query = """SELECT "PowerPlants"."PlantID"
                                                FROM
                                              public."PowerPlants"
                                                WHERE
                                              "PowerPlants"."Fuel" = 'seasonal-store'"""

            if fuel != '4' and fuel != '5' and fuel != '6':
                if chp == '0':
                    initial_query += """ "PowerPlants"."PowerThermal" IS NULL; """
                else:
                    initial_query += """ "PowerPlants"."PowerThermal" > 1; """

            cur.execute(initial_query)
            plant_list = cur.fetchall()
            for i in xrange(0, len(plant_list)):
                plant_list[i] = str(plant_list[i])
                plant_list[i] = plant_list[i][1:-2]

            print "Plant list :"
            print plant_list
            query_oneplant = """SELECT
  "EEXTrans_Blockwise"."Power",
  "TimeStamps"."TimeStamp"
FROM
  public."EEXTrans_Blockwise",
  public."PowerPlants",
  public."TimeStamps"
WHERE
  "EEXTrans_Blockwise"."timeID" = "TimeStamps"."timeID" AND
  "TimeStamps"."TimeStamp" >= %s AND
  "PowerPlants"."PlantID" = %s AND
  "TimeStamps"."TimeStamp" <= %s
ORDER BY
  "TimeStamps"."TimeStamp" ASC;"""
            plant_fullnames = []
            for plantid in plant_list:
                cur.execute(query_oneplant, (startdate, str(int(plantid)), enddate,))
                plant_fullname = cur.fetchall()

                plant_fullname = str(plant_fullname[1])
                plant_fullname = plant_fullname[2:-3]
                plant_fullnames.append(plant_fullname)

            print "Fullnames :"
            print plant_fullnames
            time_series = []
            print "End of fullnames"

            try:
                os.remove('images/results.png')
            except OSError:
                print "No results file"
            for i in xrange(0, len(plant_list)):
                cur.execute(query_oneplant, (startdate, plant_list[i], enddate,))
                time_series.append(cur.fetchall())
                self.plot_series(time_series[i], plant_fullnames[i], "%Y-%m-%d %H:%M")

    def plot_series(self, timeSerie, name, date_format_print):
        time = []
        power = []
        for row in timeSerie:
                time.append(row[1])
                power.append(row[0])
        print name
        pylab.xlabel("Time")
        ax = plt.subplot(1, 1, 1)
        start, end = ax.get_xlim()
        date_list = []
        for i in xrange(0, 10):
            date_list.append(start + i*(end - start)/9)

        myFmt = matplotlib.dates.DateFormatter(date_format_print)
        ax.xaxis.set_major_formatter(myFmt)
        ax.xaxis.set_ticks(date_list)
        pylab.ylabel("Electric power in MW")
        plt.plot(time, power, label = name)

        try:
            pylab.title('Representation of the power produced by power plants over time')
            pylab.grid()
            pylab.legend(prop={'size':8})
            figure = plt.gcf() # get current figure
            figure.set_size_inches(20, 16)
            pylab.savefig('images/results.png')
        except:
            print("Error while plotting")

    def save_file(self, listofrows, plantnames, csvfile, date_format):
        csvoutput_list = []
        csvoutput_list.append(plantnames)
        csvoutput_list[0].insert(0, "Timestamps")
        L = max(listofrows, key=len)
        for i in xrange(0, len(L)):
            # Adding timestamps at the start of the table (first row)
            csvoutput_list.append([])
            csvoutput_list[i+1].append(L[i][1].strftime(date_format))

        for k in xrange(0, len(listofrows)): # iteration over the plants
            for j in xrange(0, len(listofrows[k])): # iteration over the timestamps
                csvoutput_list[j+1].append(listofrows[k][j][0])

        with open(csvfile, "w") as output:
            writer = csv.writer(output, lineterminator='\n', delimiter=';', skipinitialspace=True)
            writer.writerows(csvoutput_list)

if __name__ == '__main__':
    cherrypy.quickstart(WebInterface(), '/', 'app.conf')
    doctest.testmod(verbose=True)
