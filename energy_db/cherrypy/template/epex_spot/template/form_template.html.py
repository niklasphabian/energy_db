# -*- encoding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 6
_modified_time = 1436960655.877583
_template_filename='template/form_template.html'
_template_uri='template/form_template.html'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='ascii'
_exports = [u'breadcrumb', u'miniguide', u'form']


def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def breadcrumb():
            return render_breadcrumb(context.locals_(__M_locals))
        group_name = context.get('group_name', UNDEFINED)
        def miniguide():
            return render_miniguide(context.locals_(__M_locals))
        def form():
            return render_form(context.locals_(__M_locals))
        function_name = context.get('function_name', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'<!doctype html>\n<html>\n<head>\n    <title></title>\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge" />\n    <link rel="stylesheet" type="text/css" href="template_edit.css" />\n    <script src="script.js"></script>\n</head>\n<body>\n<div id="page">\n    <div id="header">\n        <div style="width: 260px; float: left">\n            <a href="index"><h1 id="logo">Title</h1></a><br/><br/>\n        </div>\n        <h1>Web interface to electricity production and pricing database</h1>\n    </div>\n    <div style="clear: both;"></div>\n    ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'breadcrumb'):
            context['self'].breadcrumb(**pageargs)
        

        # SOURCE LINE 19
        __M_writer(u'\n    <div id="navigation">\n        <!-- navigation here -->\n\n        <a href=\'tutorial\'>Tutorial</a><br/><br/>\n        <a href=\'whatisthis\'>What is this</a><br/><br/></div>\n</div>\n<div id="page_content">\n\n    <form method="get" action="')
        # SOURCE LINE 28
        __M_writer(unicode(function_name))
        __M_writer(u'">')
        __M_writer(unicode(group_name))
        __M_writer(u' data download :<br/><br/>\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'form'):
            context['self'].form(**pageargs)
        

        # SOURCE LINE 29
        __M_writer(u'\n\n        <button type="submit">Get data</button></form>\n\n</div><br/>\n</form>\n\n</div>\n\n<div id="metadata">\n    ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'miniguide'):
            context['self'].miniguide(**pageargs)
        

        # SOURCE LINE 39
        __M_writer(u'\n</div>\n<div style="clear: both;"></div>\n</div>\n<footer>\n    <div id="backtotop">\n        <a href="#header">[back to top]</a>\n    </div>\n    <div id="copyright">\n        <!-- copyright here -->\n    </div>\n</footer>\n</body>\n</html>\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_breadcrumb(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        def breadcrumb():
            return render_breadcrumb(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_miniguide(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        def miniguide():
            return render_miniguide(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_form(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        def form():
            return render_form(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


