# -*- encoding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 6
_modified_time = 1436973605.086188
_template_filename='template/epex_spot.html'
_template_uri='template/epex_spot.html'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='ascii'
_exports = [u'breadcrumb', u'miniguide', u'form']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'form_template.html', _template_uri)
def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def breadcrumb():
            return render_breadcrumb(context.locals_(__M_locals))
        def miniguide():
            return render_miniguide(context.locals_(__M_locals))
        def form():
            return render_form(context.locals_(__M_locals))
        parent = context.get('parent', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'<!doctype html>\n\n')
        # SOURCE LINE 3
        __M_writer(u'\n\n')
        # SOURCE LINE 5
        __M_writer(unicode(parent.header()))
        __M_writer(u'\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'breadcrumb'):
            context['self'].breadcrumb(**pageargs)
        

        # SOURCE LINE 8
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'form'):
            context['self'].form(**pageargs)
        

        # SOURCE LINE 20
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'miniguide'):
            context['self'].miniguide(**pageargs)
        

        # SOURCE LINE 33
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_breadcrumb(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        def breadcrumb():
            return render_breadcrumb(context)
        __M_writer = context.writer()
        # SOURCE LINE 6
        __M_writer(u'\n<div id="breadcrumb">Index > <span style="color:red;">EPEX spot prices data selection</span></div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_miniguide(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        def miniguide():
            return render_miniguide(context)
        __M_writer = context.writer()
        # SOURCE LINE 22
        __M_writer(u'\nThis selects data from the following tables :<br/>\n<br/>\n<strong>EPEX_DayAheadHour_CH</strong> (Day Ahead, Hourly, Switzerland)<br/>\n<strong>EPEX_DayAheadHour_FR</strong> (Day Ahead, Hourly, France)<br/>\n<strong>EPEX_DayAheadHour_DE</strong> (Day Ahead, Hourly, Germany)<br/>\n<br/>\n<strong>EPEX_DayAheadBlocks_DE</strong> (Day Ahead, Quarterhourly, Germany)<br/>\n<strong>EPEX_DayAheadBlocks_FR</strong> (Day Ahead, Quarterhourly, France)<br/>\n<strong>EPEX_DayAheadBlocks_CH</strong> (Day Ahead, Quarterhourly, Switzerland)<br/>\n<br/>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_form(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        def form():
            return render_form(context)
        __M_writer = context.writer()
        # SOURCE LINE 10
        __M_writer(u'\n<input type="radio" name="type" value="intraday">Intraday</input><br/>\n<input type="radio" name="type" value="dayahead">Day Ahead</input><br/><br/>\n\n<input type="radio" name="resolution" value="hourly">Hourly resolution</input><br/>\n<input type="radio" name="resolution" value="quarterhourly">Quarterhourly resolution</input><br/><br/>\n\n<input type="radio" name="country" value="ch">Switzerland</input><br/>\n<input type="radio" name="country" value="fr">France</input><br/>\n<input type="radio" name="country" value="de">Germany</input><br/><br/>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


