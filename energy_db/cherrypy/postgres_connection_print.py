#!/usr/bin/env python

__author__ = 'logerais'

import cherrypy
import psycopg2
import datetime


class DatabaseConnection:
    @cherrypy.expose()
    def open_connection(self):
        try:
            conn = psycopg2.connect(dbname = "energy_price", user = "logerais", host = "eifpostgres02", port = "5432", database = "griessbaum", password = "Logerais")
            success = "<html><body>Hey, the connection works"
        except:
            print("Unable to connect to the database.")
            success = "<html><body>Nope."
        success += "</body></html>"

        return success

    @cherrypy.expose()
    def determine_query_daterange(self, startdate=datetime.datetime(2014, 1, 1, 0, 0, 0), enddate=datetime.datetime(2015, 1, 1, 0, 0, 0)):
        if isinstance(startdate, str):
                try:
                        startdate=datetime.datetime.strptime(startdate, "%Y-%m-%d %H:%M")
                except ValueError:
                        print "The starting date does not correspond to the expected format. The format is \"YYYY-MM-DD HH:MM\"\nBe careful, the double quotes matter !"
        if isinstance(enddate, str):
                try:
                        enddate=datetime.datetime.strptime(enddate, "%Y-%m-%d %H:%M")
                except ValueError:
                        print "The starting date does not correspond to the expected format. The format is \"YYYY-MM-DD HH:MM\"\nBe careful, the double quotes matter !"
        if startdate <= enddate and startdate - datetime.timedelta(days=1) <= enddate - datetime.timedelta(days=1):
                return startdate, enddate

    def determine_query_fuel(self, fuel=0):
        ''' The fuel is stored as a number to avoid SQL injections.
        0 means gas
        1 means coal
        2 means lignite
        3 means uranium '''

        try:
                fuel_right_type = int(float(fuel))
                if fuel_right_type == 0:
                        return 'gas'
                elif fuel_right_type == 1:
                        return 'coal'
                elif fuel_right_type == 2:
                        return 'lignite'
                else:
                        return 'uranium'
        except ValueError:
                print("""The value entered is invalid. Possible entries : \n
                                0 for gas \n
                                1 for coal \n
                                2 for lignite \n
                                3 for uranium""")

    def plant_uses_chp(self, chp=False):
        return bool(chp)

    def query_builder(self):
        """

        :rtype : string, string, datetime.datetime, datetime.datetime
        """
        arguments = ["2014-02-01 00:00", "2014-04-01 00:00", "0", "False"]
        for i in xrange(0, len(arguments)):
                arguments[i] = str(arguments[i])
                startdate, enddate = self.determine_query_daterange(arguments[0], arguments[1])
                fuel = self.determine_query_fuel(arguments[2])

        if len(arguments) == 3:
                # If there are 3 arguments, that means chp is false
                chp = False
                startdate, enddate = determine_query_daterange(self, arguments[0], arguments[1])
                fuel = determine_query_fuel(self, arguments[2])
                initial_query = """SELECT "PowerPlants_Neu"."PlantID" FROM
                                      public."PowerPlants_Neu",
                                      public."TimeStamps",
                                      public."EEXTrans_Blockwise"
                                  WHERE
                                      "TimeStamps"."timeID" = "EEXTrans_Blockwise"."timeID" AND
                                      "PowerPlants_Neu"."PlantID" = "EEXTrans_Blockwise"."plantID" AND
                                      "TimeStamps"."TimeStamp" <= %s AND
                                      "TimeStamps"."TimeStamp" >= %s AND
                                      "PowerPlants_Neu"."Fuel" = %s AND
                                      "PowerPlants_Neu"."PowerThermal" IS NULL; """
        elif len(arguments) <=2:
                print("""Not enough arguments given. Usage :\n
                python plotting.py <startdate> <enddate> <fuel> <chp>
                startdate and enddate should be formatted following the standard iso format : "YYYY-MM-DD HH:MM"\n
                fuel is a number set to 0 (gas), 1 (coal), 2 (lignite), 3 (uranium)\n
                chp is a boolean value : 0 if we want data about plants without CHP and 1 if we want to show data about CHP plants.\n""")
                initial_query = ""
        else:
                startdate, enddate = determine_query_daterange(self, arguments[0], arguments[1])
                fuel = determine_query_fuel(self, arguments[2])
                chp = plant_uses_chp(self, arguments[3])

                if chp == True:
                        if fuel == 'gas':
                                initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'gas' AND
                                              "PowerPlants_Neu"."PowerThermal" > 0; """
                        elif fuel == 'coal':
                                initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'coal' AND
                                              "PowerPlants_Neu"."PowerThermal" > 0; """
                        elif fuel == 'lignite':
                                initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'lignite' AND
                                              "PowerPlants_Neu"."PowerThermal" > 0; """
                        elif fuel == 'uranium':
                                initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                             "PowerPlants_Neu"."Fuel" = 'uranium' AND
                                              "PowerPlants_Neu"."PowerThermal" > 0; """
                        else:
                                print "Error : The specified fuel is invalid."
                else:
                        if fuel == 'gas':
                                initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'gas' AND
                                              "PowerPlants_Neu"."PowerThermal" IS NULL; """
                        elif fuel == 'coal':
                                initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'coal' AND
                                              "PowerPlants_Neu"."PowerThermal" IS NULL; """
                        elif fuel == 'lignite':
                                initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'lignite' AND
                                              "PowerPlants_Neu"."PowerThermal" IS NULL; """
                        elif fuel == 'uranium':
                                initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'uranium' AND
                                              "PowerPlants_Neu"."PowerThermal" IS NULL; """
                        else:
                                print "Error : The specified fuel is invalid."
        return initial_query, fuel, enddate, startdate
    @cherrypy.expose()
    def showdata(self):
        initial_query, fuel, enddate, startdate = self.query_builder(self)
        cur.execute(initial_query, (enddate, startdate, fuel))
        ids = cur.fetchall()



if __name__ == '__main__':
    cherrypy.quickstart(DatabaseConnection(), '/')

