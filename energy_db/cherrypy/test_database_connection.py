#!/usr/bin/env python

__author__ = 'logerais'

import cherrypy
import os
import pylab
import matplotlib.pyplot as plt
import psycopg2
import matplotlib.dates
import csv

class DatabaseConnection:

    delimiter = ";"
    date_format = "%a %Y-%m-%d %H:%M"
    excluded_plants = []
    date_format_print = '%Y-%m-%d'

    @cherrypy.expose()
    def set_params_csv(self, delimiter_to_set, date_format_to_set):
        self.delimiter = delimiter_to_set
        self.date_format = date_format_to_set
        print self.delimiter
        print self.date_format
        return """<p>Parameters changed.</p><a href="javascript:history.back()">Go Back</a>"""

    @cherrypy.expose()
    def set_params_print(self, excluded_plants_to_set, date_format_to_set):
        separated = [x.strip() for x in excluded_plants_to_set.split(',')]
        for item in separated:
            if not item.isdigit():
                return "Error : wrong syntax"
        self.excluded_plants = separated
        self.date_format_print = date_format_to_set
        print self.excluded_plants
        print self.date_format_print
        return """<p>Parameters changed.</p><a href="javascript:history.back()">Go Back</a>"""

    @cherrypy.expose
    def index(self):
        return """
<!doctype html>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" type="text/css" href="template_edit.css" />
<script src="script.js"></script>
</head>
<body>
<div id="page">
<div id="header">
<div style="width: 260px; float: left">
<h1 id="logo" onclick="location.href='?view=Home'">Title</h1>
</div>
<h1>Data visualizer/printer for energy_price database</h1>
</div>
<div style="clear: both;"></div>
<div id="navigation">
<!-- navigation here -->

</div>
<div id="page_content">
<form method="get" action="open_connection">Startdate : <br/>
<input type="datetime-local" name="startdate"/><br/>Enddate : <br/>
<input type="datetime-local" name="enddate"/><br/>Fuel used : <br/>
<input type="text" name="fuel"/><br/>CHP usage : <br/>
<input type="radio" name="chp" value="True" checked/>True<br/>
<input type="radio" name="chp" value="False"/>False<br/>
<button type="submit">Print desired data</button>
</form>
</div>

		<div style="clear: both;"></div>
		<div id="backtotop">
			<a href="#header">[back to top]</a>
		</div>
		<div id="copyright">
		<!-- copyright here -->
		</div>
	</div>
</body>
</html>
"""

    def send_initial_query(self, connection, fuel, chp):
        cur = connection.cursor()
        if fuel == '0':
            initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'gas' AND """
        elif fuel == '1':
            initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'coal' AND """
        elif fuel == '2':
            initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'lignite' AND """

        elif fuel == '3':
            initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'oil' AND """
        elif fuel == '4':
            initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'uranium'"""
        elif fuel == '5':
            initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'pumped-storage'"""
        else:
            initial_query = """SELECT "PowerPlants_Neu"."PlantID"
                                                FROM
                                              public."PowerPlants_Neu"
                                                WHERE
                                              "PowerPlants_Neu"."Fuel" = 'seasonal-store'"""

        if fuel != '4' and fuel != '5' and fuel != '6':
            if chp == '0':
                initial_query += """ "PowerPlants_Neu"."PowerThermal" IS NULL; """
            else:
                initial_query += """ "PowerPlants_Neu"."PowerThermal" > 1; """

        cur.execute(initial_query)
        print initial_query
        return cur.fetchall(), cur

    @cherrypy.expose()
    def open_connection(self, startdate, enddate, fuel, chp):
        try:
            conn = psycopg2.connect(dbname = "energy_price", user = "logerais", host = "eifpostgres02", port = "5432", database = "griessbaum", password = "Logerais")
            success = """<!doctype html>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" type="text/css" href="template_edit.css" />
</head>
<body>
	<div id="page">
			<div id="header">
<div style="width: 260px; float: left">
<h1 id="logo" onclick="location.href='?view=Home'">EIFER Intern</h1>
</div>
<div id="search">
<form onsubmit="return false;">
Search:
<input type="text" value="" style="padding:2px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border-radius: 5px;width: 300px" onkeyup="Search(this.value)" placeholder="... phone numbers, names, groups, documents ..." name="search">
</form>
</div>
			<div id="data_export"><a href="#" onclick="options_export();">1. Export data to CSV format</a></div>
			<div id="data_howtouse"><a href="#" onclick="options_howtouse();">2. Short tutorial on how to use the data</a></div>
			<div id="data_print"><a href="#" onclick="options_print();">3. Plot data</a></div>
<div>
<form method="get" action="open_connection" class="data_input">Startdate :
<input type="datetime-local" name="startdate" />Enddate :
<input type="datetime-local" name="enddate"/>Fuel used :
<input type="text" name="fuel"/>CHP usage :
<input type="radio" name="chp" value="True" checked/>True
<input type="radio" name="chp" value="False"/>False
<button type="submit">Print desired data</button>
</form>
<div id="select_options"></div>
		</div>
		</div>
		<div id="navigation">
<!-- navigation here -->
<ul id="ulNavigation" class="filetree treeview"><li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span class="folder">EIFER</span><ul>
	<li><a class="file" href="?view=Home">Home</a></li>
	<li><a class="file" href="?view=EIFER_Directory">Directory</a></li>
	<li><a class="file" href="?view=EIFER_EIFER20">EIFER2.0</a></li>
	<li><a class="file" href="http://www.for.kit.edu/intranet/foerdat/index.php">Public funding</a></li>
	<li><a class="file" href="?view=EIFER_Whoiswho">Who's who</a></li>
	<li><a class="file" href="?view=EIFER_Communication">Communication</a></li>
	<li><a class="file" href="?view=EIFER_Newsletter">Newsletter</a></li>
	<li><a class="file" href="?view=EIFER_Documents">Documents</a></li>
	<li><a class="file" href="?view=EIFER_Templates">Templates</a></li>
	<li><a class="file" href="?view=EIFER_Processes">Processes</a></li>
	<li><a class="file" href="?view=EIFER_KnowledgeBase">Knowledge base</a></li>
	<li><a class="file" href="?view=EIFER_Safety">Safety</a></li>
	<li><a class="file" href="?view=EIFER_Betriebsrat">Betriebsrat</a></li>
	<li><a class="file" href="?view=EIFER_Gallery">Gallery</a></li>
	<li class="last"><a class="file" href="?view=EIFER_Chat">Chat</a></li></ul></li>
	<li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span class="folder">Publications</span><ul>
	<li><a class="file" href="?view=Publications_2011">2011</a></li>
	<li><a class="file" href="?view=Publications_2010">2010</a></li>
	<li class="last"><a class="file" href="?view=Publications_2009">2009</a></li></ul></li>
	<li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span class="folder">IT Service</span><ul>
	<li><a class="file" href="?view=IT_Maintenance">Maintenances</a></li>
	<li><a class="file" href="?view=4b431bde8a63569a8ada9a73728dbf0f">Operating rules</a></li>
	<li><a class="file" href="?view=IT_Formulars">Formulars</a></li>
	<li class="last"><a class="file" href="?view=IT_Manuals">Manuals</a></li></ul></li>
	<li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span class="folder">News</span><ul>
	<li><a class="file" href="?view=News">All News</a></li>
	<li><a class="file" href="?view=News_EIFER">EIFER</a></li>
	<li><a class="file" href="?view=News_EDF">EDF</a></li>
	<li class="last"><a class="file" href="?view=News_KIT">KIT</a></li></ul></li>
	<li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span class="folder">Miscellaneous</span><ul>
	<li><a class="file" href="?view=EIFER_Casino">Casino Menu</a></li>
	<li><a class="file" href="http://www.kvv.de/" target="_blank">Tram (KVV)</a></li>
	<li class="last"><a class="file" href="http://www.db.de/" target="_blank">Train (DB)</a></li></ul></li>
	<li class="collapsable lastCollapsable"><div class="hitarea collapsable-hitarea lastCollapsable-hitarea"></div><span class="folder">External Links</span><ul>
	<li><a class="file" href="https://ftp.eifer.uni-karlsruhe.de/xu2/index.php" target="_blank">EIFER Files (FTP)</a></li>
	<li><a class="file" href="http://eifer49:8080/geoportal/catalog/main/home.page" target="_blank">Geoportal (GIS-Metadata)</a></li>
	<li><a class="file" href="http://glpi.eifer.uni-karlsruhe.de" target="_blank">Ticketsystem GLPI</a></li>
	<li><a class="file" href="https://webmail.eifer.uni-karlsruhe.de" target="_blank">Webmail</a></li>
	<li><a class="file" href="https://quickreiferintern.eifer.uni-karlsruhe.de" target="_blank">Quickr Intern</a></li>
	<li class="last"><a class="file" href="https://quickreifer.eifer.uni-karlsruhe.de" target="_blank">Quickr Extern</a></li></ul></li></ul>
	<a href="?do=logout">Logout</a><br>
		</div>"""
        except:
            print("Unable to connect to the database.")
            success = """<html> <head>
<link rel="stylesheet" type="text/css" href="template_edit.css"/>
</head> <body>Nope."""
        ids, cur = self.send_initial_query(conn, fuel, chp)

        corrected_ids = ['']*len(ids)
        print ids
        for i in xrange(0, len(ids)):
            corrected_ids[i] = str(ids[i])
            corrected_ids[i] = corrected_ids[i][1:-2]

        print(corrected_ids)
        query_plantname = """select "PowerPlants_Neu"."FullName" from public."PowerPlants_Neu" where "PowerPlants_Neu"."PlantID" = %s"""
        success += """<div id="page_content"><table style="width:50%" class="plants_table">"""
        savingstring = """<div id="page_content"><table style="width:50%" class="plants_table">"""
        plant_names = []
        for i in xrange(0, len(corrected_ids)):
            success += "<tr>"
            success += "<td>"
            success += str(i+1)
            success += "</td>"
            cur.execute(query_plantname, (str(corrected_ids[i]),))
            plant_name = cur.fetchall()
            plant_name_string = str(plant_name[0])
            plant_name_string = plant_name_string[2:-3]
            plant_names.append(plant_name_string)
            success += "<td>"
            success += plant_name_string
            success += "</td>"
            success += "<td>"
            success += str(corrected_ids[i])
            success += "</td>"
            success += "</tr>"
            savingstring += "<tr>"
            savingstring += "<td>"
            savingstring += str(i+1)
            savingstring += "</td>"
            savingstring += "<td>"
            savingstring += plant_name_string
            savingstring += "</td>"
            savingstring += "<td>"
            savingstring += str(corrected_ids[i])
            savingstring += "</td>"
            savingstring += "</tr>"

        success += "</table>"
        savingstring += "</table>"

        menu_file = open("menu.html", "w")
        menu_file.write(savingstring)
        menu_file.close()

        query_specific = """SELECT "EEXTrans_Blockwise"."Power", "TimeStamps"."TimeStamp"
            FROM public."PowerPlants_Neu", public."EEXTrans_Blockwise", public."TimeStamps"
                WHERE
                "EEXTrans_Blockwise"."timeID" = "TimeStamps"."timeID" AND
                "EEXTrans_Blockwise"."plantID" = %s  AND
                "TimeStamps"."TimeStamp" >= %s and
                "TimeStamps"."TimeStamp" <= %s
                order by "TimeStamps"."TimeStamp" asc;"""

        time_series = []

        print corrected_ids
        try:
            os.remove('images/results.png')
        except OSError:
            print "No results file"
        for i in xrange(0, len(corrected_ids)):
            cur.execute(query_specific, (corrected_ids[i], startdate, enddate,))
            time_series.append(cur.fetchall())
            self.plot_series(time_series[i], plant_names[i])

        self.save_file(time_series, plant_names, "results.csv")
        success += """<br/><a href='/images/results.png' class='download_plot' download>Download image</a>
        <a href='/results.csv' style='position:relative;top:10px;left:30px;' download>Download csv data</a><br/>
        <img src='/images/results.png' alt='Graph' class='plot'>"""

        success += "</div><div id='tutorial_area'></div>t </div><script src='/script.js'></script><iframe id='frmFile' src='menu.html' onload='javascript:LoadFile();' style='display: none;'></iframe></body></html>"
        conn.close()
        return success

    def plot_series(self, timeSerie, name):
        time = []
        power = []
        for row in timeSerie:
                time.append(row[1])
                power.append(row[0])
        print name
        pylab.xlabel("Time")
        ax = plt.subplot(1, 1, 1)
        start, end = ax.get_xlim()
        date_list = []
        for i in xrange(0, 10):
            date_list.append(start + i*(end - start)/9)

        myFmt = matplotlib.dates.DateFormatter(self.date_format_print)
        ax.xaxis.set_major_formatter(myFmt)
        ax.xaxis.set_ticks(date_list)
        pylab.ylabel("Electric power in MW")
        plt.plot(time, power, label = name)

        try:
            pylab.title('Representation of the power produced by power plants over time')
            pylab.grid()
            pylab.legend(prop={'size':8})
            figure = plt.gcf() # get current figure
            figure.set_size_inches(20, 16)
            pylab.savefig('images/results.png')
        except:
            print("Error while plotting")

    def save_file(self, listofrows, plantnames, csvfile):
        csvoutput_list = []
        csvoutput_list.append(plantnames)
        csvoutput_list[0].insert(0, "Timestamps")
        L = max(listofrows, key=len)
        for i in xrange(0, len(L)):
            # Adding timestamps at the start of the table (first row)
            csvoutput_list.append([])
            csvoutput_list[i+1].append(L[i][1].strftime(self.date_format))

        for k in xrange(0, len(listofrows)): # iteration over the plants
            for j in xrange(0, len(listofrows[k])): # iteration over the timestamps
                csvoutput_list[j+1].append(listofrows[k][j][0])

        with open(csvfile, "w") as output:
            writer = csv.writer(output, lineterminator='\n', delimiter=';', skipinitialspace=True)
            writer.writerows(csvoutput_list)

if __name__ == '__main__':
    cherrypy.quickstart(DatabaseConnection(), '/', 'app.conf')