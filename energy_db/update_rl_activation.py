import energy_db.regelleistung.activation as activation


def update_all():
    """ Updates RL Activation tables from www.regelleistung.net """
    activation.update_mrl()
    activation.update_srl()


update_all()
print('Done.')
