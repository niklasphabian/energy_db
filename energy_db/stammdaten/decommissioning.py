import matplotlib.pyplot as plt
import pandas
import energyDB.database.database

db = energyDB.database.database.Database()

query1 = 'SELECT (power/1000), (decommissioning - commissioning)/365 \
         FROM public."Stammdaten_EEG" \
         WHERE decommissioning IS NOT NULL \
         AND source = \'wind\' \
         ORDER BY round(power/1000);'
         

query2 = 'SELECT floor(power/1000), round(avg((decommissioning - commissioning)/365),2) \
             FROM public."Stammdaten_EEG" \
         WHERE decommissioning IS NOT NULL \
         AND source = \'wind\' \
         GROUP BY floor(power/1000) \
         ORDER BY floor(power/1000);'


def plotquery(query, ms, m):         
    db.cursor.execute(query)
    res = db.cursor.fetchall()
    power = []
    decomage = []
    for row in res:
        print(row)
        power.append(float(row[0]))
        decomage.append(float(row[1]))
    plt.plot(power, decomage, m, markersize=ms)

plotquery(query1, 6, '.')
plotquery(query2, 10, 'D')
plt.legend(['decommissioning age', 'class average'])

plt.grid('on')
#plt.xlim([0,2000])
#plt.ylim([0,500])
plt.xlabel('Power in MW')
plt.ylabel('Decommissioning age in years')
plt.show()