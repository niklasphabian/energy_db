import matplotlib.pyplot as plt
import pandas
import energyDB.database.database

db = energyDB.database.database.Database()

query1 = 'SELECT t1.plz, t1.power, t2.area, t1.power/t2.area, t2.inhabitants/t2.area \
        FROM \
            (SELECT \
                FLOOR("Stammdaten_EEG".postleitzahl/{factor}) AS plz, \
                SUM("Stammdaten_EEG".power) as power \
            FROM "Stammdaten_EEG"  \
            WHERE source = \'wind\' \
            GROUP BY floor("Stammdaten_EEG".postleitzahl/{factor})) t1, \
            (SELECT \
                floor(postleitzahl/{factor}) as plz, \
                sum(area) as area, \
                sum(inhabitants) as inhabitants \
            FROM "PLZ" \
            GROUP BY floor(postleitzahl/{factor})) t2 \
        WHERE t1.plz=t2.plz \
        ORDER BY t1.power/t2.area;'.format(factor=1000)
        
query2 = 'SELECT "PLZ".postleitzahl, sum(power/1000), area, sum(power)/area, inhabitants/area \
          FROM public."Stammdaten_EEG_OldFormat", "PLZ" \
          WHERE "PLZ".postleitzahl="Stammdaten_EEG_OldFormat".postleitzahl \
          GROUP BY "PLZ".postleitzahl \
          ORDER BY sum(power)/area;'



db.cursor.execute(query2)
res = db.cursor.fetchall()


plz = []
density = []
popdensity = []
for row in res:
    print(row)
    plz.append(float(row[0]))
    density.append(float(row[3]))
    popdensity.append(float(row[4]))



df = pandas.DataFrame({'density':density})
#df = df[df<=1000]
#df.hist(bins=10)

plt.plot(density, popdensity, 'o')

plt.xscale('log')
plt.yscale('log')
plt.grid('on')
#plt.xlim([0,2000])
#plt.ylim([0,500])
plt.xlabel('Density in kW/km²')
plt.ylabel('number of occasions')
plt.ylabel('Population density in person/km²')

#plt.plot(x, nd(x))

plt.show()
    
