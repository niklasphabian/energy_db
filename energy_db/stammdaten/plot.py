import csv
import matplotlib.pyplot as plt
import pandas

plz = []
density = []
with open('out.csv') as csvfile:
  reader = csv.reader(csvfile)
  for row in reader:
    plz.append(float(row[0]))
    density.append(float(row[3]))

df = pandas.DataFrame({'density':density})
df[df.density<300].hist()
#df.sort().plot()
plt.xlabel('Density in MW/km²')
plt.ylabel('number of occasions')
plt.show()
    


