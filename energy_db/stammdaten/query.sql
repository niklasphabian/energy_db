SELECT stammdaten.plz as plz, sum(power) as power, plz.area as area, sum(power)/plz.area AS area
FROM stammdaten, plz
WHERE source = 'wind'
AND stammdaten.plz = plz.plz
AND stammdaten.plz > 6000
AND stammdaten.plz < 9999
AND commissioning > '2004-01-01'
GROUP BY plz.plz
ORDER BY plz.plz;

