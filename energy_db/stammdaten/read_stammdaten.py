#!/usr/bin/env python
# -*- coding: utf-8 -*-

import energyDB.stammdaten.stammdaten_table
import glob
import csv
import codecs
import datetime
import xlrd

class StammdatenFile_TSO:
    def __init__(self, filename):
        self.plz = []
        self.power = []
        self.source = []
        self.commissioning = []
        self.decommissioning = []
        self.anlagenschluessel = []
        self.repowering = []
        self.read_file(filename)
    
    def read_file(self, filename):    
        with codecs.open(filename, 'r', 'iso-8859-1') as infile:
            self.reader = csv.reader(infile, delimiter=';')
            self.header = [x.lower() for x in next(self.reader)]
            self.get_indexes()
            self.parse_file()
   
    def parse_file(self):
        for row in self.reader:
            self.plz.append(row[self.idx_plz].replace('X', '0'))
            self.power.append(row[self.idx_power].replace(',','.'))
            self.source.append(self.parse_source(row[self.idx_source]))
            self.commissioning.append(datetime.datetime.strptime(row[self.idx_commissioning], '%d.%m.%Y').date())            
            if self.idx_decommissioning and row[self.idx_decommissioning] is not '' :
                self.decommissioning.append(datetime.datetime.strptime(row[self.idx_decommissioning], '%d.%m.%Y').date())
            else: 
                self.decommissioning.append(None)
            self.anlagenschluessel.append(row[self.idx_anlagenschluessel])
            if self.idx_repowering :
                self.repowering.append(self.parse_repowering(row[self.idx_repowering]))                

    def parse_repowering(self, inval):        
        if inval == 'Ja' or inval == 'J':
            return True
        elif inval == 'Nein' or inval == 'N' or inval == 'keine':
            return False
        elif inval is None:
            return False
        else :
            return False
            
  
    def get_indexes(self):
        self.idx_plz = self.get_index(['plz', 'postleitzahl'])
        self.idx_power = self.get_index(['installierte leistung [kw]', 'install leistung', 'leistung'])
        self.idx_source = self.get_index(['energieträger', 'bezeichnung', 'energietraeger'])
        self.idx_commissioning = self.get_index(['zeitpunkt der inbetriebnahme', 'inbetriebnahme'])
        self.idx_decommissioning = self.get_index(['zeitpunkt der außerbetriebnahme', 'zeitpunkt der außerbetriebn', 'außerbetriebnahme'])
        self.idx_anlagenschluessel = self.get_index(['anlagenschluessel', 'anlagenschluessel','ünb-anlagennummer'])
        self.idx_repowering = self.get_index(['repowering'])
  
    def get_index(self, name):        
        intersect = list(set(self.header).intersection(name))
        if len(intersect) == 1 :
            idx = self.header.index(intersect[0])
        else: 
            print('Key {} not found'.format(name))
            idx = None
        return idx
        
    def parse_source(self, source):
        if 'wind' in str(source.lower()):
            return 'wind'
        else :
            return source
        
    def convert2num(self, instr):
        try: 
            return float(instr)
        except:
            print(instr)
            return None
        
    
class StammdatenFile_BNA(StammdatenFile_TSO):    
        
    def read_file(self, filename): 
        self.book = xlrd.open_workbook(filename)#, encoding_override='iso-8859-1')
        self.sheet = self.book.sheet_by_name(u'Gesamtübersicht')          
        self.keys = [self.sheet.cell(0, col_index).value for col_index in range(self.sheet.ncols)]
        self.get_indexes()
        self.parse_file()
        
    def extractDate(self, row, col):
        try :
            date = xlrd.xldate_as_tuple(self.sheet.cell(row, col).value, self.book.datemode)
            date = datetime.datetime(*date).date()
        except: 
            print('empty row: ' + str(row))
            date = None
        return date
            
    def parse_file(self):
        for row in range(1, self.sheet.nrows):
            anlagenschluessel = self.sheet.cell(row, self.idx_anlagenschluessel).value
            if len(anlagenschluessel) > 0 :
                self.anlagenschluessel.append(anlagenschluessel)            
                self.plz.append(self.convert2num(self.sheet.cell(row, self.idx_plz).value))
                self.power.append(self.convert2num(self.sheet.cell(row, self.idx_power).value))
                self.source.append(self.parse_source(self.sheet.cell(row, self.idx_source).value.encode('utf8')))
                self.commissioning.append(self.extractDate(row, self.idx_commissioning))
                self.decommissioning.append(self.extractDate(row, self.idx_decommissioning))
                self.repowering.append(self.parse_repowering(self.sheet.cell(row, self.idx_repowering).value))
        
    def get_indexes(self):
        self.idx_anlagenschluessel = self.keys.index('1.8 EEG-Anlagenschlüssel')
        self.idx_plz = self.keys.index('4.9 Postleit-zahl')
        self.idx_power = self.keys.index('4.2 Installierte Leistung [kW]')
        self.idx_source = self.keys.index('4.1 Energieträger')
        self.idx_commissioning = self.keys.index('4.3 Tatsächliche Inbetrieb-nahme')
        self.idx_decommissioning = self.keys.index('4.5 Stilllegungs-datum')
        self.idx_repowering = self.keys.index('7.5 Repowering')
        

def update_old_format():
    mydb = energyDB.stammdaten.stammdaten_table.Stammdaten_OldFormat()
    mydb.wipe()
    mydb.commit()    
    filelist = sorted(glob.glob('/home/likewise-open/EIFERKITEDU/griessbaum/workspace/energyDB/DataArchive/Stammdaten/old_format/*.csv'))
    for filename in filelist:
        myfile = StammdatenFile_TSO(filename)
        for idx in range(len(myfile.plz)):        
            mydb.insert_or_update(myfile.plz[idx], myfile.power[idx], myfile.source[idx], myfile.commissioning[idx], myfile.decommissioning[idx], myfile.repowering[idx], myfile.anlagenschluessel[idx])
            mydb.commit()
    myfile = StammdatenFile_BNA('/home/likewise-open/EIFERKITEDU/griessbaum/workspace/energyDB/DataArchive/Stammdaten/old_format/2015_11_Veroeff_AnlReg.xls')
    for idx in range(len(myfile.anlagenschluessel)):
        mydb.insert_or_update(myfile.plz[idx], myfile.power[idx], myfile.source[idx], myfile.commissioning[idx], myfile.decommissioning[idx], myfile.repowering[idx], myfile.anlagenschluessel[idx])
    mydb.commit()
        
    

def update_new_format(mydb):
    mydb = energyDB.stammdaten.stammdaten_table.Stammdaten()
    mydb.wipe()
    mydb.finish()

    filelist = sorted(glob.glob('/home/likewise-open/EIFERKITEDU/griessbaum/workspace/energyDB/DataArchive/Stammdaten/*.csv'))
    for filename in filelist:
        print(filename)
        myfile = StammdatenFile_TSO(filename)
        for idx in range(len(myfile.plz)):            
            mydb.insert_or_update(myfile.plz[idx], myfile.power[idx], myfile.source[idx], myfile.commissioning[idx], myfile.decommissioning[idx], myfile.anlagenschluessel[idx])
        



update_old_format()








