from energyDB.dbTable.dbTable import DBTable

class Stammdaten(DBTable):
    table_name = 'Stammdaten_EEG'
    
    def deleteIfAlreadyExists(self, anlagenschluessel):
        query = 'DELETE FROM "{}" WHERE anlagenschluessel=?;'.format(self.table_name)        
        self.cursor.execute(query, (anlagenschluessel))
    
    def insert_row(self, plz, power, source, commissioning, decommissioning, anlagenschluessel):        
        query = 'INSERT INTO "{}" \
                (postleitzahl, power, source, commissioning, decommissioning, anlagenschluessel) VALUES (%s, %s, %s, %s, %s, %s);'.format(self.table_name)
        self.cursor.execute(query, (plz, power, source, commissioning, decommissioning, anlagenschluessel))
    
    def insert_or_update(self, plz, power, source, commissioning, decommissioning, anlagenschluessel):
        query = 'SELECT anlagenschluessel FROM "{}" WHERE anlagenschluessel=%s'.format(self.table_name)
        if self.execute_fetchone(query, (anlagenschluessel,)):
            print('Plant {} already existed in table. Updating.'.format(anlagenschluessel))
            self.update_row(plz, power, source, commissioning, decommissioning, anlagenschluessel)
        else:            
            self.insert_row(plz, power, source, commissioning, decommissioning, anlagenschluessel)
        
    def update_row(self, plz, power, source, commissioning, decommissioning, anlagenschluessel):
        query = 'UPDATE "{}"  SET postleitzahl=%s, power=%s, source=%s, commissioning=%s, decommissioning=%s WHERE anlagenschluessel = %s;'.format(self.table_name)
        self.cursor.execute(query, (plz, power, source, commissioning, decommissioning, anlagenschluessel))
        
class Stammdaten_OldFormat(Stammdaten):
    table_name = 'Stammdaten_EEG_OldFormat'
    
    def insert_row(self, plz, power, source, commissioning, decommissioning, repowering, anlagenschluessel):        
        query = 'INSERT INTO "{}" \
                (postleitzahl, power, source, commissioning, decommissioning, repowering, anlagenschluessel) VALUES (%s, %s, %s, %s, %s, %s, %s);'.format(self.table_name)
        self.cursor.execute(query, (plz, power, source, commissioning, decommissioning, repowering, anlagenschluessel))

    def insert_or_update(self, plz, power, source, commissioning, decommissioning, repowering, anlagenschluessel):
        query = 'SELECT anlagenschluessel FROM "{}" WHERE anlagenschluessel=%s'.format(self.table_name)
        if self.execute_fetchone(query, (anlagenschluessel,)):
            print('Plant {} already existed in table. Updating.'.format(anlagenschluessel))
            self.update_row(plz, power, source, commissioning, decommissioning, repowering, anlagenschluessel)
        else:            
            self.insert_row(plz, power, source, commissioning, decommissioning, repowering, anlagenschluessel)
        
    def update_row(self, plz, power, source, commissioning, decommissioning, repowering, anlagenschluessel):
        query = 'UPDATE "{}"  SET postleitzahl=%s, power=%s, source=%s, commissioning=%s, decommissioning=%s, repowering=%s WHERE anlagenschluessel = %s;'.format(self.table_name)
        self.cursor.execute(query, (plz, power, source, commissioning, decommissioning, repowering, anlagenschluessel))