from energy_db.epex_spot.intraday import DEUpdater, CHUpdater, FRUpdater


def update_all():
    de = DEUpdater()
    ch = CHUpdater()
    fr = FRUpdater()
    de.update_to_today()
    ch.update_to_today()
    fr.update_to_today()


update_all()
print('Done.')
