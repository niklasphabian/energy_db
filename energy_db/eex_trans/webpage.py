import requests
import time
import urllib3
import hashlib
import binascii
import datetime


class Base:
    urlTrunk = 'https://www.eex-transparency.com'
                
    def __init__(self):               
        self.get_ip()
        self.session = requests.Session()
        self.session.headers.update({'Referer':self.urlRefer})            
        
    def point_in_time(self):
        return int(time.mktime(time.localtime()))+60
    
    def get_ip(self):
        http = urllib3.PoolManager()
        self.my_ip = http.urlopen('GET','http://ip.42.pl/raw').data.decode()
    
    def parse_date(self, date):
        self.year = str(date.year)
        self.month = '{:02d}'.format(date.month)
        self.day = '{:02d}'.format(date.day)
        self.hour = '{:02d}'.format(date.hour)
    
    def md5(self, point_in_time):
        instring = self.urlPath + str(point_in_time) + self.my_ip + " zYeHzBomGdgV";
        instring = instring.encode()
        m = hashlib.md5()
        m.update(instring)
        md5 = binascii.b2a_base64(m.digest()).decode('ascii')[0:-3]
        md5 = md5.replace('/', '_').replace('+', '-').replace('%','_')
        return md5
        
    def download(self, date): 
        self.makeURLPath(date)
        point_in_time = self.point_in_time()
        fail = True        
        while fail:
            try:
                params = {'expires': point_in_time, 'md5': self.md5(point_in_time)}
                url = self.urlTrunk + self.urlPath
                payload = self.session.get(url, params=params)
                payload.encoding = 'utf-8'
                json = payload.json()
                fail = False
            except:
                print('DL fail. Error downloading {url}: Try again in {t_wait} sec'.format(url=url, t_wait=10))
                time.sleep(10)
        return json


class Blockwise(Base):
    urlRefer = 'https://www.eex-transparency.com/homepage/power/germany/production/usage/actual-unitwise-production-'
    def makeURLPath(self, date):  
        self.parse_date(date)
        self.urlPath = self.urlSpecifier + self.year + '/' + self.month + '/' + self.day + '/' + self.hour + '.json'           


class BlockwiseDE(Blockwise):    
    urlSpecifier = '/static/tem-10a/de/'


class BlockwiseBE(Blockwise):    
    urlSpecifier = '/static/tem-10a/be/'


class BlockwiseNL(Blockwise):    
    urlSpecifier = '/static/tem-10a/nl/'        


class Sectors(Base):
    def makeURLPath(self, date):
        self.parse_date(date)
        self.urlPath = self.urlSpecifier + self.year + '/' + self.month + '/' + self.day +  '.json'


class HunMWActualPageDE(Sectors):
    urlRefer = 'http://www.eex-transparency.com/startseite/strom/deutschland/produktion/nutzung/actual-production-of-generating-units-100-mw-'
    urlSpecifier = '/static/tem-10a-aggregated/de/'


class HunMWExpectedPageDE(Sectors):
    urlRefer = 'http://www.eex-transparency.com/startseite/strom/deutschland/produktion/nutzung/geplante-produktion-100-mw-tabelle-'
    urlSpecifier = '/static/tem-5a/de/'        


class SolarWindActualPageDE(Sectors):
    urlRefer = 'https://www.eex-transparency.com/homepage/power/germany/production/usage/solar-wind-power-production'
    urlSpecifier = '/static/tem-7/de/'


class SolarWindExpectedPageDE(Sectors):
    urlRefer = 'https://www.eex-transparency.com/homepage/power/germany/production/usage/solar-wind-power-production'
    urlSpecifier = '/static/tem-6/de/'
    

def test():
    wp = SolarWindActualPageDE()
    date = datetime.datetime(2016,3,2,19)
    print(wp.download(date))
    
    


