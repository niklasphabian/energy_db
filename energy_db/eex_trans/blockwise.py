import datetime
import random
import time
from eex_trans.blockwise_table import PlantTable
from eex_trans.blockwise_table import PowerGenTable
from database.time_stamps_table import TimeStampTableBlockwise
import eex_trans.webpage
import pytz
cet = pytz.timezone('CET')
pst = pytz.timezone('US/Pacific')


def sleep(current):
    t_sleep = int(round(1 * random.random(), 0))
    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(now + ": Downloading %s, but first sleeping for %s sec" % (str(current), t_sleep))
    time.sleep(t_sleep)


class Downloader:
    def __init__(self):
        self.plantTable = PlantTable()
        self.genTable = PowerGenTable()
        self.tsTable = TimeStampTableBlockwise()
        self.data = None
        self.ts_id = None
        
    def download_one(self, timestamp):
        cet_timestamp = timestamp.astimezone(cet)
        self.data = self.web_page.download(timestamp)
        self.ts_id = self.tsTable.get_time_stamp_id(cet_timestamp)

    def update_to_today(self):
        start_timestamp = self.genTable.latest_entry_date()
        end_timestamp = cet.localize(datetime.datetime.now())
        timestamp = start_timestamp
        while timestamp < end_timestamp:
            sleep(timestamp)
            self.download_one(timestamp)
            self.upload()
            timestamp += datetime.timedelta(hours=1)

    def upload(self):
        for entry in self.data['data']:
            plant_id = self.plantTable.get_plant_id(entry['prodcon'], entry['unit'], entry['company'], entry['fuel'], self.country)
            self.genTable.add_record(plant_id, self.ts_id, entry['energy'])
        self.genTable.commit()
            

class DownloaderDE(Downloader):
    country = 'DE'
    web_page = eex_trans.webpage.BlockwiseDE()


class DownloaderBE(Downloader):    
    country = 'BE'
    web_page = eex_trans.webpage.BlockwiseBE()
    
    
class DownloaderNL(Downloader):
    country = 'NL'
    web_page = eex_trans.webpage.BlockwiseNL()


def test():
    dl = DownloaderDE()
    timestamp = cet.localize(datetime.datetime(2014, 1, 1, 0))
    dl.download_one(timestamp)
    dl.upload()
    
if __name__ == '__main__':
    test()


