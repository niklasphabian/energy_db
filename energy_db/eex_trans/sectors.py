import time
from eex_trans.webpage import HunMWActualPageDE, HunMWExpectedPageDE, SolarWindActualPageDE, SolarWindExpectedPageDE
from eex_trans.sectors_table import WindTableActualDE, WindTableExpectedDE
from eex_trans.sectors_table import SolarTableActualDE, SolarTableExpectedDE
from eex_trans.sectors_table import HunMWTableActualDE, HunMWTableExpectedDE
import pytz
import datetime
import random
cet = pytz.timezone('Europe/Amsterdam')


class DataHandler:

    def __init__(self):
        self.payload = None
        self.db_table = None

    def download(self, date):        
        self.payload = self.page.download(date)     
    
    def update_record(self, timestamp, column, power):
        if self.db_table.time_stamp_does_exist(timestamp):
            self.db_table.update_record(timestamp, column, power)
        else:
            self.db_table.add_record(timestamp, column, power)
        self.db_table.commit()
    
    def update_range(self, start_date, end_date):
        date = start_date    
        while date <= end_date:
            self.sleep(date)
            self.download(date)
            self.upload()
            date = date + datetime.timedelta(days=1)
            
    def update_to_today(self):
        start_date = self.latest_entry()
        print(start_date)
        yesterday = datetime.date.today() 
        end_date = cet.localize(datetime.datetime(yesterday.year, yesterday.month, yesterday.day))
        self.update_range(start_date, end_date)

    @staticmethod
    def sleep(current_date):
        now = time.strftime("%Hh%M")
        t_sleep = int(round(2 * random.random(), 0))
        print(now + ": Downloading %s, but first sleeping for %s sec" % (str(current_date.date()), t_sleep))
        time.sleep(t_sleep)    
       
       
class HunMW(DataHandler):        
    def upload(self):
        for entry in self.payload['data']:
            timestamp = time.strftime('%Y-%m-%d %H:%M:%S %Z', time.localtime(entry['timepoint']/1000))
            fuel = entry['fuel']
            power = entry[self.energyTag]
            self.update_record(timestamp, fuel, power)

    def latest_entry(self):
        if self.db_table.latest_entry() is not None:
            return self.db_table.latest_entry()
        else:
            return cet.localize(datetime.datetime(2009, 10, 25))

        
class HunMWActualDE(HunMW):

    def __init__(self):
        self.db_table = HunMWTableActualDE()
        self.page = HunMWActualPageDE()
        self.energyTag = 'energy'


class HunMWExpected(HunMW):

    def __init__(self):
        self.db_table = HunMWTableExpectedDE()
        self.page = HunMWExpectedPageDE()
        self.energyTag = 'planned_energy'


class SolarWind(DataHandler):
    
    def __init__(self):
        self.wind_table_actual = WindTableActualDE()
        self.solar_table_actual = SolarTableActualDE()
        self.wind_table_expected = WindTableExpectedDE()
        self.solar_table_expected = SolarTableExpectedDE()
        
    def latest_entry(self):
        if self.solar_table_actual.latest_entry() is not None:
            return self.solar_table_actual.latest_entry()
        else:
            return cet.localize(datetime.datetime(2009, 10, 25))


    @staticmethod
    def download_payload_actual(date):
        actual_page = SolarWindActualPageDE()
        payload = actual_page.download(date)
        return payload

    @staticmethod
    def download_payload_expected(date):
        expected_page = SolarWindExpectedPageDE()
        payload = expected_page.download(date)
        return payload
    
    def download(self, date):
        self.payload_expected = self.download_payload_expected(date)
        self.payload_actual = self.download_payload_actual(date)
    
    def upload(self):
        self.upload_payload(self.payload_actual, self.wind_table_actual, self.solar_table_actual)        
        self.upload_payload(self.payload_expected, self.wind_table_expected, self.solar_table_expected)

    @staticmethod
    def upload_payload(payload, wind_table, solar_table):
        tso_dict = {'TransnetBW': 'transnet_bw', 'Tennet (DE)': 'tennet', 'Amprion': 'amprion', '50Hertz': 'fz_hertz'}
        
        for entry in payload['data']:            
            power = entry['energy']
            time_stamp = time.strftime('%Y-%m-%d %H:%M:%S %Z', time.localtime(entry['timepoint']/1000))        
            tso = tso_dict[entry['connecting_area']]
            fuel = entry['fuel']
            if fuel == 'wind-offshore':
                column = 'offshore_' + tso
                wind_table.upsert_record(time_stamp, column, power)
            elif fuel == 'wind-onshore':
                column = 'onshore_' + tso
                wind_table.upsert_record(time_stamp, column, power)
            elif fuel == 'solar':
                column = tso
                solar_table.upsert_record(time_stamp, column, power)


def update_hun_mw_actual():
    dh = HunMWActualDE()
    dh.update_to_today()


def update_hun_mw_expected():
    dh = HunMWExpected()
    dh.update_to_today()


def update_solar_wind():
    dh = SolarWind()
    dh.update_to_today()


def test():
    start_date = datetime.datetime(2009, 10, 25)
    end_date = datetime.datetime(2009, 10, 30)

    dh = SolarWind()
    dh.update_range(start_date=start_date, end_date=end_date)
    dh = HunMWActualDE()
    dh.update_range(start_date=start_date, end_date=end_date)
    dh = HunMWExpected()
    dh.update_range(start_date=start_date, end_date=end_date)


if __name__ == '__main__':
    #test()
    update_solar_wind()
