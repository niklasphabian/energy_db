from database.db_table import DBTable


class HunMWTable(DBTable):

    def add_record(self, timestamp, column, power):
        query = 'INSERT INTO "{schema_name}"."{table_name}" \
                (time_stamp, "{column}") SELECT %s, %s'\
            .format(schema_name=self.schema_name, table_name=self.table_name, column=column)
        self.cursor.execute(query, (timestamp, power))
    
    def update_record(self, timestamp, column, power):
        query = 'UPDATE "{schema_name}"."{table_name}" \
                SET "{col}" = %s \
                WHERE time_stamp = %s' \
            .format(schema_name=self.schema_name, table_name=self.table_name, col=column)
        self.cursor.execute(query, (power, timestamp))


class HunMWTableExpectedDE(HunMWTable):
    schema_name = 'eex_trans'
    table_name = '100mw_expected_de'


class HunMWTableActualDE(HunMWTable):
    schema_name = 'eex_trans'
    table_name = '100mw_actual_de'


class SolarWindTable(DBTable):

    def update_record(self, timestamp, column, power):
        query = 'UPDATE "{schema_name}"."{table_name}" ' \
                'SET "{column}" = %s ' \
                'WHERE time_stamp = %s' \
            .format(schema_name=self.schema_name, table_name=self.table_name, column=column)
        self.cursor.execute(query, (power, timestamp,))
        
    def add_record(self, timestamp, column, power):
        self.delete_if_already_exists(timestamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" (time_stamp, "{column}") ' \
                'SELECT %s,%s' \
            .format(schema_name=self.schema_name, table_name=self.table_name, column=column)
        self.cursor.execute(query, (timestamp, power,))
    
    def upsert_record(self, timestamp, column, power):
        if self.time_stamp_does_exist(timestamp):
            self.update_record(timestamp, column, power)
        else:
            self.add_record(timestamp, column, power)
        self.commit()

    def get_hour(self, hour):
        query = 'SELECT SUM(onshore_fz_hertz + onshore_amprion + onshore_tennet + onshore_transnet_bw)/count(onshore_fz_hertz)\
                FROM "{schema_name}"."{table_name}"\
                WHERE date_part( \'hour\', time_stamp) = %s' \
            .format(schema_name=self.schema_name, table_name=self.table_name)
        self.cursor.execute(query, (hour,))
        return self.cursor.fetchone()[0]


class SolarTableActualDE(SolarWindTable):
    schema_name = 'eex_trans'
    table_name = 'solar_actual_de'


class SolarTableExpectedDE(SolarWindTable):
    schema_name = 'eex_trans'
    table_name = 'solar_expected_de'


class WindTableActualDE(SolarWindTable):
    schema_name = 'eex_trans'
    table_name = 'wind_actual_de'


class WindTableExpectedDE(SolarWindTable):
    schema_name = 'eex_trans'
    table_name = 'wind_expected_de'
