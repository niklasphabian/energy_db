from database.db_table import DBTable
import pandas
import datetime
import pytz
cet = pytz.timezone('CET')


class PowerGenTable(DBTable):
    table_name = 'blockwise_de'
    schema_name = 'blockwise'

    def add_record(self, plant_id, time_id, power):
        self.delete_if_already_exists(plant_id, time_id)
        query = 'INSERT INTO {schema_name}.{table_name} (plant_id, time_id, power) SELECT %s,%s,%s;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (plant_id, time_id, power,))        
      
    def delete_if_already_exists(self, plant_id, time_id):
        pkey = self.get_pkey(plant_id, time_id)        
        if pkey:
            self.delete_row_ts(pkey)
    
    def delete_row_ts(self, pkey_list):
        query = 'DELETE FROM {schema_name}.{table_name} WHERE "pkey" = %s'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        for pkey in pkey_list:
            self.cursor.execute(query, (pkey,))    
    
    def get_pkey(self, plant_id, time_id):        
        query = 'SELECT "pkey" FROM {schema_name}.{table_name} WHERE plant_id = %s AND time_id = %s;'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchall(query, (plant_id, time_id,))        
        if len(ret) == 0:
            return False
        else:
            return ret
    
    def get_time_series(self, plant_id, start_time, end_time):
        query = 'SELECT time_stamp, power \
        FROM {schema_name}.{table_name}, {schema_name}."time_stamps" \
        WHERE time_stamps.time_id = {table_name}.time_id \
        AND time_stamp >= %s \
        AND time_stamp <= %s \
        AND plant_id = %s \
        order by time_stamp ASC;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchall(query, (start_time, end_time, plant_id))        
        return ret
    
    def get_time_series_pandas(self, plant_id, start_time, end_time):
        ret = self.get_time_series(plant_id, start_time, end_time)
        return pandas.DataFrame.from_records(ret, columns=['time_stamp', 'power'], index='time_stamp')
    
    def latest_entry_date(self):
        query = 'SELECT max(time_stamps.time_stamp) \
            FROM    {schema_name}.{table_name},\
                    {schema_name}.time_stamps\
            WHERE   {table_name}.time_id = time_stamps.time_id'\
            .format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchone(query)[0]
        if ret is None:
            # In case the database is empty
            ret = cet.localize(datetime.datetime(2014, 1, 1))
        return ret
       
       
class PlantTable(DBTable):
    table_name = 'power_plants'
    schema_name = 'blockwise'

    def add_plant(self, company, unit, plant, fuel, country):
        full_name = plant+unit   
        query = 'INSERT INTO {schema_name}.{table_name} (company, unit, plant, fuel, full_name, country) ' \
                'VALUES (%s,%s,%s,%s,%s,%s);' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        self.cursor.execute(query, (company, unit, plant, fuel, full_name, country))
        self.commit()
        return self.query_plant(unit, plant)
        
    def query_plant(self, plant, unit):
        full_name = plant+unit
        query = 'SELECT plant_id FROM {schema_name}.{table_name} WHERE full_name = %s;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchone(query, (full_name,))
        if ret is None:
            return None
        else: 
            return ret[0]      
        
    def get_plant_id(self, plant, unit, company, fuel, country):
        ret = self.query_plant(plant, unit)
        if ret is None:
            plant_id = self.add_plant(company, unit, plant, fuel, country)
        else:
            plant_id = ret
        return plant_id
    
    def unique_chp_plants(self):
        query = 'SELECT full_name, plant_id, fuel, power_electric ' \
                'FROM {schema_name}.{table_name} ' \
                'WHERE power_thermal > 0;' \
            .format(table_name=self.table_name, schema_name=self.schema_name)
        ret = self.execute_fetchall(query, ())
        return ret
    

