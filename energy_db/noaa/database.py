import sqlite3
import pytz
import datetime
import pandas
cet =  pytz.timezone('CET')


class tempDB:    
    def __init__(self):               
        self.conn = sqlite3.connect(self.name)        
        self.cursor = self.conn.cursor()
    
    def printAllRows(self):    
        self.cursor.execute('SELECT * FROM {tabName}'.format(tabName=self.tabName))
        rows = self.cursor.fetchall()
        for row in rows:
            print(row)    
    
    def commit(self):
        self.conn.commit()
    
    def clear(self):
        self.cursor.execute('DELETE FROM {tabName}'.format(tabName=self.tabName))
        self.commit()
    
    def disconnect(self):
        self.conn.close()


class Predictions(tempDB):
    name = 'noaa.db'
    tabName = 'predictions'
            
    def createTab(self):
        self.cursor.execute('DROP TABLE if exists {tabName}'.format(tabName=self.tabName))
        self.cursor.execute('CREATE TABLE {tabName}(TimeStamp datetime, FCHour int, Temp real)'.format(tabName=self.tabName))
    
    def insert_row(self, timeStamp, fcHour, temp):
        self.deleteRow(timeStamp, fcHour)
        self.cursor.execute('INSERT INTO {tabName} VALUES(?,?,?)'.format(tabName=self.tabName), (timeStamp, fcHour, temp))
        
    def deleteRow(self, timeStamp, fcHour):
        querry = 'DELETE FROM {tabName} WHERE {tabName}."TimeStamp"=? AND {tabName}."FCHour"=?'.format(tabName=self.tabName)
        self.cursor.execute(querry, (timeStamp, fcHour))        
            
    def getTS(self, fcHour, starttime, endtime):
        self.cursor.execute("SELECT TimeStamp, Temp FROM predictions WHERE FCHour=? AND TimeStamp >=? AND TimeStamp < ?", (fcHour,starttime, endtime))
        rows = self.cursor.fetchall()
        timeList = []
        tempList = []
        for row in rows:     
            timestamp = cet.localize(datetime.datetime.strptime(row[0],"%Y-%m-%d %H:%M:%S" ))       
            timeList.append(timestamp)
            tempList.append(row[1])            
        prediction = pandas.Series(tempList, index=timeList, name = 'Temp Prediction {fcHour}'.format(fcHour=fcHour))        
        return prediction
          

        
class Actual(tempDB):
    name = 'actualKA.db'
    tabName = 'actual'
        
    def insert_row(self, timeStamp, temp):        
        self.cursor.execute("INSERT INTO actual VALUES(?,?)", (timeStamp, temp))
        
    def deleteRowTS(self, timeStamp):
        querry = 'DELETE FROM "{tabName}" WHERE "{tabName}"."TimeStamp" = %s'.format(tabName=self.tabName)
        self.cursor.execute(querry, (timeStamp,))        
        
    def getTS(self, starttime, endtime):
        self.cursor.execute("SELECT TimeStamp, Temp FROM actual WHERE TimeStamp >=? AND TimeStamp < ?", (starttime, endtime))        
        rows = self.cursor.fetchall()
        timeList = []
        tempList = []
        for row in rows:             
            tzinfo = row[0][-6:-3]
            ts = row[0][0:19]            
            #timestamp = datetime.datetime.strptime(ts+tzinfo+'00','%Y-%m-%d %H:%M:%S%z')
            timestamp = datetime.datetime.strptime(ts,'%Y-%m-%d %H:%M:%S')
            timestamp = cet.localize(timestamp)
            timeList.append(timestamp)
            tempList.append(row[1])            
        prediction = pandas.Series(tempList, index=timeList, name = 'Temp actual')        
        return prediction
            
