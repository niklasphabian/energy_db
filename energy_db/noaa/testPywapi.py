'''
Created on Jul 14, 2015

@author: griessbaum
'''

import  pywapi
import string

weather_com_result = pywapi.get_weather_from_weather_com('10001')
yahoo_result = pywapi.get_weather_from_yahoo('10001')
noaa_result = pywapi.get_weather_from_noaa('KJFK')

print "Weather.com says: It is " + string.lower(weather_com_result['current_conditions']['text']) + " and " + weather_com_result['current_conditions']['temperature'] + "C now in New York.\n\n"

print "Yahoo says: It is " + string.lower(yahoo_result['condition']['text']) + " and " + yahoo_result['condition']['temp'] + "C now in New York.\n\n"

print "NOAA says: It is " + string.lower(noaa_result['weather']) + " and " + noaa_result['temp_c'] + "C now in New York.\n"


import urllib2, urllib, json
baseurl = "https://query.yahooapis.com/v1/public/yql?"
yql_query = 'SELECT * from weather.forecast where woeid in (SELECT woeid FROM geo.places(1) where text="Karlsruhe, Baden-Wurttemberg")'
yql_url = baseurl + urllib.urlencode({'q':yql_query}) + "&format=json"
result = urllib2.urlopen(yql_url).parse()
data = json.loads(result)


for day in data['query']['results']['channel']['item']['forecast']:
    date = day['date']
    maxTemp = day['high']
    minTemp = day['low']
    text = day['text']
    print('{date} max: {maxTemp} F, min: {minTemp} F. {text}'.format(date=date,maxTemp=maxTemp,minTemp=minTemp,text=text))
    
    