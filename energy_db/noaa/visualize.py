import pytz
import datetime
import pandas
import matplotlib.pyplot as plt
import database 
cet =  pytz.timezone('CET')


starttime = datetime.datetime(2014,1,1) 
endtime = datetime.datetime(2014,1,30)

actualDB = database.Actual()
production_table = database.Predictions()
pred0 = production_table.getTS(0, starttime, endtime)
pred6 = production_table.getTS(192, starttime, endtime)
actual = actualDB.getTS(starttime, endtime)

#frame = pandas.concat([actual,pred0, pred6], axis=1, join='inner')
#frame = frame.resample('6h', how='last')
pred0=pandas.DataFrame(pred0)
pred6=pandas.DataFrame(pred6)
print(pred6)
frame = pandas.merge(pred0, pred6, how='outer', left_index='True', right_index='True')

fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 10))
axes.grid()
frame.plot(ax=axes)
actual.plot(axes=axes)
axes.set_ylabel('Temperature in degC')
plt.show()
