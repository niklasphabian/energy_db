import energyDB.climate.db_table
import datetime
import pytz
import database 
cet =  pytz.timezone('CET')

def getActual():  
    startTime = cet.localize(datetime.datetime(2010,1,1))
    endTime = cet.localize(datetime.datetime(2015,6,30))
    dbTab = energyDB.climate.db_table.TemperatureTable()
    ret = dbTab.get_time_series(startTime, endTime, 4177)
    return ret

actualDB = database.Actual()

actual = getActual()
for row in actual:
    timestamp = row[0]
    temp = row[1]
    actualDB.insert_row(timestamp, temp)

actualDB.commit()
actualDB.disconnect()