import pygrib
import datetime
import glob
import pytz
from database import Predictions

cet =  pytz.timezone('CET')
utc = pytz.timezone('UTC')

class gribFile():
    def __init__(self, fname):
        self.fname = fname
        self.read()
        self.getDevisor()        
        self.parseMetaData()
        self.getKarlsruheTemp()        
        
    def read(self):
        self.gr = pygrib.open(self.fname)            
        self.msg = self.gr[1]         
        
    def getDevisor(self):
        self.gribVersion = self.msg.GRIBEditionNumber
        if self.gribVersion == 2:
            self.devisor = 1000*1000.0
        elif self.gribVersion == 1:
            self.devisor = 1000.0

    def parseMetaData(self):                
        self.date =  self.msg.analDate  
        #self.fcHour = self.msg.forecastTime
        self.fcHour = int(self.msg.stepRange)                
        self.lvl = self.msg.level
    
    def getKarlsruheTemp(self):               
        lonKa = 8.2
        latKA = 49.0
        x = int(round((90.0-latKA)/(self.msg.iDirectionIncrement/self.devisor)))
        y = int(round(lonKa/(self.msg.jDirectionIncrement/self.devisor)))
        #temp = self.gr[1].values[82][16]-273   # 0.5 deg domain
        self.temp = self.msg.values[x][y]-273    # 1 deg domain        
    
    def printFeedback(self):             
        print('{date}. Forcast {fcTime} hours: {temp} degC (at {lvl} m above ground)'.format(date=self.date, fcTime=self.fcHour, temp=self.temp, lvl=self.lvl))
        
def readFolder(folderName):
    production_table = Predictions()    
    fileList = sorted(glob.glob(folderName + '*.grb2'))
    fileList.extend(sorted(glob.glob(folderName + '*.grb')))    
    for fileName in fileList :
        myGribFile = gribFile(fileName)
        myGribFile.printFeedback()
        deltaT = datetime.timedelta(hours = myGribFile.fcHour)
        timestamp = myGribFile.date+deltaT
        production_table.insert_row(timestamp, myGribFile.fcHour, myGribFile.temp)
    production_table.commit()
    production_table.disconnect()
    

def readAllFolders():
    folderList = sorted(glob.glob('/home/likewise-open/EIFER/griessbaum/Climate/20*'))
    for folderName in folderList :        
        readFolder(folderName+'/')

folderName = '/home/likewise-open/EIFER/griessbaum/Climate/2014-03-01/'
readFolder(folderName)
#readAllFolders()