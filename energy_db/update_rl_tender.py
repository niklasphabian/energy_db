import energy_db.regelleistung.tender as tender


def update_to_today():
    tender.update_to_today_mrl()
    tender.update_to_today_srl()
    tender.update_to_today_prl()


if __name__ == '__main__':
    update_to_today()
    print('Done.')
