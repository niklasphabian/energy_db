import energy_db.eex.emission_allowance as emission_allowance


def update_all():
    emission_allowance.update_to_today()


update_all()
print('Done.')
