from energy_db.database.db_table import DBTable
import pandas


class DBTableHour(DBTable):

    def create_table(self):
        query = 'CREATE TABLE IF NOT EXISTS "{schema_name}"."{table_name}" ' \
                       '(ID SERIAL PRIMARY KEY, time_stamp TIMESTAMP, price double precision, volume double precision);'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query)
        self.database.commit()
    
    def insert_row(self, timestamp, price, volume):
        self.delete_if_already_exists(timestamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, price, volume) ' \
                'SELECT %s, %s, %s' \
            .format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query, (timestamp, price, volume))

    def upsert_dataframe(self, df):
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, price, volume) ' \
                'SELECT %s, %s, %s'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        for row in df.iterrows():
            time_stamp = str(row[1]['time_stamp'])
            price = nat2none(row[1]['price'])
            volume = nat2none(row[1]['volume'])
            self.delete_if_already_exists(time_stamp)
            self.database.cursor.execute(query, (time_stamp, price, volume))
        self.commit()

    def get_time_series(self, start_time, end_time):
        query = 'SELECT time_stamp, price \
                FROM "{schema_name}"."{table_name}" \
                WHERE time_stamp >= %s \
                AND time_stamp <= %s' \
            .format(schema_name=self.schema_name, table_name=self.table_name)
        ret = self.execute_fetchall(query, (start_time, end_time,))
        return ret      
    
    def get_time_series_pandas(self, start_time, end_time):
        ret = self.get_time_series(start_time, end_time)
        return pandas.DataFrame.from_records(ret, columns=['time_stamp', 'price'], index='time_stamp')
    
    def get_avg_price(self, year):
        query = 'SELECT AVG(price) \
                FROM "{schema_name}"."{table_name}" \
                WHERE DATE_PART(\'YEAR\', "{tabName}".time_stamp) = %s' \
            .format(schema_name=self.schema_name, table_name=self.table_name)
        ret = self.execute_fetchone(query, (year, ))
        return ret    


class DBTableHourDE(DBTableHour):
    schema_name = 'epexspot'
    table_name = 'dayahead_hour_de'


class DBTableHourCH(DBTableHour):
    schema_name = 'epexspot'
    table_name = 'dayahead_hour_ch'


class DBTableHourFR(DBTableHour):
    schema_name = 'epexspot'
    table_name = 'dayahead_hour_fr'


class DBTableBasePeak(DBTable):

    def create_table(self):
        create_query = 'CREATE TABLE IF NOT EXISTS "{schema_name}"."{table_name}"\
                        (ID SERIAL PRIMARY KEY, time_stamp TIMESTAMP, price_base double precision, ' \
                       'volume_base double precision, price_peak double precision, volume_peak double precision);' \
            .format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(create_query)
        self.database.commit()
    
    def insert_row(self, timestamp, price_base, volume_base, price_peak, volume_peak):
        self.delete_if_already_exists(timestamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, price_base, volume_base, price_peak, volume_peak) ' \
                'SELECT %s, %s, %s, %s, %s' \
            .format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query, (timestamp, price_base, volume_base, price_peak, volume_peak,))

    def upsert_dataframe(self, df):
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, price_base, volume_base, price_peak, volume_peak) ' \
                'SELECT %s, %s, %s, %s, %s'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        for row in df.iterrows():
            time_stamp = str(row[1]['time_stamp'])
            price_base = nat2none(row[1]['price_base'])
            volume_base = nat2none(row[1]['volume_base'])
            price_peak = nat2none(row[1]['price_peak'])
            volume_peak = nat2none(row[1]['volume_peak'])
            self.delete_if_already_exists(time_stamp)
            self.database.cursor.execute(query, (time_stamp, price_base, volume_base, price_peak, volume_peak))
        self.commit()


class DBTableBasePeakDE(DBTableBasePeak):
    schema_name = 'epexspot'
    table_name = 'dayahead_basepeak_de'


class DBTableBasePeakCH(DBTableBasePeak):
    schema_name = 'epexspot'
    table_name = 'dayahead_basepeak_ch'


class DBTableBasePeakFR(DBTableBasePeak):
    schema_name = 'epexspot'
    table_name = 'dayahead_basepeak_fr'


class DBTableBlocks(DBTable):
    
    def create_table(self):        
        query = 'CREATE TABLE IF NOT EXISTS "{schema_name}"."{table_name}" ' \
                '(ID SERIAL PRIMARY KEY, time_stamp TIMESTAMP '
        for field in self.fields:
            query = query + ', "' + field + '" double precision'
        query += ');'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query)
        self.database.commit()
    
    def inject_offpeaks(self, timestamp, off_peak1, off_peak2):
        self.delete_if_already_exists(timestamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, "off_peak1", "off_peak2") SELECT %s, %s, %s'\
            .format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query, (timestamp, off_peak1, off_peak2,))

    def upsert_dataframe(self, df):
        for row in df.iterrows():
            time_stamp = row[1]['time_stamp']
            self.delete_if_already_exists(time_stamp)
            query = 'INSERT INTO "{schema_name}"."{table_name}" (time_stamp) SELECT %s;'
            query = query.format(schema_name=self.schema_name, table_name=self.table_name)
            self.database.cursor.execute(query, (time_stamp,))
            for key in df.columns:
                value = row[1][key]
                query = 'UPDATE "{schema_name}"."{table_name}" ' \
                        'SET {key} = %s ' \
                        'WHERE time_stamp = %s;'
                query = query.format(schema_name=self.schema_name, table_name=self.table_name, key=key)
                self.database.cursor.execute(query, (value, time_stamp))
            self.commit()


class DBTableBlocksFR(DBTableBlocks):
    table_name = 'dayahead_blocks_fr'
    schema_name = 'epexspot'

    fields = ['middle_night', 'early_morning', 'late_morning', 'early_afternoon',
              'rush_hour', 'off_peak2', 'baseload', 'peakload', 'night', 'off_peak1',
              'business', 'off_peak', 'morning', 'high_noon', 'afternoon', 'evening', 'sun_peak']

    def insert_row(self, timestamp, b):
        self.delete_if_already_exists(timestamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" (time_stamp'
        for field in self.fields:
            query = query + ', "' + field + '"'
        query += ") SELECT %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s"
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query, (timestamp, b[0], b[1], b[2], b[3], b[4],
                                             b[5], b[6], b[7], b[8], b[9], b[10], b[11],
                                             b[12], b[13], b[14], b[15], b[16],))


class DBTableBlocksCH(DBTableBlocks):
    table_name = 'dayahead_blocks_ch'
    schema_name = 'epexspot'

    fields = ['middle_night', 'early_morning', 'late_morning',
              'early_afternoon', 'rush_hour', 'off_peak2', 'night',
              'off_peak1', 'business', 'off_peak', 'morning',
              'high_noon', 'afternoon', 'evening', 'sun_peak']
    
    def insert_row(self, timestamp, b):
        self.delete_if_already_exists(timestamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" (time_stamp'
        for field in self.fields:
            query = query + ', "' + field + '"'
        query += ") SELECT %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s"
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query, (timestamp, b[0], b[1], b[2], b[3], b[4],
                                             b[5], b[6], b[7], b[8], b[9], b[10],
                                             b[11], b[12], b[13], b[14],))


class DBTableBlocksDE(DBTableBlocksCH):
    table_name = 'dayahead_blocks_de'
    schema_name = 'epexspot'


class DBTableIntraDay(DBTable):
    def __init__(self, table_name, schema_name):
        super(DBTableIntraDay, self).__init__()
        self.schema_name = schema_name
        self.table_name = table_name

    def create_table(self):
        query = 'CREATE TABLE IF NOT EXISTS "{schema_name}"."{table_name}" ' \
                '(ID SERIAL PRIMARY KEY, time_stamp TIMESTAMP,  ' \
                'low double precision, high double precision, last double precision, ' \
                'weighted_avg double precision, index double precision, ' \
                'buy_volume double precision, sell_volume double precision)'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query)
        self.commit()
    
    def insert_row(self, time_stamp, row):
        self.delete_if_already_exists(time_stamp)
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, low, high, last, weighted_avg, index, buy_volume, sell_volume) ' \
                'SELECT %s, %s, %s, %s, %s, %s, %s, %s'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        self.database.cursor.execute(query, (time_stamp, row[0], row[1], row[2], row[3], row[4], row[5], row[6],))

    def insert_dataframe(self, df):
        df.to_sql(name=self.table_name, con=self.database.engine, schema=self.schema_name, if_exists='append', index=False)

    def upsert_dataframe(self, df):
        query = 'INSERT INTO "{schema_name}"."{table_name}" ' \
                '(time_stamp, time_stamp_end, low, high, last, weighted_avg, index, buy_volume, sell_volume) ' \
                'SELECT %s, %s, %s, %s, %s, %s, %s, %s, %s'
        query = query.format(schema_name=self.schema_name, table_name=self.table_name)
        for row in df.iterrows():
            time_stamp = str(row[1]['time_stamp'])
            time_stamp_end = str(row[1]['time_stamp_end'])
            low = nat2none(row[1]['low'])
            high = nat2none(row[1]['high'])
            last = nat2none(row[1]['last'])
            w_avg = nat2none(row[1]['weighted_avg'])
            index = nat2none(row[1]['index'])
            buy = nat2none(row[1]['buy_volume'])
            sell = nat2none(row[1]['sell_volume'])
            self.delete_if_already_exists(time_stamp)
            self.database.cursor.execute(query, (time_stamp, time_stamp_end, low, high, last, w_avg, index, buy, sell))
        self.commit()


def nat2none(value):
    if str(value) == 'NaT':
        return None
    else:
        return value
