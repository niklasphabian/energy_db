import time
import datetime
import pytz
from energy_db.epex_spot.db_table import DBTableIntraDay
import energy_db.epex_spot.webpage as webpage
cet = pytz.timezone('CET')


class Updater:

    def sleep(self, day):
        now = time.strftime("%Y-%m-%d %H:%M:%S")
        print('{now}: Downloading intraday {country}. {day}'.format(now=now, country=self.country, day=day))
        time.sleep(1)

    def update_to_today(self):
        latest_entry = self.table_hourly.latest_entry()
        if latest_entry is None:
            latest_entry = cet.localize(datetime.datetime(2007, 7, 12))
        today = cet.localize(datetime.datetime.now())
        self.update_range(latest_entry, today)

    def update_range(self, start_date, end_date):
        date = start_date
        while date < end_date:
            self.sleep(date)
            self.webpage.set_date(date)
            self.webpage.download_page()
            self.webpage.parse()
            self.upload()
            date += datetime.timedelta(days=1)
        print('Download complete')

    def upload(self):
        self.table_hourly.upsert_dataframe(self.webpage.df_hourly)
        self.table_15min.upsert_dataframe(self.webpage.df)


class DEUpdater(Updater):
    country = 'DE'
    table_hourly = DBTableIntraDay(table_name='intraday_de_hourly', schema_name='epexspot')
    table_15min = DBTableIntraDay(table_name='intraday_de', schema_name='epexspot')
    webpage = webpage.IntradayWebpageDE()


class CHUpdater(Updater):
    country = 'CH'
    table_hourly = DBTableIntraDay(table_name='intraday_ch_hourly', schema_name='epexspot')
    table_15min = DBTableIntraDay(table_name='intraday_ch', schema_name='epexspot')
    webpage = webpage.IntradayWebpageCH()


class FRUpdater(Updater):
    country = 'FR'
    table_hourly = DBTableIntraDay(table_name='intraday_fr_hourly', schema_name='epexspot')
    webpage = webpage.IntradayWebpageFR()

    def upload(self):
        self.table_hourly.upsert_dataframe(self.webpage.df_hourly)


if __name__ == '__main__':
    updater = DEUpdater()
    updater.update_to_today()
