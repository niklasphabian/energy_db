from energy_db.epex_spot.db_table import DBTableHourDE, DBTableHourCH, DBTableHourFR
from energy_db.epex_spot.db_table import DBTableBasePeakDE, DBTableBasePeakCH, DBTableBasePeakFR
from energy_db.epex_spot.db_table import DBTableBlocksDE, DBTableBlocksCH, DBTableBlocksFR
import energy_db.epex_spot.webpage as webpage
import pytz
import datetime
import time

cet = pytz.timezone('CET')


class Updater:

    def __init__(self):
        self.webpage = webpage.DayAheadWebpage()

    def sleep(self, day):
        now = time.strftime("%Y-%m-%d %H:%M:%S")
        print('{now}: Downloading day_ahead, {day}'.format(now=now, day=day))
        time.sleep(1)

    def update_range(self, start_date, end_date):
        date = start_date - datetime.timedelta(days=1)
        while date < end_date:
            self.sleep(date)
            self.update_day(date)
            date = date + datetime.timedelta(days=7)

    def update_day(self, date):
        self.webpage.set_date(date)
        self.webpage.download_page()
        self.webpage.parse()
        self.upload()

    def upload(self):
        self.upload_fr()
        self.upload_de()
        self.upload_ch()

    def upload_fr(self):
        hour_table = DBTableHourFR()
        basepeak_table = DBTableBasePeakFR()
        blocks_table = DBTableBlocksFR()
        hour_table.upsert_dataframe(df=self.webpage.hours_fr)
        basepeak_table.upsert_dataframe(df=self.webpage.basepeak_fr)
        blocks_table.upsert_dataframe(df=self.webpage.blocks_fr)

    def upload_de(self):
        hour_table = DBTableHourDE()
        basepeak_table = DBTableBasePeakDE()
        blocks_table = DBTableBlocksDE()
        hour_table.upsert_dataframe(df=self.webpage.hours_de)
        basepeak_table.upsert_dataframe(df=self.webpage.basepeak_de)
        blocks_table.upsert_dataframe(df=self.webpage.blocks_de)

    def upload_ch(self):
        hour_table = DBTableHourCH()
        basepeak_table = DBTableBasePeakCH()
        blocks_table = DBTableBlocksCH()
        hour_table.upsert_dataframe(df=self.webpage.hours_ch)
        basepeak_table.upsert_dataframe(df=self.webpage.basepeak_ch)
        blocks_table.upsert_dataframe(df=self.webpage.blocks_ch)


if __name__ == '__main__':
    date = datetime.datetime(2018, 3, 2)
    updater = Updater()
    updater.update_day(date)
