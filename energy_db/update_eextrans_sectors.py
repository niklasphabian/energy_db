import energy_db.eex_trans.sectors as sectors


def update_all():
    print('### updating 100 MW actual ###')
    sectors.update_hun_mw_actual()
    print('### updating 100 MW expected ###')
    sectors.update_hun_mw_expected()
    print('### updating Solar+Wind actual + expected ###')
    sectors.update_solar_wind()


if __name__ == '__main__':
    update_all()
    print('Done.')
