﻿INSERT INTO "SRL_Demand" ("Date", "Time", "Power")
SELECT
    f."Date", f."Time", f."Value"
FROM
    (SELECT * FROM "2010_SRL_Bedarf"
     UNION ALL
     SELECT * FROM "2011_SRL_Bedarf"
     UNION ALL
     SELECT * FROM "2012_SRL_Bedarf"
     UNION ALL      
     SELECT * FROM "2013_SRL_Bedarf"
     UNION ALL 
     SELECT * FROM "2014_06_SRL_Bedarf") as f;