import requests
import csv
from bs4 import BeautifulSoup
import datetime


class RLDemandCollector:
    pass

class RLDemandWebsite:
    def __init__(self,sessionID):
        self.url='https://www.regelleistung.net/ip/action/abrufwert/index.html'
        self.sessionid = sessionID
    

    def payloadConstructor(self):
        payload = {'CSRFToken':'db8ee734-719d-4b45-a76b-c9121fd6d1af', 
            '__fp':'VcCBwkeNCmLRMDhwurENRD9O2iKjThLF1Ytv5BZMFFU=',
            '_sourcePage ':'27adtWeUl9V-2-FO90ZH9ITA8W7E8o2DD0AWGDrb4M2xynYAlnEkbw==', 
            'produkt':'RZ_SALDO',
            'uenbId':'wF1B3FMIi5Y=', 
            'von':'20.05.2013',
            'work':'show'}
        return payload


    def cookieCreator(self):
        cookies = dict(JSESSIONID=self.sessionid, preferredLocale='en')
        return cookies

    def getData(self):        
        payload = self.payloadConstructor()
        cookies = self.cookieCreator()
        resp = requests.post(self.url, cookies=cookies, data=payload, allow_redirects=True)
        htmlOut= resp.text
        
        return htmlOut
        
    
class DemandData:
    
    def loadHtml(self, html):
        self.html = html
    
    def html2table(self):    
        soup = BeautifulSoup(self.html)
        htmltable = soup.find(lambda tag: tag.name=='table' and tag.has_attr ('id') and tag['id']=="abrufwert")
        
        self.table=[]
        for row in htmltable.findAll('tr'):
            temprow=[]                    
            for val in row.findAll('td'):
                value=val.text.encode('utf8')
                value=str(value)
                if value =='\n':
                    temprow.append(' ')
                else:  
                    temprow.append(value)            
            self.table.append(temprow)
            self.table.pop(0)
    
    
    def modifyDecimalSeperator(self):
        for row in self.table:
            for value in row:
                value=value.replace('.', '')
                value=value.replace(',', '.')
        
    
    def modifyDate(self):                
        for r in range(len(self.table)):                       
            myDate = datetime.datetime.strptime(self.table[r][0], '%d.%m.%Y')
            myDate = myDate.date().isoformat()
            self.table[r][0]= myDate
        
    
    
    
    def printCSV(self):
        headers = [header.text for header in self.table.findAll('th')]        
        f=open('filename', 'w')
        csvout = csv.writer(f, delimiter=';', quotechar=' ')
        csvout.writerow(headers)    
        for row in self.table.findAll('tr'):
            temprow=[]        
            for val in row.findAll('td'):
                value=val.text.encode('utf8')
                value=str(value)
                
                value=value.replace(',', '.')
                if value =='\n':
                    temprow.append(' ')
                else:  
                    temprow.append(value)
                    str_date = 'nDate'
                    datetime.datetime.strptime(str_date, '%m/%d/%Y').date().isoformat()
                    csvout.writerow(temprow)
                    f.close() 
               


        

class Database:
    pass