﻿DELETE 
FROM 
  public."EPEX_IntraDay_DE"
WHERE 
  "EPEX_IntraDay_DE"."Low" + 
  "EPEX_IntraDay_DE"."Last" +
  "EPEX_IntraDay_DE"."High" +
  "EPEX_IntraDay_DE"."WeightAvg" +
  "EPEX_IntraDay_DE"."Index" +
  "EPEX_IntraDay_DE"."BuyVol" + 
  "EPEX_IntraDay_DE"."SellVol" = 0;
