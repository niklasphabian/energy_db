import os
import glob


theDir='/home/likewise-open/EIFER/griessbaum/03_N43/0_PROJECTS/PROJECTS_EDF/DEC14 - Leipzig/06_Data/04_ControlReserve/ReserveTenders/originals/prix 2013/'

def getDir(theDir):    
    os.chdir(theDir)
    dirContents=glob.glob('*.*')
    return dirContents 

def getFilesNames(theDir):    
    os.chdir(theDir)
    dirContents=glob.glob('*.*')
    return dirContents 

def replaceStuff(inFilename,outFilename):  
    outfile=open(outFilename,'w')
    with open(inFilename,'rb') as openfile :
        Content=openfile.read()
        mystring = Content.replace(",", ".")            
        outfile.write(mystring)

def replaceWalker(dirContents):
    for inFilename in dirContents :    
        print inFilename    
        outFilename='out'+'/'+inFilename
        replaceStuff(inFilename,outFilename)

def changeEndings(dirContents):
    for thisFile in dirContents :        
        base = os.path.splitext(thisFile)[0]
        os.rename(thisFile, base + ".csv")
        
def main() :
    dirContents = getDir(theDir)    
    print dirContents
    changeEndings(dirContents)
    dirContents = getDir(theDir)
    replaceWalker(dirContents)

main()

    

    
