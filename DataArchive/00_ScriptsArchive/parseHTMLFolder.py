import glob
from parseHTMLTable import parseHTMLFile
import os

currentDir = os.path.dirname(os.path.realpath(__file__))+ '/'
folderName='outHTML/auction/'
Dir=glob.glob(folderName+'*.html')

for fName in Dir :
    parseHTMLFile(fName, currentDir)