--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.13
-- Dumped by pg_dump version 9.3.13
-- Started on 2016-08-11 10:27:27 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 11756)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2325 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 260 (class 1255 OID 37145812)
-- Name: substitue_plantid(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION substitue_plantid(old_id integer, new_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
  BEGIN
    UPDATE eextrans_blockwise_de SET plant_id = new_id WHERE plant_id = old_id;
    UPDATE power_plants SET power_electric = (SELECT power_electric FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET renovation_year = (SELECT renovation_year FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET construction_year = (SELECT construction_year FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET plz = (SELECT plz FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET power_thermal = (SELECT power_thermal FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET urban = (SELECT urban FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET disposal_plant = (SELECT disposal_plant FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET bnetz_kraftwerksnummer = (SELECT bnetz_kraftwerksnummer FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET industrial = (SELECT industrial FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    DELETE FROM power_plants WHERE plant_id = old_id;
    END;
 $$;


SET default_with_oids = false;

--
-- TOC entry 207 (class 1259 OID 19709212)
-- Name: eextrans_100mw_actual_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eextrans_100mw_actual_de (
    time_stamp timestamp with time zone,
    power real,
    id integer NOT NULL,
    coal real,
    garbage real,
    lignite real,
    "pumped-storage" real,
    "seasonal-store" real,
    "wind-offshore" real,
    "coal-derived-gas" real,
    gas real,
    oil real,
    "run-of-the-river" real,
    uranium real,
    "wind-onshore" real
);


--
-- TOC entry 2326 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE eextrans_100mw_actual_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eextrans_100mw_actual_de IS 'Actual electricity production of power plants with a power output of 100 MW and more in Germany over time in hourly resolution.
Data retrieved from www.eex-transparency.com

TimeStamp: Timestamp in ISO 8601 format
Power: Power output in MW
id: Primary key

Before 2015-07-29, only combined values for all fuel types are availble.
From 2015-07-29 on, data is availible broken down to coal-derived-gas, gas, hard-coal, lignite, uranium, oil, pump-storage, run-off-the-river, garbage, seasonal-storage and wind-offshore.';


--
-- TOC entry 208 (class 1259 OID 19709222)
-- Name: 100MW_actual_DE__ID_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "100MW_actual_DE__ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2327 (class 0 OID 0)
-- Dependencies: 208
-- Name: 100MW_actual_DE__ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "100MW_actual_DE__ID_seq" OWNED BY eextrans_100mw_actual_de.id;


--
-- TOC entry 209 (class 1259 OID 19709233)
-- Name: eextrans_100mw_expected_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eextrans_100mw_expected_de (
    time_stamp timestamp with time zone,
    power real,
    id integer NOT NULL,
    coal real,
    garbage real,
    lignite real,
    "pumped-storage" real,
    "seasonal-store" real,
    "wind-offshore" real,
    "coal-derived-gas" real,
    gas real,
    oil real,
    "run-of-the-river" real,
    uranium real,
    other real,
    "wind-onshore" real
);


--
-- TOC entry 2328 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE eextrans_100mw_expected_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eextrans_100mw_expected_de IS 'Planned electricity production of power plants with a power output of 100 MW and more in Germany over time in hourly resolution
Data retrieved from www.eex-transparency.com

TimeStamp: Timestamp in ISO 8601 format
Power: Power output in MW
id: Primary key

Before 2015-07-29, only combined values for all fuel types are availble.
From 2015-07-29 on, data is availible broken down to coal-derived-gas, gas, hard-coal, lignite, uranium, oil, pump-storage, run-off-the-river, garbage, seasonal-storage, other, wind-onshore and wind-offshore.';


--
-- TOC entry 210 (class 1259 OID 19709239)
-- Name: 100MW_expected_DE_ID_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "100MW_expected_DE_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2329 (class 0 OID 0)
-- Dependencies: 210
-- Name: 100MW_expected_DE_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "100MW_expected_DE_ID_seq" OWNED BY eextrans_100mw_expected_de.id;


--
-- TOC entry 232 (class 1259 OID 27116808)
-- Name: climate_temperature_by_station; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE climate_temperature_by_station (
    time_id integer,
    station_id integer,
    air_temp double precision,
    rel_humidity double precision,
    id integer NOT NULL
);


--
-- TOC entry 2330 (class 0 OID 0)
-- Dependencies: 232
-- Name: TABLE climate_temperature_by_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE climate_temperature_by_station IS 'Temperature and humidity measured by DWD weather stations.
Data retrieved from ww.dwd.de

TimeID: ID of the timestamp (Translatable into human-readable timestamps through TimeStamps table)
StationID: ID of the station (Metadata of station translatable through Climate_Stations table)
AirTemp: Measured air temperature in degree celsius
RelHumity: Relative air humidity in percent';


--
-- TOC entry 231 (class 1259 OID 27116806)
-- Name: ClimateTemperatureStations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ClimateTemperatureStations_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2331 (class 0 OID 0)
-- Dependencies: 231
-- Name: ClimateTemperatureStations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ClimateTemperatureStations_id_seq" OWNED BY climate_temperature_by_station.id;


--
-- TOC entry 238 (class 1259 OID 27426511)
-- Name: climate_solar_by_station; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE climate_solar_by_station (
    time_id integer,
    station_id integer,
    sunshine_duration integer,
    diffuse_power integer,
    global_power integer,
    atmosphere integer,
    id integer NOT NULL
);


--
-- TOC entry 2332 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE climate_solar_by_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE climate_solar_by_station IS 'Sunshine time and radiation power measured by DWD weather stations.
Data retrieved from ww.dwd.de

TimeID: ID of the timestamp (Translatable into human-readable timestamps through TimeStamps table)
StationID: ID of the station (Metadata of station translatable through Climate_Stations table)
DiffusePower: Measured diffuse radiation power in Joule per square meter per hour
GlobalPower: Measured global radiation power in Joule per square meter per hour
Atmosphere: ?';


--
-- TOC entry 2333 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN climate_solar_by_station.sunshine_duration; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN climate_solar_by_station.sunshine_duration IS 'in Minutes';


--
-- TOC entry 2334 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN climate_solar_by_station.diffuse_power; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN climate_solar_by_station.diffuse_power IS 'in Joule per square centimeter per hour';


--
-- TOC entry 2335 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN climate_solar_by_station.global_power; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN climate_solar_by_station.global_power IS 'in Joule per square centimeter per hour';


--
-- TOC entry 239 (class 1259 OID 27426514)
-- Name: Climate_SolarByStations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Climate_SolarByStations_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2336 (class 0 OID 0)
-- Dependencies: 239
-- Name: Climate_SolarByStations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Climate_SolarByStations_id_seq" OWNED BY climate_solar_by_station.id;


--
-- TOC entry 237 (class 1259 OID 27143486)
-- Name: climate_wind_by_station; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE climate_wind_by_station (
    time_id integer,
    station_id integer,
    wind_speed double precision,
    wind_direction integer,
    id integer NOT NULL
);


--
-- TOC entry 2337 (class 0 OID 0)
-- Dependencies: 237
-- Name: TABLE climate_wind_by_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE climate_wind_by_station IS 'Wind speed and directions measured by DWD measuring stations
Data retrieved from www.dwd.de

TimeID: ID of the timestamp (Translatable into human-readable timestamps through TimeStamps table)
StationID: ID of the station (Metadata of station translatable through Climate_Stations table)
WindSpeed: Measured wind speed in m/s
WindDirection: Direction of wind in degrees
id: primary key';


--
-- TOC entry 236 (class 1259 OID 27143484)
-- Name: Climate_WindByStations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Climate_WindByStations_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2338 (class 0 OID 0)
-- Dependencies: 236
-- Name: Climate_WindByStations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Climate_WindByStations_id_seq" OWNED BY climate_wind_by_station.id;


--
-- TOC entry 215 (class 1259 OID 19762137)
-- Name: eextrans_solar_expected_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eextrans_solar_expected_de (
    time_stamp timestamp with time zone,
    fz_hertz real,
    amprion real,
    tennet real,
    transnet_bw real,
    id integer NOT NULL
);


--
-- TOC entry 2339 (class 0 OID 0)
-- Dependencies: 215
-- Name: TABLE eextrans_solar_expected_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eextrans_solar_expected_de IS 'Expected electricity production of solar power in Germany over time in 15-minute resolution
Data retrieved from www.eex-transparency.com

TimeStamp: Timestamp in ISO 8601 format
FZHertz: Solar power production in the control area of 50Hetz in MW
Amprion: Solar power production in the control area of Amprion in MW
Tennet: Solar power production in the control area of Tennet in MW
TransnetBW: Solar power production in the control area of Transnet BW in MW
';


--
-- TOC entry 216 (class 1259 OID 19762143)
-- Name: EEXTrans_Solar_actual_DE_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EEXTrans_Solar_actual_DE_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2340 (class 0 OID 0)
-- Dependencies: 216
-- Name: EEXTrans_Solar_actual_DE_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EEXTrans_Solar_actual_DE_id_seq" OWNED BY eextrans_solar_expected_de.id;


--
-- TOC entry 217 (class 1259 OID 19762151)
-- Name: eextrans_solar_actual_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eextrans_solar_actual_de (
    time_stamp timestamp with time zone,
    fz_hertz real,
    amprion real,
    tennet real,
    transnet_bw real,
    id integer NOT NULL
);


--
-- TOC entry 2341 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE eextrans_solar_actual_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eextrans_solar_actual_de IS 'Actual electricity production of solar power in Germany over time in 15-minute resolution
Data retrieved from www.eex-transparency.com

TimeStamp: Timestamp in ISO 8601 format
FZHertz: Solar power production in the control area of 50Hetz in MW
Amprion: Solar power production in the control area of Amprion in MW
Tennet: Solar power production in the control area of Tennet in MW
TransnetBW: Solar power production in the control area of Transnet BW in MW
';


--
-- TOC entry 218 (class 1259 OID 19762157)
-- Name: EEXTrans_Solar_actual_DE_id_seq1; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EEXTrans_Solar_actual_DE_id_seq1"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2342 (class 0 OID 0)
-- Dependencies: 218
-- Name: EEXTrans_Solar_actual_DE_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EEXTrans_Solar_actual_DE_id_seq1" OWNED BY eextrans_solar_actual_de.id;


--
-- TOC entry 234 (class 1259 OID 27118502)
-- Name: eex_emission_allowances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eex_emission_allowances (
    time_stamp date,
    settlement_price double precision,
    low_price double precision,
    high_price double precision,
    opening_price double precision,
    closing_price double precision,
    volume_total double precision,
    id integer NOT NULL
);


--
-- TOC entry 2343 (class 0 OID 0)
-- Dependencies: 234
-- Name: TABLE eex_emission_allowances; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eex_emission_allowances IS 'Prices of Carbondioxide emission allowances (EUA)
Data retrieved from www.eex.com

TimeStamp: Timestamp in ISO 8601 format
SettlementPrice: Settlement price in EUR/t (EUR per metric ton)
LowPrice: Lowest price in EUR/t (EUR per metric ton)
HighPrice: Highest price in EUR/t (EUR per metric ton)
OpeningPrice: Opening price in EUR/t (EUR per metric ton)
ClosingPrice: Closing price in EUR/t (EUR per metric ton)
VolumeTotal: Totally traded volume in metric tons
id: primary key

';


--
-- TOC entry 235 (class 1259 OID 27118505)
-- Name: EEX_EmissionAllowances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EEX_EmissionAllowances_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2344 (class 0 OID 0)
-- Dependencies: 235
-- Name: EEX_EmissionAllowances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EEX_EmissionAllowances_id_seq" OWNED BY eex_emission_allowances.id;


--
-- TOC entry 211 (class 1259 OID 19709247)
-- Name: eextrans_wind_actual_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eextrans_wind_actual_de (
    time_stamp timestamp with time zone,
    onshore_fz_hertz real,
    onshore_amprion real,
    onshore_tennet real,
    onshore_transnet_bw real,
    id integer NOT NULL,
    offshore_fz_hertz real,
    offshore_amprion real,
    offshore_tennet real,
    offshore_transnet_bw real
);


--
-- TOC entry 2345 (class 0 OID 0)
-- Dependencies: 211
-- Name: TABLE eextrans_wind_actual_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eextrans_wind_actual_de IS 'Actual electricity production of wind power in Germany over time in 15-minute resolution
Data retrieved from www.eex-transparency.com

TimeStamp: Timestamp in ISO 8601 format
FZHertz: Wind power production in the control area of 50Hetz in MW
Amprion: Wind power production in the control area of Amprion in MW
Tennet: Wind power production in the control area of Tennet in MW
TransnetBW: Wind power production in the control area of Transnet BW in MW

After 2016-01-28, wind power data was separated into offshore and onshore.
';


--
-- TOC entry 213 (class 1259 OID 19762106)
-- Name: eextrans_wind_expected_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eextrans_wind_expected_de (
    time_stamp timestamp with time zone,
    onshore_fz_hertz real,
    onshore_amprion real,
    onshore_tennet real,
    onshore_transnet_bw real,
    id integer NOT NULL,
    offshore_fz_hertz real,
    offshore_amprion real,
    offshore_tennet real,
    offshore_transnet_bw real
);


--
-- TOC entry 2346 (class 0 OID 0)
-- Dependencies: 213
-- Name: TABLE eextrans_wind_expected_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eextrans_wind_expected_de IS 'Expected electricity production of wind power in Germany over time in 15-minute resolution
Data retrieved from www.eex-transparency.com

TimeStamp: Timestamp in ISO 8601 format
FZHertz: Solar power production in the control area of 50Hetz in MW
Amprion: Solar power production in the control area of Amprion in MW
Tennet: Solar power production in the control area of Tennet in MW
TransnetBW: Solar power production in the control area of Transnet BW in MW
';


--
-- TOC entry 244 (class 1259 OID 34154531)
-- Name: EEX_Trans_RE; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW "EEX_Trans_RE" AS
 SELECT date_trunc('hour'::text, eextrans_wind_expected_de.time_stamp) AS "TimeStamp",
    (((avg(eextrans_solar_expected_de.fz_hertz) + avg(eextrans_solar_expected_de.amprion)) + avg(eextrans_solar_expected_de.tennet)) + avg(eextrans_solar_expected_de.transnet_bw)) AS solar_expected,
    (avg(eextrans_wind_expected_de.offshore_fz_hertz) + avg(eextrans_wind_expected_de.offshore_tennet)) AS offshore_expected,
    (((avg(eextrans_wind_expected_de.onshore_fz_hertz) + avg(eextrans_wind_expected_de.onshore_amprion)) + avg(eextrans_wind_expected_de.onshore_tennet)) + avg(eextrans_wind_expected_de.onshore_transnet_bw)) AS onshore_expected,
    (((avg(eextrans_solar_actual_de.fz_hertz) + avg(eextrans_solar_actual_de.amprion)) + avg(eextrans_solar_actual_de.tennet)) + avg(eextrans_solar_actual_de.transnet_bw)) AS solar_actual,
    (avg(eextrans_wind_actual_de.offshore_fz_hertz) + avg(eextrans_wind_actual_de.offshore_tennet)) AS offshore_actual,
    (((avg(eextrans_wind_actual_de.onshore_fz_hertz) + avg(eextrans_wind_actual_de.onshore_amprion)) + avg(eextrans_wind_actual_de.onshore_tennet)) + avg(eextrans_wind_actual_de.onshore_transnet_bw)) AS onshore_actual
   FROM eextrans_wind_actual_de,
    eextrans_wind_expected_de,
    eextrans_solar_expected_de,
    eextrans_solar_actual_de
  WHERE (((eextrans_wind_expected_de.time_stamp = eextrans_wind_actual_de.time_stamp) AND (eextrans_wind_expected_de.time_stamp = eextrans_solar_expected_de.time_stamp)) AND (eextrans_wind_expected_de.time_stamp = eextrans_solar_actual_de.time_stamp))
  GROUP BY date_trunc('hour'::text, eextrans_wind_expected_de.time_stamp)
  ORDER BY date_trunc('hour'::text, eextrans_wind_expected_de.time_stamp);


--
-- TOC entry 241 (class 1259 OID 32876807)
-- Name: ENTSOE_Frequency; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "ENTSOE_Frequency" (
    "TimeStamp" timestamp with time zone NOT NULL,
    "Frequency" double precision NOT NULL
);


--
-- TOC entry 2347 (class 0 OID 0)
-- Dependencies: 241
-- Name: TABLE "ENTSOE_Frequency"; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE "ENTSOE_Frequency" IS 'Net Frequency of the ENTSO-E Region

http://www.50hertz.com/de/Maerkte/Regelenergie/Regelenergie-Downloadbereich

';


--
-- TOC entry 2348 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN "ENTSOE_Frequency"."Frequency"; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN "ENTSOE_Frequency"."Frequency" IS 'Frequency in Hz';


--
-- TOC entry 182 (class 1259 OID 19666255)
-- Name: epex_dayahead_basepeak_ch; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_basepeak_ch (
    id integer NOT NULL,
    time_stamp date,
    price_base double precision,
    volume_base double precision,
    price_peak double precision,
    volume_peak double precision
);


--
-- TOC entry 2349 (class 0 OID 0)
-- Dependencies: 182
-- Name: TABLE epex_dayahead_basepeak_ch; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_basepeak_ch IS 'EPEX Day ahead settlement prices and traded volumes for base and peak in Swizterland
Data retrieved from www.epexspot.com

TimeStamp: Date in ISO 8601 format
PriceBase: Settlement price for base period in EUR/MWh
VolumeBase: Traded volume in base period in MWh
PricePeak: Settlement price for peak period in EUR/MWh
VolumeBase: Traded volume in peak period in MWh
id: primary key
';


--
-- TOC entry 181 (class 1259 OID 19666253)
-- Name: EPEX_DayAheadBasePeak_CH_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_DayAheadBasePeak_CH_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2350 (class 0 OID 0)
-- Dependencies: 181
-- Name: EPEX_DayAheadBasePeak_CH_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_DayAheadBasePeak_CH_id_seq" OWNED BY epex_dayahead_basepeak_ch.id;


--
-- TOC entry 186 (class 1259 OID 19666289)
-- Name: epex_dayahead_basepeak_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_basepeak_de (
    id integer NOT NULL,
    time_stamp date,
    price_base double precision,
    volume_base double precision,
    price_peak double precision,
    volume_peak double precision
);


--
-- TOC entry 2351 (class 0 OID 0)
-- Dependencies: 186
-- Name: TABLE epex_dayahead_basepeak_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_basepeak_de IS 'EPEX Day ahead settlement prices and traded volumes for base and peak in Germany
Data retrieved from www.epexspot.com

TimeStamp: Date in ISO 8601 format
PriceBase: Settlement price for base period in EUR/MWh
VolumeBase: Traded volume in base period in MWh
PricePeak: Settlement price for peak period in EUR/MWh
VolumeBase: Traded volume in peak period in MWh
id: primary key
';


--
-- TOC entry 185 (class 1259 OID 19666287)
-- Name: EPEX_DayAheadBasePeak_DE_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_DayAheadBasePeak_DE_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 185
-- Name: EPEX_DayAheadBasePeak_DE_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_DayAheadBasePeak_DE_id_seq" OWNED BY epex_dayahead_basepeak_de.id;


--
-- TOC entry 174 (class 1259 OID 19666188)
-- Name: epex_dayahead_basepeak_fr; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_basepeak_fr (
    id integer NOT NULL,
    time_stamp date,
    price_base double precision,
    volume_base double precision,
    price_peak double precision,
    volume_peak double precision
);


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 174
-- Name: TABLE epex_dayahead_basepeak_fr; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_basepeak_fr IS 'EPEX Day ahead settlement prices and traded volumes for base and peak in France
Data retrieved from www.epexspot.com

TimeStamp: Date in ISO 8601 format
PriceBase: Settlement price for base period in EUR/MWh
VolumeBase: Traded volume in base period in MWh
PricePeak: Settlement price for peak period in EUR/MWh
VolumeBase: Traded volume in peak period in MWh
id: primary key
';


--
-- TOC entry 173 (class 1259 OID 19666186)
-- Name: EPEX_DayAheadBasePeak_FR_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_DayAheadBasePeak_FR_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 173
-- Name: EPEX_DayAheadBasePeak_FR_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_DayAheadBasePeak_FR_id_seq" OWNED BY epex_dayahead_basepeak_fr.id;


--
-- TOC entry 184 (class 1259 OID 19666266)
-- Name: epex_dayahead_blocks_ch; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_blocks_ch (
    id integer NOT NULL,
    time_stamp date,
    middle_night double precision,
    early_morning double precision,
    late_morning double precision,
    early_afternoon double precision,
    rush_hour double precision,
    off_peak2 double precision,
    night double precision,
    off_peak1 double precision,
    business double precision,
    off_peak double precision,
    morning double precision,
    high_noon double precision,
    afternoon double precision,
    evening double precision,
    sun_peak double precision
);


--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 184
-- Name: TABLE epex_dayahead_blocks_ch; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_blocks_ch IS 'EPEX Day ahead settlement prices for pre-defined blocks in Switzerland
Data retrieved from www.eex.com

TimeStamp: Date in ISO 8601 format
Offpeak1: Settlement price Monday through Friday 0:00 to 8:00 in EUR/MWh
Offpeak2: Settlement price Saturday through Sunday in EUR/MWh
id: primary key

Other columns are discontinued and meta data is not availible
';


--
-- TOC entry 183 (class 1259 OID 19666264)
-- Name: EPEX_DayAheadBlocks_CH_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_DayAheadBlocks_CH_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2356 (class 0 OID 0)
-- Dependencies: 183
-- Name: EPEX_DayAheadBlocks_CH_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_DayAheadBlocks_CH_id_seq" OWNED BY epex_dayahead_blocks_ch.id;


--
-- TOC entry 178 (class 1259 OID 19666233)
-- Name: epex_dayahead_blocks_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_blocks_de (
    id integer NOT NULL,
    time_stamp date,
    middle_night double precision,
    early_morning double precision,
    late_morning double precision,
    early_afternoon double precision,
    rush_hour double precision,
    off_peak2 double precision,
    night double precision,
    off_peak1 double precision,
    business double precision,
    off_peak double precision,
    morning double precision,
    high_noon double precision,
    afternoon double precision,
    evening double precision,
    sun_peak double precision
);


--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 178
-- Name: TABLE epex_dayahead_blocks_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_blocks_de IS 'EPEX Day ahead settlement prices for pre-defined blocks in Germany
Data retrieved from www.eex.com

TimeStamp: Date in ISO 8601 format
Offpeak1: Settlement price Monday through Friday 0:00 to 8:00 in EUR/MWh
Offpeak2: Settlement price Saturday through Sunday in EUR/MWh
id: primary key

Other columns are discontinued and meta data is not availible
';


--
-- TOC entry 177 (class 1259 OID 19666231)
-- Name: EPEX_DayAheadBlocks_DE_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_DayAheadBlocks_DE_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 177
-- Name: EPEX_DayAheadBlocks_DE_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_DayAheadBlocks_DE_id_seq" OWNED BY epex_dayahead_blocks_de.id;


--
-- TOC entry 176 (class 1259 OID 19666199)
-- Name: epex_dayahead_blocks_fr; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_blocks_fr (
    id integer NOT NULL,
    time_stamp date,
    middle_night double precision,
    early_morning double precision,
    late_morning double precision,
    early_afternoon double precision,
    rush_hour double precision,
    off_peak2 double precision,
    baseload double precision,
    peakload double precision,
    night double precision,
    off_peak1 double precision,
    business double precision,
    off_peak double precision,
    morning double precision,
    high_noon double precision,
    afternoon double precision,
    evening double precision,
    sun_peak double precision
);


--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 176
-- Name: TABLE epex_dayahead_blocks_fr; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_blocks_fr IS 'EPEX Day ahead settlement prices for pre-defined blocks in France
Data retrieved from www.eex.com

TimeStamp: Date in ISO 8601 format
Offpeak1: Settlement price Monday through Friday 0:00 to 8:00 in EUR/MWh
Offpeak2: Settlement price Saturday through Sunday in EUR/MWh
id: primary key

Other columns are discontinued and meta data is not availible
';


--
-- TOC entry 175 (class 1259 OID 19666197)
-- Name: EPEX_DayAheadBlocks_FR_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_DayAheadBlocks_FR_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2360 (class 0 OID 0)
-- Dependencies: 175
-- Name: EPEX_DayAheadBlocks_FR_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_DayAheadBlocks_FR_id_seq" OWNED BY epex_dayahead_blocks_fr.id;


--
-- TOC entry 180 (class 1259 OID 19666244)
-- Name: epex_dayahead_hour_ch; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_hour_ch (
    id integer NOT NULL,
    time_stamp timestamp with time zone,
    price double precision,
    volume double precision
);


--
-- TOC entry 2361 (class 0 OID 0)
-- Dependencies: 180
-- Name: TABLE epex_dayahead_hour_ch; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_hour_ch IS 'EPEX hourly day-ahead settlement prices and traded volumes Switzerland
Data retrieved from www.epexspot.com

TimeStamp: Date in ISO 8601 format
Price: Settlement price in EUR/MWh
Volume: Traded volume in MWh
id: primary key
';


--
-- TOC entry 179 (class 1259 OID 19666242)
-- Name: EPEX_DayAheadHour_CH_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_DayAheadHour_CH_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2362 (class 0 OID 0)
-- Dependencies: 179
-- Name: EPEX_DayAheadHour_CH_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_DayAheadHour_CH_id_seq" OWNED BY epex_dayahead_hour_ch.id;


--
-- TOC entry 172 (class 1259 OID 19666177)
-- Name: epex_dayahead_hour_fr; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_hour_fr (
    id integer NOT NULL,
    time_stamp timestamp with time zone,
    price double precision,
    volume double precision
);


--
-- TOC entry 2363 (class 0 OID 0)
-- Dependencies: 172
-- Name: TABLE epex_dayahead_hour_fr; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_hour_fr IS 'EPEX hourly day-ahead settlement prices and traded volumes in France
Data retrieved from www.epexspot.com

TimeStamp: Date in ISO 8601 format
Price: Settlement price in EUR/MWh
Volume: Traded volume in MWh
id: primary key
';


--
-- TOC entry 171 (class 1259 OID 19666175)
-- Name: EPEX_DayAheadHour_FR_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_DayAheadHour_FR_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2364 (class 0 OID 0)
-- Dependencies: 171
-- Name: EPEX_DayAheadHour_FR_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_DayAheadHour_FR_id_seq" OWNED BY epex_dayahead_hour_fr.id;


--
-- TOC entry 196 (class 1259 OID 19666495)
-- Name: epex_intraday_ch_hourly; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_intraday_ch_hourly (
    id integer NOT NULL,
    time_stamp timestamp with time zone,
    low double precision,
    high double precision,
    last double precision,
    weighted_avg double precision,
    index double precision,
    buy_volume double precision,
    sell_volume double precision
);


--
-- TOC entry 2365 (class 0 OID 0)
-- Dependencies: 196
-- Name: TABLE epex_intraday_ch_hourly; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_intraday_ch_hourly IS 'EPEX hourly intraday prices and traded volumes in Switzerland
Data retrieved from www.epexspot.com

TimeStamp: Timestamp
Low: Lowest exectued trade price in EUR/MWh
High: Highest executed trade price in EUR/MWh
Last: Last executed trade price in EUR/MWh
WeightAvg: Weighted average price of all executed trades in EUR/MWh
Index: Index Price (=WeightedAvg) in EUR/MWh
BuyVol: Totally demanded volume in MW
SelVol: Totally offered volume in MW
';


--
-- TOC entry 195 (class 1259 OID 19666493)
-- Name: EPEX_IntraDay_CH_Hourly_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_IntraDay_CH_Hourly_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2366 (class 0 OID 0)
-- Dependencies: 195
-- Name: EPEX_IntraDay_CH_Hourly_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_IntraDay_CH_Hourly_id_seq" OWNED BY epex_intraday_ch_hourly.id;


--
-- TOC entry 194 (class 1259 OID 19666484)
-- Name: epex_intraday_ch; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_intraday_ch (
    id integer NOT NULL,
    time_stamp timestamp with time zone,
    low double precision,
    high double precision,
    last double precision,
    weighted_avg double precision,
    index double precision,
    buy_volume double precision,
    sell_volume double precision
);


--
-- TOC entry 2367 (class 0 OID 0)
-- Dependencies: 194
-- Name: TABLE epex_intraday_ch; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_intraday_ch IS 'EPEX 15-min intraday prices and traded volumes in Switzerland
Data retrieved from www.epexspot.com

TimeStamp: Timestamp
Low: Lowest exectued trade price in EUR/MWh
High: Highest executed trade price in EUR/MWh
Last: Last executed trade price in EUR/MWh
WeightAvg: Weighted average price of all executed trades in EUR/MWh
Index: Index Price (=WeightedAvg) in EUR/MWh
BuyVol: Totally demanded volume in MW
SelVol: Totally offered volume in MW
';


--
-- TOC entry 193 (class 1259 OID 19666482)
-- Name: EPEX_IntraDay_CH_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_IntraDay_CH_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 193
-- Name: EPEX_IntraDay_CH_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_IntraDay_CH_id_seq" OWNED BY epex_intraday_ch.id;


--
-- TOC entry 190 (class 1259 OID 19666456)
-- Name: epex_intraday_de_hourly; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_intraday_de_hourly (
    id integer NOT NULL,
    time_stamp timestamp with time zone,
    low double precision,
    high double precision,
    last double precision,
    weighted_avg double precision,
    index double precision,
    buy_volume double precision,
    sell_volume double precision
);


--
-- TOC entry 2369 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE epex_intraday_de_hourly; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_intraday_de_hourly IS 'EPEX hourly intraday prices and traded volumes in Germany
Data retrieved from www.epexspot.com

TimeStamp: Timestamp
Low: Lowest exectued trade price in EUR/MWh
High: Highest executed trade price in EUR/MWh
Last: Last executed trade price in EUR/MWh
WeightAvg: Weighted average price of all executed trades in EUR/MWh
Index: Index Price (=WeightedAvg) in EUR/MWh
BuyVol: Totally demanded volume in MW
SelVol: Totally offered volume in MW
';


--
-- TOC entry 189 (class 1259 OID 19666454)
-- Name: EPEX_IntraDay_DE_Hourly_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_IntraDay_DE_Hourly_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 189
-- Name: EPEX_IntraDay_DE_Hourly_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_IntraDay_DE_Hourly_id_seq" OWNED BY epex_intraday_de_hourly.id;


--
-- TOC entry 188 (class 1259 OID 19666445)
-- Name: epex_intraday_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_intraday_de (
    id integer NOT NULL,
    time_stamp timestamp with time zone,
    low double precision,
    high double precision,
    last double precision,
    weighted_avg double precision,
    index double precision,
    buy_volume double precision,
    sell_volume double precision
);


--
-- TOC entry 2371 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE epex_intraday_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_intraday_de IS 'EPEX 15-min intraday prices and traded volumes in Germany
Data retrieved from www.epexspot.com

TimeStamp: Timestamp
Low: Lowest exectued trade price in EUR/MWh
High: Highest executed trade price in EUR/MWh
Last: Last executed trade price in EUR/MWh
WeightAvg: Weighted average price of all executed trades in EUR/MWh
Index: Index Price (=WeightedAvg) in EUR/MWh
BuyVol: Totally demanded volume in MW
SelVol: Totally offered volume in MW
';


--
-- TOC entry 187 (class 1259 OID 19666443)
-- Name: EPEX_IntraDay_DE_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_IntraDay_DE_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 187
-- Name: EPEX_IntraDay_DE_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_IntraDay_DE_id_seq" OWNED BY epex_intraday_de.id;


--
-- TOC entry 198 (class 1259 OID 19666517)
-- Name: epex_intraday_fr_hourly; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_intraday_fr_hourly (
    id integer NOT NULL,
    time_stamp timestamp with time zone,
    low double precision,
    high double precision,
    last double precision,
    weighted_avg double precision,
    index double precision,
    buy_volume double precision,
    sell_volume double precision
);


--
-- TOC entry 2373 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE epex_intraday_fr_hourly; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_intraday_fr_hourly IS 'EPEX hourly intraday prices and traded volumes in France

Data retrieved from www.epexspot.com

TimeStamp: Timestamp
Low: Lowest exectued trade price in EUR/MWh
High: Highest executed trade price in EUR/MWh
Last: Last executed trade price in EUR/MWh
WeightAvg: Weighted average price of all executed trades in EUR/MWh
Index: Index Price (=WeightedAvg) in EUR/MWh
BuyVol: Totally demanded volume in MW
SelVol: Totally offered volume in MW
';


--
-- TOC entry 197 (class 1259 OID 19666515)
-- Name: EPEX_IntraDay_FR_Hourly_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "EPEX_IntraDay_FR_Hourly_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 197
-- Name: EPEX_IntraDay_FR_Hourly_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "EPEX_IntraDay_FR_Hourly_id_seq" OWNED BY epex_intraday_fr_hourly.id;


--
-- TOC entry 224 (class 1259 OID 27108662)
-- Name: mrl_netzregelverbund; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE mrl_netzregelverbund (
    time_stamp timestamp with time zone,
    betr_neg double precision,
    betr_pos double precision,
    qual_neg double precision,
    qual_pos double precision,
    id integer NOT NULL
);


--
-- TOC entry 2375 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE mrl_netzregelverbund; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE mrl_netzregelverbund IS 'Required TCR power in Germany in 15-minute temporal resolution
Data obtained from www.regelleistung.net

Timestamp: Timestamp of the sample
betrNeg: Negative demand in MW
betrPos: Positive demand in MW
qualNeg: ?
qualPos: ?
id: Primary key
';


--
-- TOC entry 223 (class 1259 OID 27108660)
-- Name: MRL_Netzregelverbund_Neu_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "MRL_Netzregelverbund_Neu_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 223
-- Name: MRL_Netzregelverbund_Neu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "MRL_Netzregelverbund_Neu_id_seq" OWNED BY mrl_netzregelverbund.id;


--
-- TOC entry 233 (class 1259 OID 27116818)
-- Name: PLZ; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "PLZ" (
    "ClosestClimateStationID" integer,
    inhabitants integer,
    area double precision,
    postleitzahl integer NOT NULL
);


--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 233
-- Name: TABLE "PLZ"; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE "PLZ" IS 'Table to map Postleitzahlen (PLZ) (zip-codes) to climate stations of DWD

PLZ: plz (zip) 
ClimateStationID: ID of closest DWD climate statiom
Inhabitants: Number of inhabitants of the PLZ (according to census 2011)
Area: Area of polygon 
';


--
-- TOC entry 199 (class 1259 OID 19666561)
-- Name: tender_prl; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tender_prl (
    time_stamp date,
    time_stamp_end date,
    capacity_price real,
    offered_power real,
    award boolean,
    id integer NOT NULL,
    accepted_power real
);


--
-- TOC entry 2378 (class 0 OID 0)
-- Dependencies: 199
-- Name: TABLE tender_prl; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE tender_prl IS 'Results of tenders for SCR in Germany
Data obtained from www.regelleistung.net

Startdate: Timestamp of the starting time of the tendered periode
Enddate: Timestamp of the ending time of the tendered periode
CapacityPrice: Capacity price in EUR/MW
OfferedPower: Power offered in that bid in MW
AcceptedPower: Power accepted by the TSO in MW (replaces Award from 2015-06-08 on)
Award: Indicates whether bid got accepted or not as boolean value (TRUE meaning that bid got accepted)
id: primary key

';


--
-- TOC entry 202 (class 1259 OID 19666766)
-- Name: PRL_Tenders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "PRL_Tenders_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2379 (class 0 OID 0)
-- Dependencies: 202
-- Name: PRL_Tenders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "PRL_Tenders_id_seq" OWNED BY tender_prl.id;


--
-- TOC entry 201 (class 1259 OID 19666685)
-- Name: eextrans_blockwise_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eextrans_blockwise_de (
    plant_id integer,
    power double precision,
    time_id integer,
    pkey integer NOT NULL
);


--
-- TOC entry 2380 (class 0 OID 0)
-- Dependencies: 201
-- Name: TABLE eextrans_blockwise_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eextrans_blockwise_de IS 'Electricity production of German power plants with a power output of 100 MW or more with to-the-block-precision and hourly resolution
Data retrieved from www.eex-transparency.com

PlantID: ID of the power plant (metadata of plant translatable through PowerPlants table)
Power: Power output in MW
timeID: ID of the timestamp (translatable to human-readable timestamps through TimeStamps table)
pkey: Primary key';


--
-- TOC entry 206 (class 1259 OID 19666928)
-- Name: PowerGeneration_pkey_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "PowerGeneration_pkey_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2381 (class 0 OID 0)
-- Dependencies: 206
-- Name: PowerGeneration_pkey_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "PowerGeneration_pkey_seq" OWNED BY eextrans_blockwise_de.pkey;


--
-- TOC entry 220 (class 1259 OID 19797656)
-- Name: power_plants; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE power_plants (
    company text,
    unit text,
    plant text,
    fuel text,
    plant_id integer NOT NULL,
    full_name text,
    power_electric double precision,
    renovation_year integer,
    construction_year integer,
    plz text,
    power_thermal double precision,
    country character(2),
    urban boolean,
    disposal_plant boolean,
    bnetz_kraftwerksnummer text,
    industrial boolean
);


--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE power_plants; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE power_plants IS 'Table of German powerplants with a power output of 100 MW or more
Data obtained from www.eex-transparency.com and from UBA.

Company: Company operating and/or operating the powerplant
Unit: Name of the unit
Plant: Name of the plant
Fuel: Employed fuel of the powerplant
PlantID: ID of the powerplant (internal DB key)
FullName: Unique identifier string - merge of column "Unit" and "Plant" 
PowerElectric: Electric nominal power of the powerplant in MW
RenovationYear: Year of most recent renovation
ContructionYear: Year of construction
ZipCode: Zipcode (PLZ) of the power plant
PowerThermal: Nominal thermal power of the powerplant in MW';


--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN power_plants.full_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN power_plants.full_name IS 'Made by the concatenation of Unit + Plant';


--
-- TOC entry 2384 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN power_plants.power_electric; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN power_plants.power_electric IS 'Die elektrische Bruttoleistung von dem Kraftwerkteil in MW';


--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN power_plants.power_thermal; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN power_plants.power_thermal IS 'Die CHP Leistung von dem Kraftwerkteil in MW';


--
-- TOC entry 225 (class 1259 OID 27112245)
-- Name: PowerPlants_PlantID_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "PowerPlants_PlantID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 225
-- Name: PowerPlants_PlantID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "PowerPlants_PlantID_seq" OWNED BY power_plants.plant_id;


--
-- TOC entry 200 (class 1259 OID 19666589)
-- Name: RZ_Saldo_Netzelgerverbund; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "RZ_Saldo_Netzelgerverbund" (
    "Startdate" timestamp without time zone,
    "Enddate" timestamp without time zone,
    "Value" real,
    id integer NOT NULL
);


--
-- TOC entry 203 (class 1259 OID 19666793)
-- Name: RZ_Saldo_Netzelgerverbund_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "RZ_Saldo_Netzelgerverbund_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 203
-- Name: RZ_Saldo_Netzelgerverbund_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "RZ_Saldo_Netzelgerverbund_id_seq" OWNED BY "RZ_Saldo_Netzelgerverbund".id;


--
-- TOC entry 192 (class 1259 OID 19666473)
-- Name: SRL_ActivationHighRes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "SRL_ActivationHighRes" (
    "Date" date,
    "Time" time without time zone,
    "Power" numeric,
    "ID" integer NOT NULL
);


--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE "SRL_ActivationHighRes"; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE "SRL_ActivationHighRes" IS 'Required SCR power of the German TSO 50Hertz in 5 second resolution
Data obtained from www.regelleistung.net

StartDate: Starttime of the sample
EndDate: Endtime of the sample
Power: Signed SCR demand in MW (negatively signed values meaning negative control reserve)
ID: Primary key
';


--
-- TOC entry 191 (class 1259 OID 19666471)
-- Name: SRL_Demand2_ID_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "SRL_Demand2_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 191
-- Name: SRL_Demand2_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "SRL_Demand2_ID_seq" OWNED BY "SRL_ActivationHighRes"."ID";


--
-- TOC entry 221 (class 1259 OID 23357824)
-- Name: srl_netzregelverbund; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE srl_netzregelverbund (
    time_stamp timestamp with time zone,
    betr_neg double precision,
    betr_pos double precision,
    qual_neg double precision,
    qual_pos double precision,
    id integer NOT NULL
);


--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE srl_netzregelverbund; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE srl_netzregelverbund IS 'Required SCR power in Germany in 15-minute temporal resolution
Data obtained from www.regelleistung.net

Timestamp: Timestamp of the sample
betrNeg: Negative demand in MW
betrPos: Positive demand in MW
qualNeg: ?
qualPos: ?
id: Primary key
';


--
-- TOC entry 222 (class 1259 OID 23357827)
-- Name: SRL_Netzregelverbung_Neu_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "SRL_Netzregelverbung_Neu_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 222
-- Name: SRL_Netzregelverbung_Neu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "SRL_Netzregelverbung_Neu_id_seq" OWNED BY srl_netzregelverbund.id;


--
-- TOC entry 242 (class 1259 OID 32947175)
-- Name: Stammdaten_EEG; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Stammdaten_EEG" (
    commissioning date,
    decommissioning date,
    postleitzahl integer,
    power double precision,
    source text,
    anlagenschluessel text NOT NULL
);


--
-- TOC entry 243 (class 1259 OID 32959898)
-- Name: Stammdaten_EEG_OldFormat; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Stammdaten_EEG_OldFormat" (
    commissioning date,
    decommissioning date,
    postleitzahl integer,
    power double precision,
    source text,
    repowering boolean,
    anlagenschluessel text NOT NULL
);


--
-- TOC entry 227 (class 1259 OID 27116756)
-- Name: tender_mrl; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tender_mrl (
    time_stamp timestamp with time zone,
    time_stamp_end timestamp with time zone,
    reserve_type character(3),
    capacity_price double precision,
    energy_price double precision,
    offered_power integer,
    award boolean,
    id integer NOT NULL
);


--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE tender_mrl; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE tender_mrl IS 'Results of tenders for TCR in Germany
Data obtained from www.regelleistung.net

TimeStamp: Timestamp in ISO 8601 format of the starting time of the tendered periode
TimeStampEnd: Timestamp in ISO 8601 format of the ending time of the tendered periode
ReserveType: Type of reserve as in negative (NEG) or positive (POS) as a 3 character string
CapacityPrice: Capacity price in EUR/MW
EnergyPrice: Energy price in EUR/MWh
OfferedPower: Power offered in that bid in MW
Award: Indicates whether bid got accepted or not as boolean value (TRUE meaning that bid got accepted)
id: primary key
';


--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN tender_mrl.energy_price; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN tender_mrl.energy_price IS 'Negative values mean supplier pays to TSO
Positive values mean TSO pays to supplier';


--
-- TOC entry 226 (class 1259 OID 27116754)
-- Name: Tender_MRL_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Tender_MRL_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2394 (class 0 OID 0)
-- Dependencies: 226
-- Name: Tender_MRL_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Tender_MRL_id_seq" OWNED BY tender_mrl.id;


--
-- TOC entry 229 (class 1259 OID 27116786)
-- Name: tender_srl; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tender_srl (
    time_stamp timestamp without time zone,
    time_stamp_end timestamp without time zone,
    reserve_type character(3),
    time_period character(2),
    capacity_price double precision,
    energy_price double precision,
    offered_power double precision,
    award boolean,
    id integer NOT NULL
);


--
-- TOC entry 2395 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE tender_srl; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE tender_srl IS 'Results of tenders for SCR in Germany Data obtained from www.regelleistung.net \n TimeStamp: Timestamp in ISO 8601 format of the starting time of the tendered periode
TimeStampEnd: Timestamp in ISO 8601 format of the ending time of the tendered periode
ReserveType: Type of reserve as in negative (NEG) or positive (POS) as a 3 character string
TimePeriod: Time period as in peak (HT) and off-peak (NT) as a 2 character string. Peak period is
Monday through Friday 8:00 to 20:00. Off-Peak is Monday through Friday 20:00 to 8:00, and Saturday and Sunday.
CapacityPrice: Capacity price in EUR/MW
EnergyPrice: Energy price in EUR/MWh
OfferedPower: Power offered in that bid in MW
Award: Indicates whether bid got accepted or not as boolean value (TRUE meaning that bid got accepted)
id: primary key
';


--
-- TOC entry 228 (class 1259 OID 27116784)
-- Name: Tender_SRL_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Tender_SRL_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2396 (class 0 OID 0)
-- Dependencies: 228
-- Name: Tender_SRL_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Tender_SRL_id_seq" OWNED BY tender_srl.id;


--
-- TOC entry 205 (class 1259 OID 19666897)
-- Name: time_stamps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE time_stamps (
    time_stamp timestamp with time zone,
    time_id integer NOT NULL
);


--
-- TOC entry 2397 (class 0 OID 0)
-- Dependencies: 205
-- Name: TABLE time_stamps; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE time_stamps IS 'Table to translate timeIDs into human-readable TimeStamps';


--
-- TOC entry 204 (class 1259 OID 19666895)
-- Name: TimeStamps_timeID_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "TimeStamps_timeID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2398 (class 0 OID 0)
-- Dependencies: 204
-- Name: TimeStamps_timeID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "TimeStamps_timeID_seq" OWNED BY time_stamps.time_id;


--
-- TOC entry 212 (class 1259 OID 19762083)
-- Name: Wind_actual_DE_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Wind_actual_DE_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2399 (class 0 OID 0)
-- Dependencies: 212
-- Name: Wind_actual_DE_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Wind_actual_DE_id_seq" OWNED BY eextrans_wind_actual_de.id;


--
-- TOC entry 214 (class 1259 OID 19762109)
-- Name: Wind_expected_DE_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "Wind_expected_DE_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2400 (class 0 OID 0)
-- Dependencies: 214
-- Name: Wind_expected_DE_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "Wind_expected_DE_id_seq" OWNED BY eextrans_wind_expected_de.id;


--
-- TOC entry 230 (class 1259 OID 27116798)
-- Name: climate_stations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE climate_stations (
    station_id integer NOT NULL,
    altitude integer,
    latitude double precision,
    longitude double precision,
    station_name character varying,
    bundesland character varying,
    plz integer
);


--
-- TOC entry 2401 (class 0 OID 0)
-- Dependencies: 230
-- Name: TABLE climate_stations; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE climate_stations IS 'Table with DWD climate stations
Data retrieved from ww.dwd.de

SationID: Sation identifier number
Altitude: Altitude above sea level in m
Latitude: Geographic latitude (WGS 84)
Longitude: Geographic longitude (WGS 84)
StationName: Name of the station
Bundesland: Bundesland the station is situated in
PLZ: PLZ of the station

';


--
-- TOC entry 219 (class 1259 OID 19762167)
-- Name: eexfuture_set_prices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE eexfuture_set_prices (
    next_week_base double precision,
    next_week_peak double precision,
    next_we_base double precision,
    next_we_peak double precision,
    time_stamp timestamp without time zone NOT NULL
);


--
-- TOC entry 2402 (class 0 OID 0)
-- Dependencies: 219
-- Name: TABLE eexfuture_set_prices; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eexfuture_set_prices IS 'EPEX future prices
Data retrieved from www.epex.com

';


--
-- TOC entry 245 (class 1259 OID 37145447)
-- Name: epex_dayahead_hour_de; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE epex_dayahead_hour_de (
    time_stamp timestamp with time zone,
    price double precision,
    volume double precision,
    id integer NOT NULL
);


--
-- TOC entry 2403 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE epex_dayahead_hour_de; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE epex_dayahead_hour_de IS 'EPEX hourly day-ahead settlement prices and traded volumes Germany
Data retrieved from www.epexspot.com

TimeStamp: Date in ISO 8601 format
Price: Settlement price in EUR/MWh
Volume: Traded volume in MWh
id: primary key
';


--
-- TOC entry 246 (class 1259 OID 37145756)
-- Name: epex_dayahead_hour_de_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE epex_dayahead_hour_de_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2404 (class 0 OID 0)
-- Dependencies: 246
-- Name: epex_dayahead_hour_de_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE epex_dayahead_hour_de_id_seq OWNED BY epex_dayahead_hour_de.id;


--
-- TOC entry 247 (class 1259 OID 37145823)
-- Name: plant_distinciton; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW plant_distinciton AS
 SELECT power_plants.plant_id,
    regexp_replace(power_plants.plant, 'KW |Kraftwerk |GuD |HKW |Heizkraftwerk |GKW |KKW |Kernkraftwerk '::text, ''::text) AS cut_name,
    power_plants.company,
    power_plants.unit,
    power_plants.plant,
    power_plants.fuel,
    power_plants.power_electric,
    power_plants.bnetz_kraftwerksnummer
   FROM power_plants
  ORDER BY regexp_replace(power_plants.plant, 'KW |Kraftwerk |GuD |HKW |Heizkraftwerk |GKW |KKW |Kernkraftwerk '::text, ''::text);


--
-- TOC entry 240 (class 1259 OID 32617453)
-- Name: tendered_rl; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tendered_rl (
    time_stamp date NOT NULL,
    negpos_prl double precision,
    neg_srl double precision,
    pos_srl double precision,
    neg_mrl double precision,
    pos_mrl double precision
);


--
-- TOC entry 2087 (class 2604 OID 19666795)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "RZ_Saldo_Netzelgerverbund" ALTER COLUMN id SET DEFAULT nextval('"RZ_Saldo_Netzelgerverbund_id_seq"'::regclass);


--
-- TOC entry 2082 (class 2604 OID 19666476)
-- Name: ID; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "SRL_ActivationHighRes" ALTER COLUMN "ID" SET DEFAULT nextval('"SRL_Demand2_ID_seq"'::regclass);


--
-- TOC entry 2104 (class 2604 OID 27426516)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY climate_solar_by_station ALTER COLUMN id SET DEFAULT nextval('"Climate_SolarByStations_id_seq"'::regclass);


--
-- TOC entry 2101 (class 2604 OID 27116811)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY climate_temperature_by_station ALTER COLUMN id SET DEFAULT nextval('"ClimateTemperatureStations_id_seq"'::regclass);


--
-- TOC entry 2103 (class 2604 OID 27143489)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY climate_wind_by_station ALTER COLUMN id SET DEFAULT nextval('"Climate_WindByStations_id_seq"'::regclass);


--
-- TOC entry 2102 (class 2604 OID 27118507)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eex_emission_allowances ALTER COLUMN id SET DEFAULT nextval('"EEX_EmissionAllowances_id_seq"'::regclass);


--
-- TOC entry 2090 (class 2604 OID 19709224)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_100mw_actual_de ALTER COLUMN id SET DEFAULT nextval('"100MW_actual_DE__ID_seq"'::regclass);


--
-- TOC entry 2091 (class 2604 OID 19709241)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_100mw_expected_de ALTER COLUMN id SET DEFAULT nextval('"100MW_expected_DE_ID_seq"'::regclass);


--
-- TOC entry 2088 (class 2604 OID 19666930)
-- Name: pkey; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_blockwise_de ALTER COLUMN pkey SET DEFAULT nextval('"PowerGeneration_pkey_seq"'::regclass);


--
-- TOC entry 2095 (class 2604 OID 19762159)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_solar_actual_de ALTER COLUMN id SET DEFAULT nextval('"EEXTrans_Solar_actual_DE_id_seq1"'::regclass);


--
-- TOC entry 2094 (class 2604 OID 19762145)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_solar_expected_de ALTER COLUMN id SET DEFAULT nextval('"EEXTrans_Solar_actual_DE_id_seq"'::regclass);


--
-- TOC entry 2092 (class 2604 OID 19762085)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_wind_actual_de ALTER COLUMN id SET DEFAULT nextval('"Wind_actual_DE_id_seq"'::regclass);


--
-- TOC entry 2093 (class 2604 OID 19762111)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_wind_expected_de ALTER COLUMN id SET DEFAULT nextval('"Wind_expected_DE_id_seq"'::regclass);


--
-- TOC entry 2077 (class 2604 OID 19666258)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_basepeak_ch ALTER COLUMN id SET DEFAULT nextval('"EPEX_DayAheadBasePeak_CH_id_seq"'::regclass);


--
-- TOC entry 2079 (class 2604 OID 19666292)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_basepeak_de ALTER COLUMN id SET DEFAULT nextval('"EPEX_DayAheadBasePeak_DE_id_seq"'::regclass);


--
-- TOC entry 2073 (class 2604 OID 19666191)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_basepeak_fr ALTER COLUMN id SET DEFAULT nextval('"EPEX_DayAheadBasePeak_FR_id_seq"'::regclass);


--
-- TOC entry 2078 (class 2604 OID 19666269)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_blocks_ch ALTER COLUMN id SET DEFAULT nextval('"EPEX_DayAheadBlocks_CH_id_seq"'::regclass);


--
-- TOC entry 2075 (class 2604 OID 19666236)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_blocks_de ALTER COLUMN id SET DEFAULT nextval('"EPEX_DayAheadBlocks_DE_id_seq"'::regclass);


--
-- TOC entry 2074 (class 2604 OID 19666202)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_blocks_fr ALTER COLUMN id SET DEFAULT nextval('"EPEX_DayAheadBlocks_FR_id_seq"'::regclass);


--
-- TOC entry 2076 (class 2604 OID 19666247)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_hour_ch ALTER COLUMN id SET DEFAULT nextval('"EPEX_DayAheadHour_CH_id_seq"'::regclass);


--
-- TOC entry 2105 (class 2604 OID 37145758)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_hour_de ALTER COLUMN id SET DEFAULT nextval('epex_dayahead_hour_de_id_seq'::regclass);


--
-- TOC entry 2072 (class 2604 OID 19666180)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_hour_fr ALTER COLUMN id SET DEFAULT nextval('"EPEX_DayAheadHour_FR_id_seq"'::regclass);


--
-- TOC entry 2083 (class 2604 OID 19666487)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_ch ALTER COLUMN id SET DEFAULT nextval('"EPEX_IntraDay_CH_id_seq"'::regclass);


--
-- TOC entry 2084 (class 2604 OID 19666498)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_ch_hourly ALTER COLUMN id SET DEFAULT nextval('"EPEX_IntraDay_CH_Hourly_id_seq"'::regclass);


--
-- TOC entry 2080 (class 2604 OID 19666448)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_de ALTER COLUMN id SET DEFAULT nextval('"EPEX_IntraDay_DE_id_seq"'::regclass);


--
-- TOC entry 2081 (class 2604 OID 19666459)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_de_hourly ALTER COLUMN id SET DEFAULT nextval('"EPEX_IntraDay_DE_Hourly_id_seq"'::regclass);


--
-- TOC entry 2085 (class 2604 OID 19666520)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_fr_hourly ALTER COLUMN id SET DEFAULT nextval('"EPEX_IntraDay_FR_Hourly_id_seq"'::regclass);


--
-- TOC entry 2098 (class 2604 OID 27108665)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mrl_netzregelverbund ALTER COLUMN id SET DEFAULT nextval('"MRL_Netzregelverbund_Neu_id_seq"'::regclass);


--
-- TOC entry 2096 (class 2604 OID 27112247)
-- Name: plant_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY power_plants ALTER COLUMN plant_id SET DEFAULT nextval('"PowerPlants_PlantID_seq"'::regclass);


--
-- TOC entry 2097 (class 2604 OID 23357829)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY srl_netzregelverbund ALTER COLUMN id SET DEFAULT nextval('"SRL_Netzregelverbung_Neu_id_seq"'::regclass);


--
-- TOC entry 2099 (class 2604 OID 27116759)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tender_mrl ALTER COLUMN id SET DEFAULT nextval('"Tender_MRL_id_seq"'::regclass);


--
-- TOC entry 2086 (class 2604 OID 19666768)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tender_prl ALTER COLUMN id SET DEFAULT nextval('"PRL_Tenders_id_seq"'::regclass);


--
-- TOC entry 2100 (class 2604 OID 27116789)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tender_srl ALTER COLUMN id SET DEFAULT nextval('"Tender_SRL_id_seq"'::regclass);


--
-- TOC entry 2089 (class 2604 OID 19666900)
-- Name: time_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY time_stamps ALTER COLUMN time_id SET DEFAULT nextval('"TimeStamps_timeID_seq"'::regclass);


--
-- TOC entry 2149 (class 2606 OID 19709229)
-- Name: 100MW_actual_DE__pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_100mw_actual_de
    ADD CONSTRAINT "100MW_actual_DE__pkey" PRIMARY KEY (id);


--
-- TOC entry 2151 (class 2606 OID 19709246)
-- Name: 100MW_expected_DE_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_100mw_expected_de
    ADD CONSTRAINT "100MW_expected_DE_pkey" PRIMARY KEY (id);


--
-- TOC entry 2181 (class 2606 OID 27116805)
-- Name: ClimateStations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY climate_stations
    ADD CONSTRAINT "ClimateStations_pkey" PRIMARY KEY (station_id);


--
-- TOC entry 2183 (class 2606 OID 27116813)
-- Name: ClimateTemperatureStations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY climate_temperature_by_station
    ADD CONSTRAINT "ClimateTemperatureStations_pkey" PRIMARY KEY (id);


--
-- TOC entry 2195 (class 2606 OID 27426521)
-- Name: Climate_SolarByStations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY climate_solar_by_station
    ADD CONSTRAINT "Climate_SolarByStations_pkey" PRIMARY KEY (id);


--
-- TOC entry 2191 (class 2606 OID 27143491)
-- Name: Climate_WindByStations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY climate_wind_by_station
    ADD CONSTRAINT "Climate_WindByStations_pkey" PRIMARY KEY (id);


--
-- TOC entry 2165 (class 2606 OID 19762173)
-- Name: EEXFutureSetPrices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eexfuture_set_prices
    ADD CONSTRAINT "EEXFutureSetPrices_pkey" PRIMARY KEY (time_stamp);


--
-- TOC entry 2159 (class 2606 OID 19762150)
-- Name: EEXTrans_Solar_actual_DE_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_solar_expected_de
    ADD CONSTRAINT "EEXTrans_Solar_actual_DE_pkey" PRIMARY KEY (id);


--
-- TOC entry 2162 (class 2606 OID 19762164)
-- Name: EEXTrans_Solar_actual_DE_pkey1; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_solar_actual_de
    ADD CONSTRAINT "EEXTrans_Solar_actual_DE_pkey1" PRIMARY KEY (id);


--
-- TOC entry 2189 (class 2606 OID 27118512)
-- Name: EEX_EmissionAllowances_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eex_emission_allowances
    ADD CONSTRAINT "EEX_EmissionAllowances_pkey" PRIMARY KEY (id);


--
-- TOC entry 2201 (class 2606 OID 32877119)
-- Name: ENTSOE_Frequency_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ENTSOE_Frequency"
    ADD CONSTRAINT "ENTSOE_Frequency_pkey" PRIMARY KEY ("TimeStamp");


--
-- TOC entry 2117 (class 2606 OID 19666263)
-- Name: EPEX_DayAheadBasePeak_CH_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_basepeak_ch
    ADD CONSTRAINT "EPEX_DayAheadBasePeak_CH_pkey" PRIMARY KEY (id);


--
-- TOC entry 2121 (class 2606 OID 19666297)
-- Name: EPEX_DayAheadBasePeak_DE_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_basepeak_de
    ADD CONSTRAINT "EPEX_DayAheadBasePeak_DE_pkey" PRIMARY KEY (id);


--
-- TOC entry 2109 (class 2606 OID 19666196)
-- Name: EPEX_DayAheadBasePeak_FR_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_basepeak_fr
    ADD CONSTRAINT "EPEX_DayAheadBasePeak_FR_pkey" PRIMARY KEY (id);


--
-- TOC entry 2119 (class 2606 OID 19666274)
-- Name: EPEX_DayAheadBlocks_CH_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_blocks_ch
    ADD CONSTRAINT "EPEX_DayAheadBlocks_CH_pkey" PRIMARY KEY (id);


--
-- TOC entry 2113 (class 2606 OID 19666241)
-- Name: EPEX_DayAheadBlocks_DE_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_blocks_de
    ADD CONSTRAINT "EPEX_DayAheadBlocks_DE_pkey" PRIMARY KEY (id);


--
-- TOC entry 2111 (class 2606 OID 19666207)
-- Name: EPEX_DayAheadBlocks_FR_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_blocks_fr
    ADD CONSTRAINT "EPEX_DayAheadBlocks_FR_pkey" PRIMARY KEY (id);


--
-- TOC entry 2115 (class 2606 OID 19666252)
-- Name: EPEX_DayAheadHour_CH_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_hour_ch
    ADD CONSTRAINT "EPEX_DayAheadHour_CH_pkey" PRIMARY KEY (id);


--
-- TOC entry 2107 (class 2606 OID 19666185)
-- Name: EPEX_DayAheadHour_FR_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_hour_fr
    ADD CONSTRAINT "EPEX_DayAheadHour_FR_pkey" PRIMARY KEY (id);


--
-- TOC entry 2133 (class 2606 OID 19666503)
-- Name: EPEX_IntraDay_CH_Hourly_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_ch_hourly
    ADD CONSTRAINT "EPEX_IntraDay_CH_Hourly_pkey" PRIMARY KEY (id);


--
-- TOC entry 2131 (class 2606 OID 19666492)
-- Name: EPEX_IntraDay_CH_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_ch
    ADD CONSTRAINT "EPEX_IntraDay_CH_pkey" PRIMARY KEY (id);


--
-- TOC entry 2126 (class 2606 OID 19666464)
-- Name: EPEX_IntraDay_DE_Hourly_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_de_hourly
    ADD CONSTRAINT "EPEX_IntraDay_DE_Hourly_pkey" PRIMARY KEY (id);


--
-- TOC entry 2123 (class 2606 OID 19666453)
-- Name: EPEX_IntraDay_DE_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_de
    ADD CONSTRAINT "EPEX_IntraDay_DE_pkey" PRIMARY KEY (id);


--
-- TOC entry 2135 (class 2606 OID 19666525)
-- Name: EPEX_IntraDay_FR_Hourly_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_intraday_fr_hourly
    ADD CONSTRAINT "EPEX_IntraDay_FR_Hourly_pkey" PRIMARY KEY (id);


--
-- TOC entry 2172 (class 2606 OID 27108667)
-- Name: MRL_Netzregelverbund_Neu_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mrl_netzregelverbund
    ADD CONSTRAINT "MRL_Netzregelverbund_Neu_pkey" PRIMARY KEY (id);


--
-- TOC entry 2187 (class 2606 OID 32958072)
-- Name: PLZ_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "PLZ"
    ADD CONSTRAINT "PLZ_pkey" PRIMARY KEY (postleitzahl);


--
-- TOC entry 2137 (class 2606 OID 19666770)
-- Name: PRL_Tenders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tender_prl
    ADD CONSTRAINT "PRL_Tenders_pkey" PRIMARY KEY (id);


--
-- TOC entry 2141 (class 2606 OID 19666935)
-- Name: PowerGeneration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_blockwise_de
    ADD CONSTRAINT "PowerGeneration_pkey" PRIMARY KEY (pkey);


--
-- TOC entry 2139 (class 2606 OID 19666797)
-- Name: RZ_Saldo_Netzelgerverbund_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "RZ_Saldo_Netzelgerverbund"
    ADD CONSTRAINT "RZ_Saldo_Netzelgerverbund_pkey" PRIMARY KEY (id);


--
-- TOC entry 2129 (class 2606 OID 19666481)
-- Name: SRL_Demand2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "SRL_ActivationHighRes"
    ADD CONSTRAINT "SRL_Demand2_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 2169 (class 2606 OID 23357834)
-- Name: SRL_Netzregelverbung_Neu_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY srl_netzregelverbund
    ADD CONSTRAINT "SRL_Netzregelverbung_Neu_pkey" PRIMARY KEY (id);


--
-- TOC entry 2206 (class 2606 OID 32959905)
-- Name: Stammdaten_EEG_OldFormat_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Stammdaten_EEG_OldFormat"
    ADD CONSTRAINT "Stammdaten_EEG_OldFormat_pkey" PRIMARY KEY (anlagenschluessel);


--
-- TOC entry 2204 (class 2606 OID 32958097)
-- Name: Stammdaten_EEG_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Stammdaten_EEG"
    ADD CONSTRAINT "Stammdaten_EEG_pkey" PRIMARY KEY (anlagenschluessel);


--
-- TOC entry 2175 (class 2606 OID 27116761)
-- Name: Tender_MRL_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tender_mrl
    ADD CONSTRAINT "Tender_MRL_pkey" PRIMARY KEY (id);


--
-- TOC entry 2178 (class 2606 OID 27116791)
-- Name: Tender_SRL_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tender_srl
    ADD CONSTRAINT "Tender_SRL_pkey" PRIMARY KEY (id);


--
-- TOC entry 2199 (class 2606 OID 32617457)
-- Name: Tendered_RL_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tendered_rl
    ADD CONSTRAINT "Tendered_RL_pkey" PRIMARY KEY (time_stamp);


--
-- TOC entry 2146 (class 2606 OID 19666902)
-- Name: TimeStamps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY time_stamps
    ADD CONSTRAINT "TimeStamps_pkey" PRIMARY KEY (time_id);


--
-- TOC entry 2153 (class 2606 OID 19762097)
-- Name: Wind_actual_DE_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_wind_actual_de
    ADD CONSTRAINT "Wind_actual_DE_pkey" PRIMARY KEY (id);


--
-- TOC entry 2156 (class 2606 OID 19762116)
-- Name: Wind_expected_DE_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY eextrans_wind_expected_de
    ADD CONSTRAINT "Wind_expected_DE_pkey" PRIMARY KEY (id);


--
-- TOC entry 2208 (class 2606 OID 37145764)
-- Name: epex_dayahead_hour_de_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY epex_dayahead_hour_de
    ADD CONSTRAINT epex_dayahead_hour_de_pkey PRIMARY KEY (id);


--
-- TOC entry 2167 (class 2606 OID 37145810)
-- Name: power_plants_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY power_plants
    ADD CONSTRAINT power_plants_pkey PRIMARY KEY (plant_id);


--
-- TOC entry 2163 (class 1259 OID 36615578)
-- Name: eextrans_solar_actual_de_tsidx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX eextrans_solar_actual_de_tsidx ON eextrans_solar_actual_de USING btree (time_stamp);


--
-- TOC entry 2160 (class 1259 OID 36615579)
-- Name: eextrans_solar_expected_de_tsidx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX eextrans_solar_expected_de_tsidx ON eextrans_solar_expected_de USING btree (time_stamp);


--
-- TOC entry 2154 (class 1259 OID 36615580)
-- Name: eextrans_wind_actual_de_tsidx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX eextrans_wind_actual_de_tsidx ON eextrans_wind_actual_de USING btree (time_stamp);


--
-- TOC entry 2157 (class 1259 OID 36615581)
-- Name: eextrans_wind_expected_de_tsidx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX eextrans_wind_expected_de_tsidx ON eextrans_wind_expected_de USING btree (time_stamp);


--
-- TOC entry 2142 (class 1259 OID 32478507)
-- Name: pididx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pididx ON eextrans_blockwise_de USING btree (plant_id);


--
-- TOC entry 2184 (class 1259 OID 32861121)
-- Name: stationidx1; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX stationidx1 ON climate_temperature_by_station USING btree (station_id);


--
-- TOC entry 2192 (class 1259 OID 27143493)
-- Name: stationidx3; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX stationidx3 ON climate_wind_by_station USING btree (station_id);


--
-- TOC entry 2196 (class 1259 OID 27445065)
-- Name: stationidx4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX stationidx4 ON climate_solar_by_station USING btree (station_id);


--
-- TOC entry 2147 (class 1259 OID 27116980)
-- Name: tsidx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidx ON time_stamps USING btree (time_stamp);


--
-- TOC entry 2185 (class 1259 OID 27116983)
-- Name: tsidx2; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidx2 ON climate_temperature_by_station USING btree (time_id);


--
-- TOC entry 2193 (class 1259 OID 27143492)
-- Name: tsidx3; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidx3 ON climate_wind_by_station USING btree (time_id);


--
-- TOC entry 2197 (class 1259 OID 27445066)
-- Name: tsidx4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidx4 ON climate_solar_by_station USING btree (time_id);


--
-- TOC entry 2202 (class 1259 OID 32877136)
-- Name: tsidx_freq; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidx_freq ON "ENTSOE_Frequency" USING btree ("TimeStamp");

ALTER TABLE "ENTSOE_Frequency" CLUSTER ON tsidx_freq;


--
-- TOC entry 2176 (class 1259 OID 32619126)
-- Name: tsidx_mrltender; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidx_mrltender ON tender_mrl USING btree (time_stamp);


--
-- TOC entry 2179 (class 1259 OID 27118516)
-- Name: tsidx_srltender; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidx_srltender ON tender_srl USING btree (time_stamp);


--
-- TOC entry 2143 (class 1259 OID 32478533)
-- Name: tsidxbwbe; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidxbwbe ON eextrans_blockwise_de USING btree (time_id);


--
-- TOC entry 2144 (class 1259 OID 32478508)
-- Name: tsidxbwde; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidxbwde ON eextrans_blockwise_de USING btree (time_id);


--
-- TOC entry 2209 (class 1259 OID 37145456)
-- Name: tsidxdade2; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidxdade2 ON epex_dayahead_hour_de USING btree (time_stamp);


--
-- TOC entry 2124 (class 1259 OID 27135014)
-- Name: tsidxepexidde; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidxepexidde ON epex_intraday_de USING btree (time_stamp);


--
-- TOC entry 2127 (class 1259 OID 27135015)
-- Name: tsidxepexiddehour; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidxepexiddehour ON epex_intraday_de_hourly USING btree (time_stamp);


--
-- TOC entry 2173 (class 1259 OID 27118514)
-- Name: tsidxmrlnrv; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidxmrlnrv ON mrl_netzregelverbund USING btree (time_stamp);


--
-- TOC entry 2170 (class 1259 OID 27118515)
-- Name: tsidxsrlnrv; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tsidxsrlnrv ON srl_netzregelverbund USING btree (time_stamp);


-- Completed on 2016-08-11 10:27:27 CEST

--
-- PostgreSQL database dump complete
--

