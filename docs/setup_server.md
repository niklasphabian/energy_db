# Have postgres listen
edit /etc/postgresql/9.5/main/pg_hba.conf
host    all     all   192.168.2.0/24                         md5

edit /etc/postgresql/9.5/main/postgresql.conf
listen_addresses = '*'

# Setting postgresuser password
sudo -u postgres psql
\password

# Create database
sudo -u postgres psql -c "CREATE DATABASE IF NOT EXISTS energyDB;"

# Create schemas
psql -d energyDB -f create_schema.sql
psql -d energyDB -f create_schema_blockwise.sql

# Add user and group
sudo -u postgres psql -c "CREATE USER <user> WITH PASSWORD '<password>';"
sudo -u postgres createdb <user>
sudo -u postgres psql -c "CREATE GROUP energy_db_rw;"
sudo -u postgres psql -d energydb  -c "ALTER GROUP energy_db_rw ADD USER <user>;"

# Set privileges
sudo -u postgres psql -c "GRANT ALL ON DATABASE energydb TO energy_db_rw;"
sudo -u postgres psql -d energydb -c "GRANT ALL ON ALL TABLES IN SCHEMA blockwise TO energy_db_rw;"
sudo -u postgres psql -d energydb -c "GRANT ALL ON SCHEMA blockwise TO energy_db_rw;"

