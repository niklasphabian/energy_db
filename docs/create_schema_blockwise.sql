--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

-- Started on 2017-03-09 18:25:19 PST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 2200)
-- Name: blockwise; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA blockwise;


--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA blockwise; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA blockwise IS 'standard public schema';


--
-- TOC entry 1 (class 3079 OID 12361)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = blockwise, pg_catalog;

--
-- TOC entry 188 (class 1255 OID 16397)
-- Name: substitue_plantid(integer, integer); Type: FUNCTION; Schema: blockwise; Owner: -
--

CREATE FUNCTION substitue_plantid(old_id integer, new_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
  BEGIN
    UPDATE blockwise_de SET plant_id = new_id WHERE plant_id = old_id;
    UPDATE power_plants SET power_electric = (SELECT power_electric FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET renovation_year = (SELECT renovation_year FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET construction_year = (SELECT construction_year FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET plz = (SELECT plz FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET power_thermal = (SELECT power_thermal FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET urban = (SELECT urban FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET disposal_plant = (SELECT disposal_plant FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET bnetz_kraftwerksnummer = (SELECT bnetz_kraftwerksnummer FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    UPDATE power_plants SET industrial = (SELECT industrial FROM power_plants WHERE plant_id=old_id) WHERE plant_id = new_id;
    DELETE FROM power_plants WHERE plant_id = old_id;
    END;
 $$;


SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 16530)
-- Name: blockwise_de; Type: TABLE; Schema: blockwise; Owner: -
--

CREATE TABLE blockwise_de (
    plant_id integer,
    power double precision,
    time_id integer,
    pkey integer NOT NULL
);


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 181
-- Name: TABLE blockwise_de; Type: COMMENT; Schema: blockwise; Owner: -
--

COMMENT ON TABLE blockwise_de IS 'Electricity production of German power plants with a power output of 100 MW or more with to-the-block-precision and hourly resolution
Data retrieved from www.eex-transparency.com

PlantID: ID of the power plant (metadata of plant translatable through PowerPlants table)
Power: Power output in MW
timeID: ID of the timestamp (translatable to human-readable timestamps through TimeStamps table)
pkey: Primary key';


--
-- TOC entry 182 (class 1259 OID 16533)
-- Name: PowerGeneration_pkey_seq; Type: SEQUENCE; Schema: blockwise; Owner: -
--

CREATE SEQUENCE "PowerGeneration_pkey_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2140 (class 0 OID 0)
-- Dependencies: 182
-- Name: PowerGeneration_pkey_seq; Type: SEQUENCE OWNED BY; Schema: blockwise; Owner: -
--

ALTER SEQUENCE "PowerGeneration_pkey_seq" OWNED BY blockwise_de.pkey;


--
-- TOC entry 183 (class 1259 OID 16535)
-- Name: power_plants; Type: TABLE; Schema: blockwise; Owner: -
--

CREATE TABLE power_plants (
    company text,
    unit text,
    plant text,
    fuel text,
    plant_id integer NOT NULL,
    full_name text,
    power_electric double precision,
    renovation_year integer,
    construction_year integer,
    plz text,
    power_thermal double precision,
    country character(2),
    urban boolean,
    disposal_plant boolean,
    bnetz_kraftwerksnummer text,
    industrial boolean
);


--
-- TOC entry 2141 (class 0 OID 0)
-- Dependencies: 183
-- Name: TABLE power_plants; Type: COMMENT; Schema: blockwise; Owner: -
--

COMMENT ON TABLE power_plants IS 'Table of German powerplants with a power output of 100 MW or more
Data obtained from www.eex-transparency.com and from UBA.

Company: Company operating and/or operating the powerplant
Unit: Name of the unit
Plant: Name of the plant
Fuel: Employed fuel of the powerplant
PlantID: ID of the powerplant (internal DB key)
FullName: Unique identifier string - merge of column "Unit" and "Plant" 
PowerElectric: Electric nominal power of the powerplant in MW
RenovationYear: Year of most recent renovation
ContructionYear: Year of construction
ZipCode: Zipcode (PLZ) of the power plant
PowerThermal: Nominal thermal power of the powerplant in MW';


--
-- TOC entry 2142 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN power_plants.full_name; Type: COMMENT; Schema: blockwise; Owner: -
--

COMMENT ON COLUMN power_plants.full_name IS 'Made by the concatenation of Unit + Plant';


--
-- TOC entry 2143 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN power_plants.power_electric; Type: COMMENT; Schema: blockwise; Owner: -
--

COMMENT ON COLUMN power_plants.power_electric IS 'Die elektrische Bruttoleistung von dem Kraftwerkteil in MW';


--
-- TOC entry 2144 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN power_plants.power_thermal; Type: COMMENT; Schema: blockwise; Owner: -
--

COMMENT ON COLUMN power_plants.power_thermal IS 'Die CHP Leistung von dem Kraftwerkteil in MW';


--
-- TOC entry 184 (class 1259 OID 16541)
-- Name: PowerPlants_PlantID_seq; Type: SEQUENCE; Schema: blockwise; Owner: -
--

CREATE SEQUENCE "PowerPlants_PlantID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2145 (class 0 OID 0)
-- Dependencies: 184
-- Name: PowerPlants_PlantID_seq; Type: SEQUENCE OWNED BY; Schema: blockwise; Owner: -
--

ALTER SEQUENCE "PowerPlants_PlantID_seq" OWNED BY power_plants.plant_id;


--
-- TOC entry 185 (class 1259 OID 16583)
-- Name: time_stamps; Type: TABLE; Schema: blockwise; Owner: -
--

CREATE TABLE time_stamps (
    time_stamp timestamp with time zone,
    time_id integer NOT NULL
);


--
-- TOC entry 2146 (class 0 OID 0)
-- Dependencies: 185
-- Name: TABLE time_stamps; Type: COMMENT; Schema: blockwise; Owner: -
--

COMMENT ON TABLE time_stamps IS 'Table to translate timeIDs into human-readable TimeStamps';


--
-- TOC entry 186 (class 1259 OID 16586)
-- Name: TimeStamps_timeID_seq; Type: SEQUENCE; Schema: blockwise; Owner: -
--

CREATE SEQUENCE "TimeStamps_timeID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2147 (class 0 OID 0)
-- Dependencies: 186
-- Name: TimeStamps_timeID_seq; Type: SEQUENCE OWNED BY; Schema: blockwise; Owner: -
--

ALTER SEQUENCE "TimeStamps_timeID_seq" OWNED BY time_stamps.time_id;


--
-- TOC entry 187 (class 1259 OID 16606)
-- Name: plant_distinciton; Type: VIEW; Schema: blockwise; Owner: -
--

CREATE VIEW plant_distinciton AS
 SELECT power_plants.plant_id,
    regexp_replace(power_plants.plant, 'KW |Kraftwerk |GuD |HKW |Heizkraftwerk |GKW |KKW |Kernkraftwerk '::text, ''::text) AS cut_name,
    power_plants.company,
    power_plants.unit,
    power_plants.plant,
    power_plants.fuel,
    power_plants.power_electric,
    power_plants.bnetz_kraftwerksnummer
   FROM power_plants
  ORDER BY (regexp_replace(power_plants.plant, 'KW |Kraftwerk |GuD |HKW |Heizkraftwerk |GKW |KKW |Kernkraftwerk '::text, ''::text));


--
-- TOC entry 2004 (class 2604 OID 16621)
-- Name: pkey; Type: DEFAULT; Schema: blockwise; Owner: -
--

ALTER TABLE ONLY blockwise_de ALTER COLUMN pkey SET DEFAULT nextval('"PowerGeneration_pkey_seq"'::regclass);


--
-- TOC entry 2005 (class 2604 OID 16641)
-- Name: plant_id; Type: DEFAULT; Schema: blockwise; Owner: -
--

ALTER TABLE ONLY power_plants ALTER COLUMN plant_id SET DEFAULT nextval('"PowerPlants_PlantID_seq"'::regclass);


--
-- TOC entry 2006 (class 2604 OID 16646)
-- Name: time_id; Type: DEFAULT; Schema: blockwise; Owner: -
--

ALTER TABLE ONLY time_stamps ALTER COLUMN time_id SET DEFAULT nextval('"TimeStamps_timeID_seq"'::regclass);


--
-- TOC entry 2008 (class 2606 OID 16702)
-- Name: PowerGeneration_pkey; Type: CONSTRAINT; Schema: blockwise; Owner: -
--

ALTER TABLE ONLY blockwise_de
    ADD CONSTRAINT "PowerGeneration_pkey" PRIMARY KEY (pkey);


--
-- TOC entry 2015 (class 2606 OID 16720)
-- Name: TimeStamps_pkey; Type: CONSTRAINT; Schema: blockwise; Owner: -
--

ALTER TABLE ONLY time_stamps
    ADD CONSTRAINT "TimeStamps_pkey" PRIMARY KEY (time_id);


--
-- TOC entry 2013 (class 2606 OID 16728)
-- Name: power_plants_pkey; Type: CONSTRAINT; Schema: blockwise; Owner: -
--

ALTER TABLE ONLY power_plants
    ADD CONSTRAINT power_plants_pkey PRIMARY KEY (plant_id);


--
-- TOC entry 2009 (class 1259 OID 16733)
-- Name: pididx; Type: INDEX; Schema: blockwise; Owner: -
--

CREATE INDEX pididx ON blockwise_de USING btree (plant_id);


--
-- TOC entry 2016 (class 1259 OID 16737)
-- Name: tsidx; Type: INDEX; Schema: blockwise; Owner: -
--

CREATE INDEX tsidx ON time_stamps USING btree (time_stamp);


--
-- TOC entry 2010 (class 1259 OID 16744)
-- Name: tsidxbwbe; Type: INDEX; Schema: blockwise; Owner: -
--

CREATE INDEX tsidxbwbe ON blockwise_de USING btree (time_id);


--
-- TOC entry 2011 (class 1259 OID 16745)
-- Name: tsidxbwde; Type: INDEX; Schema: blockwise; Owner: -
--

CREATE INDEX tsidxbwde ON blockwise_de USING btree (time_id);


-- Completed on 2017-03-09 18:25:19 PST

--
-- PostgreSQL database dump complete
--

