echo 'this script creates the db config file. It will prompt for hostname, host address, database name, database user, and password'

read -p "Host name: " HOSTNAME
read -p "Host address: " HOSTADDR
read -p "DB name: " DBNAME
read -p "DB user name: " USER
read -s -p "DB user pwd: " PWD
echo ''

echo "[$HOSTNAME]" > ../energy_db/database/database_config.ini
echo "host = $HOSTADDR" >> ../energy_db/database/database_config.ini
echo "database = $DBNAME" >> ../energy_db/database/database_config.ini
echo "user = $USER" >> ../energy_db/database/database_config.ini
echo "pwd = $PWD" >> ../energy_db/database/database_config.ini

chmod 600 ../energy_db/database/database_config.ini
